interface Demo{
	void fun();
	void gun();
}
abstract class DemoChild implements Demo{

	public void gun(){
		System.out.println("In gun");
	}
}
class DemoChild1 extends DemoChild{

	public void fun(){
		System.out.println("In fun");
	}
}
class Client{
	public static void main(String[] args){

		Demo obj1=new DemoChild1();
		obj1.fun();
		obj1.gun();

		DemoChild obj2=new DemoChild1();
		obj2.fun();
		obj2.gun();

		DemoChild1 obj3=new DemoChild1();
		obj3.fun();
		obj3.gun();
	}
}
/*
output:
In fun
In gun
In fun
In gun
In fun
In gun
 */
