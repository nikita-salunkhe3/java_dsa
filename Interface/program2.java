interface Demo{

	void fun();
	void gun();
}
class DemoChild implements Demo{
	public void gun(){
		System.out.println("In Gun");
	}
	public void fun(){
		System.out.println("In fun");
	}
}
class Client{
	public static void main(String[] args){

		DemoChild obj=new DemoChild();
		obj.fun();
		obj.gun();

		Demo obj1=new DemoChild();
		obj1.fun();
		obj1.gun();
	}
}
/*
 In fun
 In Gun
 In fun
 In Gun
 */




