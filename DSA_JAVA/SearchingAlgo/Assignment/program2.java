//write a program of binary serach in recursion form

import java.util.*;
class Client{

	static int binarySearch(int arr[],int search,int start,int end){

		if(start > end){
			return -1;
		}
		int mid=(start+end)/2;

		if(arr[mid]==search)
			return mid;
		else if(arr[mid] > search)
			return binarySearch(arr,search,start,mid-1);
		else
			return binarySearch(arr,search,mid+1,end);
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("ENter the array elemnets");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the search elements");
		int search=sc.nextInt();
		
		int ret=binarySearch(arr,search,0,arr.length-1);
		if(ret==-1){
			System.out.println(search+" is Not found");
		}else{
			System.out.println(search+" is found at index"+ret);
		}
	}
}



