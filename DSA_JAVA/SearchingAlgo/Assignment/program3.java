//
//arr[]=[2,4,4,4,6,7,8]
//target = 4
//output = 1
//
//find the first occurence in sorted array

import java.util.*;
class FirstOcc{
	static int firstocc(int arr[],int target){

		int start=0;
		int end=arr.length-1;

		int store=-1;
		int store1=-1;
		while(start <= end){

			int mid=(start+end)/2;

			if(arr[mid]==target){

				if(arr[mid-1] < target  && arr[mid+1] > target){
					return mid;
				}
				else if(arr[mid+1] > target){


				}else if(arr[mid-1] != target){
					return mid;
				}
			}
		
			if(arr[mid] > target){
				end=mid-1;
			}else{
				start=mid+1;
			}
		}
		return -1;
	}

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter array size");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int target=sc.nextInt();

		int ret=firstocc(arr,target);

		if(ret==-1){
			System.out.println("Element not found");
		}else{
			System.out.println("Element occur at index"+ret);
		}

	}
}



