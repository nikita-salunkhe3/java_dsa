//Binary serach code using While loop

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter search elements");

		int search=sc.nextInt();

		int start=0;
		int end=arr.length-1;
		while(start <= end){

			int mid=(start+end)/2;

			if(arr[mid] == search){
				System.out.println(mid);
				break;
			}
			if(arr[mid] > search){
				end=mid-1;
			}
			if(arr[mid]< search){
				start=mid+1;
			}
		}
	}
}
