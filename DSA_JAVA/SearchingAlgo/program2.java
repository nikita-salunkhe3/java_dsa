import java.util.*;
class Client{
	static int start=0;
	static int end=0;
	static int binarySearch(int arr[],int search){

		if(start > end){
			return -1;
		}else{
			int mid=(start+end)/2;

			if(arr[mid] == search){
				return mid;
			}
			if(arr[mid] > search){
				end=mid-1;
			}else{
				start=mid+1;
			}
		}
		return binarySearch(arr,search);
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter the elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		end=arr.length-1;

		System.out.println("Enter the search element");

		int search=sc.nextInt();

		int ret=binarySearch(arr,search);

		if(ret == -1){
			System.out.println("Element is not found");
		}else{
			System.out.println("Element found at index: "+ret);
		}
	}
}



