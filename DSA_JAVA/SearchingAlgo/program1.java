//Normal code

import java.util.*;
class BinarySearch{

	static int binarySearch(int arr[],int search){

		int start=0;
		int end=arr.length-1;

		while(start <= end){
			int mid=(start+end)/2;

			if(arr[mid] > search){
				end=mid-1;
			}
			if(arr[mid] < search){
				start=mid+1;
			}
		}
		return -1;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter search element");

		int search=sc.nextInt();

		int ret=binarySearch(arr,search);

		if(ret==-1){
			System.out.println("Search element is not found");
		}else{
			System.out.println("Search element is found at index :"+ret);
		}
	}
}
