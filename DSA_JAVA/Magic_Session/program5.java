/*
 Problem 5
Given an array of size N-1 such that it only contains distinct integers in the range of 1 to N. Find
the missing element.
Example 1:
Input:
N=5
A[] = {1,2,3,5}
Output: 4
Example 2:
Input:
N = 10
A[] = {6,1,2,8,3,4,7,10,5}
Output: 9
Your Task :
You don't need to read input or print anything. Complete the function MissingNumber() that
takes array and N as input parameters and returns the value of the missing number.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 106
1 ≤ A[i] ≤ 106
 */

import java.util.*;
class Client{
	static int MissingEle(int arr[]){

		for(int i=1;i<=arr.length;i++){
			int flag=0;
			for(int j=0;j<arr.length;j++){

				if(i==arr[j]){
					flag=1;
					break;
				}
			}

			if(flag==0){
				return i;
			}
		}
		return -1;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int ret=MissingEle(arr);
		if(ret==-1){
			System.out.println("There is no Missing element found");
		}else{
			System.out.println("Missing element is: "+ret);
		}
	}
}
