/*
 Problem 1:
Given an array containing n integers. The problem is to find the sum of the
elements of the contiguous subarray having the smallest(minimum) sum.
Examples:
Input : arr[] = {3, -4, 2, -3, -1, 7, -5}
Output : -6
Subarray is {-4, 2, -3, -1} = -6
Input : arr = {2, 6, 8, 1, 4}
Output : 1
 */

import java.io.*;
class Client{
	static int subArray(int arr[]){

		int store=0;
		int minSum=Integer.MAX_VALUE;
		for(int i=0;i<arr.length;i++){
			
			for(int j=i;j<arr.length;j++){

				int sum=0;
				for(int k=i;k<=j;k++){
					sum=sum+arr[k];
					store=sum;
				}
				if(minSum > store){
					minSum=store;
				}
			}
		}
		return minSum;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of an array");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Minimun sum of SubArray: "+subArray(arr));
	}
}
		


