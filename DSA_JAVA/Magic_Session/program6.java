/*
 Problem 6
Given an array A of n positive numbers. The task is to find the first equilibrium point in an array.
Equilibrium point in an array is an index (or position) such that the sum of all elements before
that index is the same as the sum of elements after it.
Note: Return equilibrium point in 1-based indexing. Return -1 if no such point exists.
Example 1:
Input:
n=5
A[] = {1,3,5,2,2}
Output:
3
Explanation:
equilibrium point is at position 3 as sum of elements before it (1+3) = sum of elements after it
(2+2).
Example 2:
Input:
n=1
A[] = {1}
Output:
1
Explanation:
Since there's only one element hence it's only the equilibrium point.
Your Task:
The task is to complete the function equilibriumPoint() which takes the array and n as input
parameters and returns the point of equilibrium.
Expected Time Complexity: O(n)
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 105
1 <= A[i] <= 109
 */

import java.util.*;
class Client{
	static int equilibriumPosition(int arr[]){

		for(int i=0;i<arr.length;i++){
			int leftsum=0;
			int rightsum=0;
			for(int j=0;j<i;j++){
				leftsum=leftsum+arr[j];
			}
			for(int j=i+1;j<arr.length;j++){
				rightsum=rightsum+arr[j];
			}
			if(leftsum==rightsum){
				return i+1;
			}
		}
		return -1;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter array size");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int ret=equilibriumPosition(arr);

		if(ret==-1){
			System.out.println("Equilibrium position is Not found");
		}else{
			System.out.println("Equilibrium position found at: "+ret);
		}
	}
}

