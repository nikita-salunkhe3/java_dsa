/*
 Example 10
You are given an array of size N containing integers. Your task is to find the number of
subarrays that can be formed from the given array. A subarray is defined as a contiguous
sequence of elements in the array.
Input:
arr = [1, 2, 3]
Output:
6
Explanation:
In this example, the possible subarrays are [1], [2], [3], [1, 2], [2, 3], and [1, 2, 3], so the
total count is 6.
 */

import java.util.*;
class Client{
	static int subArray(int arr[]){
		
		int count=0;
		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){
				count++;
			}
		}
		return count;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter size an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		
		System.out.println("No of subarray is: "+subArray(arr));
	}
}
