/*
 Problem 4:
Given an array of positive integers nums and a positive integer target, return the minimal length
of a
subarray
whose sum is greater than or equal to target. If there is no such subarray, return 0 instead.
Example 1:
Input: target = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: The subarray [4,3] has the minimal length under the problem constraint.
Example 2:
Input: target = 4, nums = [1,4,4]
Output: 1
Example 3:
Input: target = 11, nums = [1,1,1,1,1,1,1,1]
Output: 0
Constraints:
1 <= target <= 109
1 <= nums.length <= 105
1 <= nums[i] <= 104
 */

import java.util.*;
class Client{

	static int ArraySum(int arr[],int target){

		int storei=-1;
		int storej=-1;

		int minlen=Integer.MAX_VALUE;

		int len=0;
		int flag=0;
		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){

				int sum=0;
				for(int k=i;k<=j;k++){
					sum=sum+arr[k];
				}
				len=j-i+1;
			
				if(minlen > len && target <= sum){
					minlen=len;	
					storei=i;
					storej=j;
					flag=1;
				}
			}
		}
		if(flag==1){
			return minlen;
		}else{
			return 0;
		}
		
	}
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter target element");

		int target=sc.nextInt();

		System.out.println(ArraySum(arr,target));
	}
}
		

			
