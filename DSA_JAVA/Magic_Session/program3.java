/*
 Problem 3:
Given a string s and an array of string words, determine whether s is a prefix string of words.
A string s is a prefix string of words if s can be made by concatenating the first k strings in words
for some positive k no larger than words.length.
Return true if s is a prefix string of words, or false otherwise.
Example 1:
Input: s = "iloveleetcode", words = ["i","love","leetcode","apples"]
Output: true
Explanation:
s can be made by concatenating "i", "love", and "leetcode" together.
Example 2:
Input: s = "iloveleetcode", words = ["apples","i","love","leetcode"]
Output: false
Explanation:
It is impossible to make s using a prefix of arr.
Constraints:
1 <= words.length <= 100
1 <= words[i].length <= 20
1 <= s.length <= 1000
words[i] and s consist of only lowercase English letters.
 */

import java.util.*;
class Client{
	static boolean prefixString(String str,String arr[]){
	
		int k=0;
		for(int i=0;i<arr.length;i++){
			int j=0;
			while(j < arr[i].length()){
				if(k < str.length()){
					if(str.charAt(k) == arr[i].charAt(j)){
						k++;
					}else{
						return false;
					}
				}
				j++;
			}
		}
		return true;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the prefix String");

		String str=sc.next();

		System.out.println("Enter the size of String array");

		int size=sc.nextInt();

		String arr[]=new String[size];

		System.out.println("Enter the string");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.next();
		}
		System.out.println(prefixString(str,arr));
	}
}

				
