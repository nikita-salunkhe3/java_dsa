/*
 Problem 7
Given an array Arr[] that contains N integers (may be positive, negative or zero). Find the
product of the maximum product subarray.
Example 1:
Input:
N=5
Arr[] = {6, -3, -10, 0, 2}
Output: 180
Explanation: Subarray with maximum product
is [6, -3, -10] which gives the product as 180.
Example 2:
Input:
N=6
Arr[] = {2, 3, 4, 5, -1, 0}
Output: 120
Explanation: Subarray with maximum product
is [2, 3, 4, 5] which gives the product as 120.
 */

import java.util.*;
class Client{
	static int maxProduct(int arr[]){

		int maxpro=Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){
				int pro=1;
				for(int k=i;k<=j;k++){
					pro=pro*arr[k];
				}
				if(maxpro < pro){
					maxpro=pro;
				}
			}
		}
		return maxpro;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter size");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Maximum product is : "+maxProduct(arr));
	}
}
