/*
 Example 9
Given a sorted array arr containing n elements with possibly some duplicate, the task is to find
the first and last occurrences of an element x in the given array.
Note: If the number x is not found in the array then return both the indices as -1.
Example 1:
Input:
n=9, x=5
arr[] = { 1, 3, 5, 5, 5, 5, 67, 123, 125 }
Output:
25
Explanation:
First occurrence of 5 is at index 2 and last occurrence of 5 is at index 5.
Example 2:
Input:
n=9, x=7
arr[] = { 1, 3, 5, 5, 5, 5, 7, 123, 125 }
Output:
66
Explanation:
First and last occurrence of 7 is at index 6.
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter sorted elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter the occurence Number");

		int num=sc.nextInt();

		int start=0;
		int end=0;
		int last=0;
		int flag=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==num){
				start=i;
				while(arr[i] == num){

					System.out.println("===============");
					if(i == arr.length-1){
						flag=1;
						System.out.println("*************");
						last=i;
						break;
					}
					i++;
				}
			        end=i-1;
				break;
			}
		}
		System.out.println("First Occurence of "+num+ " is : "+start);
		if(flag==0){
			System.out.println("last Occurence of "+num+ " is : "+end);
		}else{
			System.out.println("last Occurence of "+num+ " is : "+last);
		}

	}
}
