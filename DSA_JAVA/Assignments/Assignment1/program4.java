/*
 Q4. Time to equality
Problem Description
- Given an integer array A of size N.
- In one second, you can increase the value of one element by 1.
- Find the minimum time in seconds to make all elements of the array
equal.
Problem Constraints
1 <= N <= 1000000
1 <= A[i] <= 1000
Example Input
A = [2, 4, 1, 3, 2]
Example Output
8
Example Explanation
We can change the array A = [4, 4, 4, 4, 4]. The time required will be 8
seconds.
 */

import java.util.*;

class Client{
	static int timeToEquality(int arr[]){

		int max=Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			if(arr[i] > max){
				max=arr[i];
			}
		}

		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==max){
				continue;
			}else{
				int num=arr[i];
				while(num != max){
					count++;
					num++;
				}
				arr[i]=num;
			}
		}
		return count;
	}
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Seconds are : "+timeToEquality(arr));
	}
}



