/*
 Q3. Range Sum Query
Problem Description
- You are given an integer array A of length N.
- You are also given a 2D integer array B with dimensions M x 2, where
each row
denotes a [L, R] query.
- For each query, you have to find the sum of all elements from L to R
indices
in A (0 - indexed).
- More formally, find A[L] + A[L + 1] + A[L + 2] +... + A[R - 1] + A[R] for each
query.
Problem Constraints
1 <= N, M <= 103
1 <= A[i] <= 105
0 <= L <= R < N
Example Input
Input 1:
A = [1, 2, 3, 4, 5]
B = [[0, 3], [1, 2]]
Input 2:
A = [2, 2, 2]
B = [[0, 0], [1, 2]]
Example Output
Output 1:
[10, 5]
Output 2:
[2, 4]
Example Explanation
Explanation 1:
The sum of all elements of A[0 ... 3] = 1 + 2 + 3 + 4 = 10.
The sum of all elements of A[1 ... 2] = 2 + 3 = 5.
Explanation 2:
The sum of all elements of A[0 ... 0] = 2 = 2.
The sum of all elements of A[1 ... 2] = 2 + 2 = 4.
 */

import java.util.*;

class Client{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter row for 2D array");

		int row=sc.nextInt();

		System.out.println("Enter column for 2D array");

		int col=sc.nextInt();

		int arr1[][]=new int[row][col];

		System.out.println("Enter the elements");

		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				arr1[i][j]=sc.nextInt();
			}
		}

		for(int i=1;i<arr.length;i++){
			arr[i]=arr[i-1]+arr[i];
		}

		for(int i=0;i<row;i++){
			int sum=0;
			int j=0;
			
			if(arr1[i][j] < 0 || arr1[i][j+1] >= arr.length){
				System.out.println("Invalid index in array");
			}else{		
				if(arr1[i][j]==0){
					sum=arr[arr1[i][j+1]];
				}else{
					sum=arr[arr1[i][j+1]] - arr[arr1[i][j]-1];			
				}
				System.out.println("sum is: "+sum);
			}
		}
	}
}
