/*
 Q6. Product array puzzle
Problem Description
- Given an array of integers A, find and return the product array of the same
size where the ith element of the product array will be equal to the
product of all the elements divided by the ith element of the array.
- Note: It is always possible to form the product array with integer (32 bit)
values. Solve it without using the division operator.
Constraints
2 <= length of the array <= 1000
1 <= A[i] <= 10
For Example
Input 1:
A = [1, 2, 3, 4, 5]
Output 1:
[120, 60, 40, 30, 24]
Input 2:
A = [5, 1, 10, 1]
Output 2:
[10, 50, 5, 50]
 */

class Arrayi{
	static void PArray(int arr[],int n){

		if(n==1){
			System.out.print(0);
			return ;
		}

		int left[]=new int[n];
		int right[]=new int[n];
		int pro[]=new int[n];

		left[0]=1;
		right[n-1]=1;

		for(int i=1;i<n;i++){
			left[i]=arr[i-1] * left[i-1];
		}
		for(int j=n-2;j>=0;j++){
			right[j]=arr[j+1] * right[j+1];
		}

		for(int i=0;i<n;i++){
			pro[i]=left[i]*right[i];
		}
		for(int i=0;i<n;i++){
			System.out.println(pro[i]);
		}
	}
	public static void main(String[] args){

		int arr[]=new int[]{1,2,3,4,5};

		int n=arr.length;
		PArray(arr,n);

		System.out.println("Product array becomes");
	}
}

