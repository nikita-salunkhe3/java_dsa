import java.util.*;

class StackUsingArray{
	int maxsize;
	int maxarr[];
	int top=-1;

	StackUsingArray(int size){

		maxsize=size;
		maxarr=new int[size];
	}

	boolean empty(){
		if(top==-1){
			return true;
		}else{
			return false;
		}
	}
	void push(int data){
		if(maxsize-1 == top){
			System.out.println("Stack Overflow");
			return;
		}else{
			top++;
			maxarr[top]=data;
		}
	}
	int pop(){
		if(empty()){
			System.out.println("Stack Underflow");
			return -1;
		}else{
			int val=maxarr[top];
			top--;
			return val;
		}
	}
	int peek(){
		if(empty()){
			System.out.println("Empty LinkedList");
			return -1;
		}else{
			return maxarr[top];
		}
	}
	void printStack(){
		if(empty()){
			System.out.println("Empty LL");
			return;
		}else{
			System.out.print("[");
			for(int i=0;i<=top;i++){
				System.out.print(maxarr[i]+" ");
			}
			System.out.println("]");
		}
	}
	int size(){
		return top+1;
	}

}
class Client{
	public static void main(String[] args){

		char ch;
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of stack Array");
		int size=sc.nextInt();
		
		StackUsingArray obj=new StackUsingArray(size);

		do{
			System.out.println("1. push");
			System.out.println("2. pop");
			System.out.println("3. peek");
			System.out.println("4. printStack");
			System.out.println("5. empty");
			System.out.println("6. size");

			System.out.println("Enter your choice");

			int choice=sc.nextInt();

			switch(choice){
				case 1:{
						System.out.println("Enter the data except -1 value");
						int data=sc.nextInt();
						obj.push(data);
					}
					break;
				case 2:
					{
						int ret=obj.pop();
						if(ret != -1){
							System.out.println(ret+" is Popped");
						}
					}
					break;
				case 3:
					{
						int ret=obj.peek();
						if(ret != -1){
							System.out.println(ret+" is printed");
						}
					}
					break;
				case 4:
					obj.printStack();
					break;
				case 5:{
						boolean val=obj.empty();
						System.out.println(val);
					}
				       	break;
				case 6:
					{
						int num=obj.size();
						System.out.println("Size of array stack is "+num);
					}
					break;
				default:
					System.out.println("Invalid Choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}







			


