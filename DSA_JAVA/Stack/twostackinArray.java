import java.util.*;
class TwoStack{

	int flag=0;

	int top1=-1;
	int top2;
	int maxsize;
	int stackarr[];

	TwoStack(int size){
		this.stackarr=new int[size];
		this.maxsize=size;
		top2=size;
	}

	void push1(int data){

		if(top2 - top1 == 1){
			System.out.println("Stack1 Overflow");
			return;
		}else{
			top1++;
			stackarr[top1]=data;
		}
	}
	void push2(int data){

		if(top2 - top1 == 1){
			System.out.println("Stack2 Overflow");
			return;
		}else{
			top2--;
			stackarr[top2]=data;
		}
	}
	int pop1(){
		if(top1 == -1){
			return -1;
		}else{
			int val=stackarr[top1];
			if(val == -1){
				flag=1;
			}
			top1--;
			return val;
		}
	}
	int pop2(){
		if(top2 == maxsize){
			return -1;
		}else{
			int val=stackarr[top2];
			if(val == -1){
				flag=1;
			}
			top2--;
			return val;
		}
	}
}
class Client{
	public static void main(String[] args){

		char ch;

		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		TwoStack obj=new TwoStack(size);

		do{
			System.out.println("1. push1");
			System.out.println("2. push2");
			System.out.println("3. pop1");
			System.out.println("4. pop2");

			System.out.println("Enter your choice");
			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter data for stact1");
						int data=sc.nextInt();
						obj.push1(data);
					}
					break;
				case 2:
					{
						System.out.println("Enter data for stact2");
						int data=sc.nextInt();
						obj.push1(data);
					}
					break;
				case 3:
					{
						int ret=obj.pop1();
						if(obj.flag==0 && ret == -1){
							System.out.println("stack1 is empty");
						}else{
							System.out.println(ret+" is Popped from stack1");
						}
					}
					break;
				case 4:
					{
						int ret=obj.pop2();
						if(obj.flag==0 && ret==-1){
							System.out.println("Stack2 is empty");
						}else{
							System.out.println(ret+"is Popped for stack2");
						}
					}
					break;
				default:
					System.out.println("Invalid choice");

			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}


