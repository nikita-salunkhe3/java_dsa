//Reverse String using Stack

import java.util.*;
import java.io.*;
class ReverseString{

	String revString(String str){

		char stackarr[]=new char[str.length()];
		Stack<Character> s=new Stack<Character>();//stack madhil pretek character ha Object menun jato

		for(int i=0;i<str.length();i++){
			s.push(str.charAt(i));
		}
		int i=0;
		while(!s.empty()){
			stackarr[i]=s.pop();
			i++;
		}
		return new String(stackarr);//String kade aasa constructor ahe jya cha parameter
		// ha array ahe jer String cha constructor la character cha array pathvla ter to tyala 
		// String madhe convert kerto
	}

}
class Client{
	public static void main(String[] args) throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");

		String str=br.readLine();

		ReverseString obj=new ReverseString();

		String rev = obj.revString(str);

		System.out.println("Reverse String is "+rev);
	}
}
