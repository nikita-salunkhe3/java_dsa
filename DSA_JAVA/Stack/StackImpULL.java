import java.util.*;
class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}
class LinkedList{

	Node head=null;

	void push(int data){
		Node newnode =new Node(data);

		if(head==null){
			head=newnode;
		}else{
			newnode.next=head;
			head=newnode;
		}
	}
	int pop(){
		int data;
		if(head==null){
			System.out.println("Empty LinkedList");
			return -1;
		}else if(head.next == null){
			data=head.data;
			head=null;
		}else{
			data=head.data;
			head=head.next;
		}
		return data;
	}
	int peek(){
		int val;
		if(head==null){
			System.out.println("Empty LinkedList");
			return -1;
		}else{
			val=head.data;
		}
		return val;
	}
	void printLL(){
		if(head==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node temp=head;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
	}
}
class Client{
	public static void main(String[] args){
		
		char ch;
		LinkedList obj=new LinkedList();

		Scanner sc=new Scanner(System.in);

		do{
			System.out.println("1. push");
			System.out.println("2. pop");
			System.out.println("3. peek");
			System.out.println("4. printLL");

			System.out.println("Enter your choice");

			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();
						obj.push(data);
					}
					break;
				case 2:{

						int ret=obj.pop();
						if(ret != -1){
							System.out.println(ret);
						}
					}
					break;
				case 3:
					int val=obj.peek();
					if(val != -1){
						System.out.println(val);
					}
					break;
				case 4:
					obj.printLL();
					break;
				default:
					System.out.println("Invalid choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}
			
						


