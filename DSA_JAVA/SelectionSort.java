class Client{
	static int selectionSort(int arr[]){

		int count=0;
		for(int i=0;i<arr.length-1;i++){
			int minIndex=i;
			int occ=0;

			for(int j=i+1;j<arr.length;j++){
				count++;
				if(arr[j]<arr[minIndex]){
					occ++;
					minIndex=j;
				}
			}
			if(occ == 0){
				break;
			}
			int temp=arr[minIndex];
			arr[minIndex]=arr[i];
			arr[i]=temp;
		}
		return count;
	}
	public static void main(String[] args){

		int arr[]=new int[]{3,5,1,7,3,5,3,1,8,4,7};

		System.out.println(Client.selectionSort(arr));

		System.out.println("Sorted Array Becomes");

		for(int i=0;i<arr.length;i++){

			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}
