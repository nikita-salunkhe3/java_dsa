import java.io.*;
class Client{

	static int linearSearch(int arr[],int search){

		for(int i=0;i<arr.length;i++){
			if(search == arr[i]){

				return i;
			}
		}
		return -1;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of an array");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter search Element");

		int search=Integer.parseInt(br.readLine());

		int ret=linearSearch(arr,search);

		if(ret ==-1){
			System.out.println("Not found");
		}else{
			System.out.println("Found at index: "+ret);
		}
	}
}
