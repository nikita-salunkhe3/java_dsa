//Bubble Sort

class Client{
	static int bubbleSort(int arr[]){

		int steps=0;
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr.length-1-i;j++){	
				steps++;
				if(arr[j] > arr[j+1]){
					count++;
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
			if(count==0){
				break;
			}
		}
		return steps;
	}
	public static void main(String[] args){

		int arr[]=new int[]{5,2,3,1,4,2,6,7,2};

		System.out.println(Client.bubbleSort(arr));

		System.out.println("Sorted Array Becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
	
		System.out.println();
	}
}

