//Array must be sorted for bnary search

class Client{
	static int SearchEle(int arr[],int search){

		int start = 0;
		int end = arr.length-1;
		while(start <=  end){

			int mid=start + (end - start)/2;

			if(search == arr[mid]){
				return mid;
			}
			if(search < arr[mid]){
				end=mid-1;
			}else{
				start=mid+1;
			}
		}
		return -1;
	}

	public static void main(String[] args){

		int arr[]=new int[]{1,2,3,4,5,6,7,8,9};

		int search=6;
		
		int ret=Client.SearchEle(arr,search);

		if(ret ==-1)
			System.out.println("Not found");
		else
			System.out.println("Found at Index "+ret);
	}
}

