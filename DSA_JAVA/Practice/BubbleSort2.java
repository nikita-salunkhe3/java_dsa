//Bubble Sort

class Client{
	static int bubbleSort(int arr[]){

		int count=0;
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length-1;j++){
				count++;
				if(arr[j] > arr[j+1]){
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		return count;
	}
	public static void main(String[] args){

		int arr[]=new int[]{5,2,3,1,4};

		System.out.println(Client.bubbleSort(arr));

		System.out.println("Sorted Array Becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
	
		System.out.println();
	}
}

