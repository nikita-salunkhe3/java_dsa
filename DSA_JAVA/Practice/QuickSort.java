class Client{
	int partition(int arr[],int start,int end){
		
		int pivot=arr[end]; //consider pivot element

		int i=start-1;

		for(int j=start;j<end;j++){
			if(arr[j] < pivot){
				i++;
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}
		i++;
		int temp=arr[i];
		arr[i]=arr[end];
		arr[end]=temp;

		return i;
	}

	void quickSort(int arr[],int start,int end){
		
		if(start < end){

			int pivotIndex=partition(arr,start,end);

			quickSort(arr,start,pivotIndex-1);
			quickSort(arr,pivotIndex+1,end);
		}
	}

	public static void main(String[] args){

		int arr[]=new int[]{1,5,2,6,8,4,2};

		Client obj=new Client();

		obj.quickSort(arr,0,arr.length-1);

		System.out.println("Sorted Array Becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}
