//Insertion Sort
//Approach-  
class Client{
	
	static void insertionSortRec(int arr[],int n){

		if(n == 0){
			return;
		}

		insertionSortRec(arr,n-1);
		int j=n-1;
		int ele=arr[n];

		while(j >= 0 && arr[j] > ele){
			arr[j+1]=arr[j];
			j--;
		}
		arr[j+1]=ele;
	}
	public static void main(String[] args){

		int arr[]=new int[]{3,5,7,1,7,4,2};

		Client.insertionSortRec(arr,arr.length-1);
		
		System.out.println("Sorted Array Becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}

}

