class Client{
	static void bubbleSortRec(int arr[],int n){

		if(n == -1){
			return;
		}

		int count=0;
		for(int i=0;i<n;i++){
			if(arr[i] > arr[i+1]){
				count++;
				int temp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
			}
		}
		if(count == 0){
			return;
		}
		bubbleSortRec(arr,n-1);
	}
	public static void main(String[] args){

		int arr[]=new int[]{4,2,4,6,1,42,6,7};

		Client.bubbleSortRec(arr,arr.length-1);

		System.out.println("Sorted Array Becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}


