class Client{
	static void bubbleSort(int arr[],int n){

		if(n == -1){
			return;
		}

		bubbleSort(arr,n-1);
		for(int i=0;i<arr.length-1-n;i++){
			
			if(arr[i] > arr[i+1]){
				int temp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
			}
		}
	}

	public static void main(String[] args){

		int arr[]=new int[]{4,2,5,2,6,34,24,64,1};

		Client.bubbleSort(arr,arr.length-1);

		System.out.println("Sorted Array Becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}

		
