
//write code of factorial in recursion

import java.util.*;

class Client{
	static int factorial(int num){

		if(num == 1){
			return 1;
		}
		return num*factorial(--num);
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		System.out.println("Factorial is: "+factorial(num));
	}
}

