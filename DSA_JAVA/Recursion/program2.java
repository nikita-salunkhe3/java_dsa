import java.util.*;

class Client{
	static int sumofNum(int num){
		if(num == 0){
			return 0;
		}
		return num + sumofNum(--num);
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		int ret=sumofNum(num);

		System.out.println("Sum is :"+ret);
	}
}

