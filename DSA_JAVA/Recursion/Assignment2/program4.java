/*
 Q. 4
Write a program to print the sum of odd numbers up to a given number.
 */

import java.util.*;

class Recursion{

	int sumOfOddNo(int num){

		if(num == 1){
			return 1;
		}
		if(num % 2 == 1){
			return num + sumOfOddNo(num-1);
		}else{
			return sumOfOddNo(num-1);
		}
	}

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		Recursion obj=new Recursion();

		System.out.println("Sum of odd number is: "+obj.sumOfOddNo(num));
	}
}
