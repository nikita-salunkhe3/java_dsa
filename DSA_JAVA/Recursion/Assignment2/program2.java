/*
 Q. 2
Write a program to print the product of digits of a given number.
 */

import java.util.*;
class Recursion{
	static int digitProduct(int num){

		if(num == 0){
			return 1;
		}
		return (num%10) * digitProduct(num/10);
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		System.out.println("Product of Digits is "+Recursion.digitProduct(num));
	}
}
