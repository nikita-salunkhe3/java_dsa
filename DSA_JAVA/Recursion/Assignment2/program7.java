/*
 Q. 7
Write a program to check if a given number is a Magic Number or not. (A Magic
Number is a number in which the eventual sum of the digits is equal to 1).
 */

//Example _=>> 325

import java.util.*;
class Client{

	int sum=0;
	int  validMagic(int num){
		if(num == 0 && sum/10 == 1){
			return sum;
		}
		if(num == 0 && sum/10 != 0){
			num=sum;
			sum=0;
			validMagic(num);
		}
		sum=sum+num%10;
		return validMagic(num/10);
	}
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		Client obj=new Client();
		int ret=obj.validMagic(num);

		System.out.println("ret is "+ret);
		if(ret == 1){
			System.out.println("Magic Number");
		}else{
			System.out.println("Not Magic Number");
		}
	}
}

