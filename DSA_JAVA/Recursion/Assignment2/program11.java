/*
 Q. 10
Write a program to check if a given number is an Armstrong number or not.
( An Armstrong number is a number that is equal to the sum of its own digits each
raised to the power of the number of digits.)
 */

import java.util.*;
class Client{
	int sum=0;
	int powerofNo(int n,int count){
		if(count == 0){
			return 0;
		}
		return n * powerofNo(n,count-1);
	}
	int validArmstrong(int num,int count){

		if(num == 0){
			return sum;
		}
		sum=sum+powerofNo(num%10,count);

		return validArmstrong(num/10,count);
	}
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		int temp=num;
		int count=0;

		while(temp != 0){
			count++;
			temp=temp/10;
		}

		Client obj=new Client();
		int ret=obj.validArmstrong(num,count);

		System.out.println("ret is "+ret);
		if(ret == num){
			System.out.println("Armstrong Number");
		}else{
			System.out.println("Not Armstrong Number");
		}
	}
}
