/*
 Q. 1
Write a program to print the factorial of a given number.
 */

import java.util.*;
class Client{
	int factorial(int num){

		if(num == 1){
			return 1;
		}
		return num * factorial(num-1);
	}
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		Client obj=new Client();
		System.out.println("Factorial is:"+obj.factorial(num));
	}
}
