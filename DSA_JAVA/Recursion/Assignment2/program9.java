/*
 Q. 9
Write a program to determine whether a given number is a happy number or not.
(A happy number is a number which eventually reaches 1 when replaced by the
sum of the square of each digit.)
 */

import java.util.*;
class Recursion{
	int product =0;
	int validHappyNo(int num){
		if(num == 0){
			if(product / 10 == 0){
				return product;
			}else{
				num=product;
				product = 0;
				return validHappyNo(num);
			}
		}
		product = product + (num%10)*(num%10);

		return validHappyNo(num/10);

	}
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		Recursion obj=new Recursion();

		int ret=obj.validHappyNo(num);

		if(ret == 1){
			System.out.println("Happy Number");
		}else{
			System.out.println("Not Happy Number");
		}
	}
}
