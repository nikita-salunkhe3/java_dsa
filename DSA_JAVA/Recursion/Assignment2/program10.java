/*
 Write a program for Happy number
 */

import java.util.*;

class Client{

	int validHappyNo(int num){

		while(num !=0){
			
		//	System.out.println("Num is : "+num);

			int temp=num;
			int sum=0;
			while(temp != 0){
				sum=sum+((temp%10)*(temp%10));

				temp=temp/10;
			}
			if(sum / 10 == 0){
				return sum;
			}else{
				num=sum;
			}
		}
		return 0;
	}
	public static void main(String[] arg){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		Client obj=new Client();

		int ret=obj.validHappyNo(num);

		//System.out.println(ret);

		if(ret == 1){
			System.out.println("Happy Number");
		}else{
			System.out.println("Not Happy Number");
		}
	}
}
