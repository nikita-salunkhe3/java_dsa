/*
 Q. 3
Write a program to print the maximum digit in a given number.
 */

import java.util.*;
class Recursion{
	int max=Integer.MIN_VALUE;

	int maxDigit(int num){
		if(num == 0){
			return max;
		}
		if((num % 10) > max){
			max=num%10;
		}
		return maxDigit(num/10);
	}
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		Recursion obj=new Recursion();

		System.out.println("Max digit is : "+obj.maxDigit(num));
	}
}

