/*
 Q. 8
Write a program to check whether a given positive integer is a Perfect Number or
not.
(A Perfect Number is a positive integer that is equal to the sum of its proper
divisors, excluding itself.)
 */

import java.util.*;
class Recursion{
	int validPerfectNo(int num,int i){

		if(num/2 < i){
			return 0;
		}
		if(num % i == 0){
			return i+validPerfectNo(num,i+1);
		}
		return validPerfectNo(num,i+1);
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		Recursion obj=new Recursion();

		int ret=obj.validPerfectNo(num,1);

		if(ret == num){
			System.out.println("Perfect Number");
		}else{
			System.out.println("Not Perfect Number");
		}
	}
}

