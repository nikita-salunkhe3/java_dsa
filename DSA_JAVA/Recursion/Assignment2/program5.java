/*
 Q. 5
Write a program to check whether the given number is a palindrome number or
not.
 */

import java.util.*;

class Client{
        int rev=0;
        int validPallindrome(int num){
                if(num == 0){
                        return rev;
                }
                rev=rev*10 + num%10;
                return validPallindrome(num/10);
        }

        public static void main(String[] args){

                Scanner sc=new Scanner(System.in);

                System.out.println("Enter the number");

                int num=sc.nextInt();

                Client obj=new Client();

                int ret = obj.validPallindrome(num);

                if(ret==num)
                        System.out.println("Number is Palindrome");
                else
                        System.out.println("Number is not Palindrome");

        }
}

