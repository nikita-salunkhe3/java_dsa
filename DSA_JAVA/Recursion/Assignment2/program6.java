/*
 Q. 6
Write a program to check whether a given number is a Strong Number or not.
 */

import java.util.*;
class Recursion{

	int factorial(int num){
		if(num ==1){
			return num;
		}
		return num * factorial(num-1);
	}
	int validStrong(int num){
		if(num/10 == 0){
			return factorial(num%10);
		}
		return factorial(num % 10) + validStrong(num/10);
	}
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		Recursion obj=new Recursion();

		int ret=obj.validStrong(num);

		System.out.println(ret);
		if(ret == num){
			System.out.println("Strong No");
		}else{
			System.out.println("Not Strong No");
		}
	}
}
