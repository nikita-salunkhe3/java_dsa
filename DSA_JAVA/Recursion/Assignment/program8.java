/*
 8. WAP to count the occurrence of a specific digit in a given number.
 */

import java.util.*;
class Client{

	int count=0;

	int countdigit(int num,int search){

		if(num == 0){
			return count;
		}
		if(num % 10 == search){
			count++;
		}
		return countdigit(num/10,search);
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		System.out.println("Enter occurence element");

		int occ=sc.nextInt();

		Client obj=new Client();

		System.out.println("count of "+occ+" is "+obj.countdigit(num,occ));
	}
}
