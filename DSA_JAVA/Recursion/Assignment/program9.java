/*
 9. WAP to print string in reverse order.
 */

import java.util.*;

class Client{

	static void reverseOrder(char arr[],int start,int end){

		if(start > end){
			return;
		}
		char ch= arr[start];
		arr[start]=arr[end];
		arr[end]=ch;

		reverseOrder(arr,start+1,end-1);
	}

	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the String");

		String str=sc.next();

		char arr[]=str.toCharArray();

		Client.reverseOrder(arr,0,arr.length-1);

		System.out.println(arr);
	}
}
