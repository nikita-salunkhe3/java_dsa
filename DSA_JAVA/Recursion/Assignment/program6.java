/*
 6. WAP to calculate the sum of digits of a given positive integer.
 */


import java.util.*;
class Recursion{

	int sumofDigits(int num){

		if(num == 0){
			return 0;
		}
		return num%10 + sumofDigits(num/10);
	}
	public static void main(String[] args){

		Scanner sc =new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		Recursion obj=new Recursion();

		System.out.println("Sum of Digits: "+obj.sumofDigits(num));
	}
}
