/*
 3. WAP to print the sum of n natural numbers.
 */

import java.util.*;
class Recursion{

	static int sumofN(int num){

		if(num == 1){
			return num;
		}
		return num+sumofN(num-1);
	}
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter natural No.");

		int num=sc.nextInt();

		System.out.println("Sum is : "+Recursion.sumofN(num));
	}
}

