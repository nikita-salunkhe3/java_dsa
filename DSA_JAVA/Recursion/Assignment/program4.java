
/*
 4. WAP to print the length of digits in a number.
 */

import java.util.*;
class Recursion{

	int len = 0;
	int countLength(int num){

		if(num%10 == 0){
			return len;
		}
		if(num % 10 != 0){
			len++;
		}
		return countLength(num/10);
	}	
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the digit");

		int digit=sc.nextInt();

		Recursion obj=new Recursion();

		System.out.println("Count is: "+obj.countLength(digit));
	}
}
