/*
 7. WAP to find the factorial of a number.
 */

import java.util.*;
class Client{

	static int factorial(int num){

		if(num==1){
			return 1;
		}
		return num*factorial(num-1);
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		System.out.println("Factorial is : "+Client.factorial(num));
	}
}
