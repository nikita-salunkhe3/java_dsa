/*
 1. WAP to print the numbers between 1 to 10.
 */

class Client{
	static void printNo(int num){

		if(num == 0){
			return;
		}
		printNo(--num);

		System.out.println(num+1);
	}
	public static void main(String[] args){

		Client.printNo(10);
	}
}


