/*
 5. WAP to check whether the number is prime or not.
 */

import java.util.*;
class Recursion{
	
	boolean validPrime(int num,int i){

		if(num == 1){
			return false;
		}
		if(num/2 < i){
			return true;
		}
		if(num % i == 0){
			return false;
		}
		return validPrime(num, i+1);
	}

	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		Recursion obj=new Recursion();

		boolean ret=obj.validPrime(num,2);

		if(ret == true)
			System.out.println("Number is Prime");
		else
			System.out.println("Number is not prime");

	}
}
