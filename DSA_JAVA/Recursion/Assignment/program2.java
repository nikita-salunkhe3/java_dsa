/*
 2. WAP to display the first 10 natural numbers in reverse order
 */

class Recursion{
	static void printNo(int num){

		if(num == 0){
			return ;
		}
		System.out.println(num);

		printNo(--num);
	}
	public static void main(String[] args){
		
		Recursion.printNo(10);
	}
}
