//Selection Sort in Recursion


class Client{
	static int findminIndex(int arr[],int n){
		
		int minIndex=n;
		if(n == arr.length-1){
			return minIndex;
		}
		if(arr[n+1] < arr[minIndex]){
			minIndex=n;
		}
		return findminIndex(arr,n+1);
	}

	static void selectionSort(int arr[],int n){

		if(n==arr.length-1){
			return;
		}
	//	int minIndex=n;
	//	int occ=0;
	/*	for(int j=n+1;j<arr.length;j++){
			occ++;
			if(arr[j] < arr[minIndex]){
				minIndex=j;
			}
		}*/

		int minIndex=findminIndex(arr,n);

		int temp=arr[n];
		arr[n]=arr[minIndex];
		arr[minIndex]=temp;
	
		selectionSort(arr,n+1);
	}

	public static void main(String[] args){
		
		int arr[]=new int[]{1,5,3,85,3,8,2};

		Client.selectionSort(arr,0);

		System.out.println("sorted Array becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();

	}
}

