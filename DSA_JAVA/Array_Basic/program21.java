/*
 21] First element to occur k times
Given an array of N integers. Find the first element that occurs at least K number
of times.
Example 1:
Input :
N = 7, K = 2
A[] = {1, 7, 4, 3, 4, 8, 7}
Output :
4
Explanation:
Both 7 and 4 occur 2 times.
But 4 is first that occurs 2 times
As at index = 4, 4 has occurred
at least 2 times whereas at index = 6,
7 has occurred at least 2 times.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10^4
1 <= K <= 100
1<= A[i] <= 200
 */

import java.io.*;
class ExceptionallyOdd{
	void oddOcc(int arr[],int occ){

		int flag=0;
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr.length;j++){
				if(arr[i]==arr[j]){
					count++;
				}
			}
			if(count == occ){
				flag=0;
				System.out.println(arr[i]);
			}
		}
		if(flag==0){
			System.out.println("Element is not found");
		}
	}
}
class Client{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter Occurence Number");

		int occ=Integer.parseInt(br.readLine());

		
		ExceptionallyOdd obj=new ExceptionallyOdd();

		obj.oddOcc(arr,occ);
	}
}

		
					

