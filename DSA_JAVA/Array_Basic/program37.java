/*
 37] Move all negative numbers to beginning and positive to end
with constant extra space
An array contains both positive and negative numbers in random order. Rearrange
the array elements so that all negative numbers appear before all positive numbers.
Examples :
Input: -12, 11, -13, -5, 6, -7, 5, -3, -6
Output: -12 -13 -5 -7 -3 -6 11 6 5
 */
import java.io.*;
class Client{
	static void moveEle(int arr[]){

		int j=0;
		for(int k=0;k<arr.length;k++){
		for(int i=0;i<arr.length-1;i++){
			if(arr[i] < 0){
				continue;
			}else if(arr[i] > 0){
				int temp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
			}
		}
		}
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		moveEle(arr);
		System.out.println("Array becomes");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}
