/*
 8] Even occurring elements
Given an array Arr of N integers that contains an odd number of occurrences for all
numbers except for a few elements which are present even number of times. Find
the elements which have even occurrences in the array.
Example 1:
Input:
N = 11
Arr[] = {9, 12, 23, 10, 12, 12,
15, 23, 14, 12, 15}
Output: 12 15 23
Example 2:
Input:
N =5
Arr[] = {23, 12, 56, 34, 32}
Output: -1
Explanation:
Every integer is present odd number of times.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10^5
0 ≤ Arr[i] ≤ 639]
 */

import java.io.*;
class EvenOccurence{
	int even(int arr[],int index){

			int count=0;
			for(int j=index;j<arr.length;j++){
				if(arr[index]==arr[j] || arr[j]!=-1){
					count++;
					arr[j]=-1;
				}
			}
			if(count % 2 == 0){
				return arr[index];
			}
		
			return -1;
	}
}
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array Elements");

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		int flag=0;
		EvenOccurence obj= new EvenOccurence();
		System.out.println("+++++++++++++++++++++++++++++++++++");
		for(int i=0;i<arr.length;i++){
			int result=obj.even(arr,i);
			if(result == -1){
			
			}else{
				flag=1;
				System.out.println(result);
			}
		}
		if(flag==0){
			System.out.println("-1");
		}		
	}
}
	

