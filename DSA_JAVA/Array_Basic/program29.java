/*
 29] Last index of One
Given a string S consisting only of '0's and '1's, find the last index of the '1' present
in it.
Example 1:
Input:
S = 00001
Output:
4
Explanation:
Last index of 1 in the given string is 4.
Example 2:
Input:
0
Output:
-1
Explanation:
Since, 1 is not present, so output is -1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= |S| <= 10^6
S = {0,1}
 */

import java.io.*;
class Client{
	int LastIndex(String str){
		char arr[]=str.toCharArray();

		int store=-1;
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='1'){
				store=i;
			}
		}
		return store;
	}
	public static void main(String[] args)throws IOException{

		System.out.println("Enter String");

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		String str=br.readLine();

		Client obj=new Client();
		int ret=obj.LastIndex(str);

		if(ret==-1){
			System.out.println("1 is Not Occur");
		}else{
			System.out.println("1 is Occur at index : "+ret);
		}
	}
}

