/*
 19] Find common elements in three sorted arrays
Given three Sorted arrays in non-decreasing order, print all common elements in
these arrays.
Examples:
Input:
ar1[] = {1, 5, 10, 20, 40, 80}
ar2[] = {6, 7, 20, 80, 100}
ar3[] = {3, 4, 15, 20, 30, 70, 80, 120}
Output: 20, 80
Input:
ar1[] = {1, 5, 5}
ar2[] = {3, 4, 5, 5, 10}
ar3[] = {5, 5, 10, 20}
Output: 5, 5 
 */

import java.io.*;
class CommonEle{
	void commonEle(int arr1[],int arr2[],int arr3[]){

		int flag=0;
		int flag1=0;
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				for(int k=0;k<arr3.length;k++){
					if(arr1[i]==arr2[j] && arr2[j]==arr3[k] && arr1[i]==arr3[k]){
						flag=1;
						flag1=1;
						System.out.println(arr1[i]);
						break;
					}
				}
				if(flag==1){
					flag=0;
					break;
				}
			}
		}
		if(flag1==0){
			System.out.println("Common Element Not Found");
		}
	}
}
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter first Array size");

		int size1=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr1[]=new int[size1];

		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter Second Array size");

		int size2=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr2[]=new int[size2];

		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter Third Array size");

		int size3=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr3[]=new int[size3];

		for(int i=0;i<arr3.length;i++){
			arr3[i]=Integer.parseInt(br.readLine());
		}
		CommonEle obj=new CommonEle();
		obj.commonEle(arr1,arr2,arr3);
	}
}
		
		
