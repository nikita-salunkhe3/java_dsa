/*
 40] Fibonacci in the array
Given an array arr of size N, the task is to count the number of elements of the
array which are Fibonacci numbers
Example 1:
Input: N = 9, arr[] = {4, 2, 8, 5, 20, 1,
4<F9>0, 13, 23}
Output: 5
Explanation: Here, Fibonacci series will be 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55.
Numbers that are present in array are 2, 8, 5, 1, 13
Example 2:
Input: N = 4, arr[] = {4, 7, 6, 25}
Output: 0
Explanation: No Fibonacci number in
this array.
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 ≤ N ≤ 10^6
 */

/*
 * A number which is present in fibbonacci series can be decided by following formula
 * 5*n*n-4  || 5*n*n+4
 */

import java.io.*;
import java.math.*;
class Client{
	static int fibonacciNo(int arr[]){

		int count=0;
		int a=0;
		int b=1;
		int c=0;

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				if(c==arr[j]){
					count++;
				}
			}
			c=a+b;
			a=b;
			b=c;	
		}
		return count;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array Elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int ret=fibonacciNo(arr);
		if(ret==0){
			System.out.println("Element Not Present");
		}else{
			System.out.println("Fibonacci no. count is: "+fibonacciNo(arr));
		}
	}
}
