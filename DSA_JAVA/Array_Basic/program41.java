/*
 41]Countries at war
The two countries of A and B are at war against each other. Both countries have N
number of soldiers. The power of these soldiers are given by A[i]...A[N] and
B[i]....B[N].
These soldiers have a peculiarity. They can only attack their counterpart enemies,
like A[i] can attack only B[i] and not anyone else. A soldier with higher power can
kill the enemy soldier. If both soldiers have the same power, they both die. You
need to find the winning country.
Example 1:
Input : a[ ] = {2, 2}, b[ ] = {5, 5}
Output : B
Explanation:
Both countries have 2 soldiers.
B[0] kills A[0], B[1] kills A[1].
A has 0 soldiers alive at the end.
B has both soldiers alive at the end.
Return "B" as a winner.
Example 2:
Input : a[ ] = {9}, b[ ] = {8}
Output : A
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 ≤ N ≤ 10^5
0 ≤ Ai ≤ 10^7
0 ≤ Bi ≤ 10^7
 */

import java.io.*;
class Client{
	static int countryWar(int arr1[],int arr2[]){

		int flag=-1;
		if(arr1.length==arr2.length){
			for(int i=0;i<arr1.length;i++){
				if(arr1[i] > arr2[i]){
					flag=0;
				}else{
					flag=1;
				}
			}
		}else if(arr1.length > arr2.length){
			flag=0;
		}else{
			flag=1;
		}
		return flag;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size for 1st array");

		int size1=Integer.parseInt(br.readLine());

		int arr1[]=new int[size1];

		System.out.println("Enter array elements");

		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter array size for 2nd array");

		int size2=Integer.parseInt(br.readLine());

		int arr2[]=new int[size2];

		System.out.println("Enter array elements");

		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		int ret=countryWar(arr1,arr2);
		if(ret==0){
			System.out.println("A Country is win");
		}else{
			System.out.println("B Country is win");
		}
	}
}

