/*
 5] Replace all 0's with 5
You are given an integer N. You need to convert all zeros of N to 5.
Example 1:
Input:
N = 1004
Output: 1554
Explanation: There are two zeroes in 1004
on replacing all zeroes with "5", the new
number will be "1554"


Expected Time Complexity: O(K) where K is the number of digits in N
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 10000
 */

import java.io.*;

class Client{
	public static void main(String[] Nikita)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number");

		int num = Integer.parseInt(br.readLine());

		int rev=0;
		while(num != 0){
			int rem = num%10;
			if(rem==0){
				rem = 5;
			}
			rev=rev*10+rem;
			num=num/10;
		}
//		System.out.println("rev: "+rev);

		int rev1=0;
		while(rev != 0){
			int rem = rev%10;
			rev1=rev1*10+rem;
			rev=rev/10;
		}
		System.out.println("Replace Number is: "+rev1);
	}
}

