/*
 13] Find unique element
Given an array of size n which contains all elements occurring in multiples of K,
except one element which doesn't occur in multiple of K. Find that unique element.
Example 1:
Input :
n = 7, k = 3
arr[] = {6, 2, 5, 2, 2, 6, 6}
Output :
5
Explanation:
Every element appears 3 times except 5.
Example 2:
Input :
n = 5, k = 4
arr[] = {2, 2, 2, 10, 2}
Output :
10
Explanation:
Every element appears 4 times except 10.
Expected Time Complexity: O(N. Log(A[i]) )
Expected Auxiliary Space: O( Log(A[i]) )
Constraints:
3<= N<=2*10^5
2<= K<=2*10^5
1<= A[i]<=10^9
 */

import java.io.*;
class UniqueEle{
	int uniqueEle(int arr[],int occ,int num){

		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==num){
				count++;
			}
		}
		if(count==occ){
			return 0;
		}
		return count;
	}
}
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Array Elements are");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter Occurance number");

		int flag=0;
		int occ=Integer.parseInt(br.readLine());

		UniqueEle obj=new UniqueEle();

		for(int i=0;i<arr.length;i++){
			int count=obj.uniqueEle(arr,occ,arr[i]);
			if(count == 1){
				flag=1;
				System.out.println(arr[i]);
			}
		}
		if(flag==0){
			System.out.println("-1");
		}
	}
}

