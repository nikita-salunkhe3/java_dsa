/*
 23] Find the smallest and second smallest element in an array
Given an array of integers, your task is to find the smallest and second smallest
element in the array. If smallest and second smallest do not exist, print -1.
Example 1:
Input :
5
24356
Output :
23
Explanation:
2 and 3 are respectively the smallest
and second smallest elements in the array.
Example 2:
Input :
6
121367
Output :
12
Explanation:
1 and 2 are respectively the smallest
and second smallest elements in the array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1<=N<=10^5
1<=A[i]<=10^5
 */

import java.io.*;
class SecondSmall{
	void secsmall(int arr[]){

		for(int i=0;i<arr.length-1;i++){
			for(int j=i+1;j<arr.length;j++){
				if(arr[i] > arr[j]){
					int temp = arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}

		System.out.println("Smallest Element is: "+arr[0]);
		System.out.println("Second Smallest Element is: "+arr[1]);
	}
}
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		SecondSmall obj=new SecondSmall();

		obj.secsmall(arr);
	}
}

