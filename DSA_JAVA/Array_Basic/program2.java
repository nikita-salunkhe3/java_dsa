//Find Minimum and Maximum element in an array

import java.io.*;

class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size = Integer.parseInt(br.readLine());

		int []arr = new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int min=arr[0];
		int max=arr[0];

		for(int i=0;i<arr.length;i++){
			if(arr[i]<min){
				min=arr[i];
			}
			if(arr[i]>max){
				max=arr[i];
			}
		}
		System.out.println("Maximum elements is: "+max);
		System.out.println("Minimum elements is: "+min);
	}
}
