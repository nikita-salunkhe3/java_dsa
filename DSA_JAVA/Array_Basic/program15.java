/*
 15] Sum of distinct elements
You are given an array Arr of size N. Find the sum of distinct elements in an array.
Example 1:
Input:
N =5
Arr[] = {1, 2, 3, 4, 5}
Output: 15
Explanation: Distinct elements are 1, 2, 3
4, 5. So the sum is 15.
Example 2:
Input:
N =5
Arr[] = {5, 5, 5, 5, 5}
Output: 5
Explanation: Only Distinct element is 5.
So the sum is 5.
Expected Time Complexity: O(N*logN)
Expected Auxiliary Space: O(N*logN)
Constraints:
1 ≤ N ≤ 10^7
0 ≤ A[i] ≤ 10^4
 */


import java.io.*;
class SumOfDistictNo{
	int sumofNo(int arr[]){

		int sum=0;

		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				if(arr[i]==arr[j]){
					arr[i]=0;
				}
			}
			sum=sum+arr[i];
		}
		return sum;
	}
}
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		SumOfDistictNo obj = new SumOfDistictNo();
		int sum=obj.sumofNo(arr);

		System.out.println("Sum of Distinct Array element is : "+sum);
	}
}

