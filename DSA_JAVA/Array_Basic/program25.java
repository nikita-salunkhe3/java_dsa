/*
 25] Maximum product of two numbers
Given an array Arr of size N with all elements greater than or equal to zero. Return
the maximum product of two numbers possible.
Example 1:
Input:
N =6
Arr[] = {1, 4, 3, 6, 7, 0}
Output: 42
Example 2:
Input:
N =5
Arr = {1, 100, 42, 4, 23}
Output: 4200
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
2 ≤ N ≤ 10^7
0 ≤ Arr[i] ≤ 10^4
 */

import java.io.*;

class MaxProduct{
	int maxproduct(int arr[]){

		for(int i=0;i<arr.length-1;i++){
			for(int j=i+1;j<arr.length;j++){
				if(arr[i] > arr[j]){
					int temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
		return arr[arr.length-1]*arr[arr.length-2];
	}
}
class Client{
	public static void main(String[] nikita)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		MaxProduct obj=new MaxProduct();
		System.out.println("Maximum Product is: "+obj.maxproduct(arr));
	}
}
