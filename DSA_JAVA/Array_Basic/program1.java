//Search an Element in an Array

import java.io.*;
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of an array");

		int size=Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements");

		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter search Element");

		int search = Integer.parseInt(br.readLine());

		for(int i=0;i<arr.length;i++){
			if(arr[i] == search){
				System.out.println("Element Found at index : "+i);
				break;
			}
		}
	}
}



