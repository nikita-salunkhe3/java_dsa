/*
 39] Leaders in an array
Write a program to print all the LEADERS in the array. An element is a leader if it
is greater than all the elements to its right side. And the rightmost element is
always a leader.
For example:
Input: arr[] = {16, 17, 4, 3, 5, 2},
Output : 17, 5, 2
Input: arr[] = {1, 2, 3, 4, 5, 2},
Output: 5, 2
 */

import java.io.*;
class Client{
	static void leaderArray(int arr[]){

		for(int i=1;i<arr.length;i++){
			if(i<arr.length-1){
				if(arr[i]>arr[i+1] && arr[i]>arr[i-1]){
					System.out.print(arr[i]+"  ");
				}
			}else{
				System.out.print(arr[i]+"  ");
			}
		}
		System.out.println();
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		leaderArray(arr);
	}
}
