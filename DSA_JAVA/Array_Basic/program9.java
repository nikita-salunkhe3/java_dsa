/*
 9] Remove an Element at Specific Index from an Array
Given an array of a fixed length. The task is to remove an element at a specific
index from the array.
Examples 1:
Input: arr[] = { 1, 2, 3, 4, 5 }, index = 2
Output: arr[] = { 1, 2, 4, 5 }
Examples 2:
Input: arr[] = { 4, 5, 9, 8, 1 }, index = 3
Output: arr[] = { 4, 5, 9, 1 }
 */

import java.io.*;
class RemoveEle{
	void removeEle(int arr[],int index){

		if(index == arr.length-1){
			arr[index]=0;
		}else{
			for(int i=index;i<arr.length-1;i++){

				arr[i]=arr[i+1];
			
			}
		}
	}
}
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter index to remove the element");

		int num=Integer.parseInt(br.readLine());

		RemoveEle obj=new RemoveEle();

		obj.removeEle(arr,num);

		System.out.println("Array Becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]);
		}
		System.out.println();
	}
}


