/*<F10>
 28] Remove Duplicates from unsorted array
Given an array of integers which may or may not contain duplicate elements. Your
task is to remove duplicate elements, if present.
Example 1:
Input:
N =6
A[] = {1, 2, 3, 1, 4, 2}
Output:
1234
Example 2:
Input:
N =4
A[] = {1, 2, 3, 4}
Output:
1234
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1<=N<=10^5
1<=A[i]<=10^5
 */

import java.io.*;

class RemoveDupEle{

	void removedupele(int arr[]){
		//int arr1[]=new int[arr.length];
		
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=i+1;j<arr.length;j++){
				if(arr[i] == arr[j]){
					//count++;
					arr[j]=0;
				}
			}/*
			if(count == 0){
				arr[k]=arr[i];
				k++;
			}*/
		}
		//int arr1[]=new int[arr.length - count];
		System.out.println("Array Becomes:");
		for(int i=0;i<arr.length;i++){
			if(arr[i]!= 0)
				System.out.println(arr[i]);
		}

	}
}
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		RemoveDupEle obj=new RemoveDupEle();

		obj.removedupele(arr);
	}
}

