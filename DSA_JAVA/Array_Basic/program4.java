//Find Product of Array elements

import java.io.*;

class Client{
	public static void main(String[] Nikita)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("enter array elements");

		int product=1;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			product=product*arr[i];
		}
		System.out.println("Product of Array Element is : "+product);
	}
}

