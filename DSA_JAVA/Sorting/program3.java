//Merge Sort

import java.util.*;
class MergeSort{
	void merge(int arr[],int start,int mid,int end){
		
		int size1=mid-start+1;
		int size2=end-mid;

		int arr1[]=new int[size1];
		int arr2[]=new int[size2];

		for(int i=0;i<arr1.length;i++){
			arr1[i]=arr[start+i];
		}

		for(int i=0;i<arr2.length;i++){
			arr2[i]=arr[mid+i+1];
		}

		int i=0;
		int j=0;
		int k=start;

		while(i<arr1.length && j < arr2.length){

			if(arr1[i] < arr2[j]){
				arr[k]=arr1[i];
				i++;
			}else{
				arr[k]=arr2[j];
				j++;
			}
			k++;
		}

		while(i < arr1.length){
			arr[k]=arr1[i];
			i++;
			k++;
		}
		while(j < arr2.length){
			arr[k]=arr2[j];
			j++;
			k++;
		}
	}
	void mergeSort(int arr[],int start,int end){

		if(start >= end){
			return;
		}
		int mid=start+(end-start)/2;

		mergeSort(arr,start,mid);
		mergeSort(arr,mid+1,end);
		merge(arr,start,mid,end);
	}

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter the array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		MergeSort obj=new MergeSort();

		int start=0;
		int end=arr.length-1;
		obj.mergeSort(arr,start,end);

		System.out.println("Sorted Array becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}



