//Selection Sort

class SelectionSort{
	void selsort(int arr[]){
		
		for(int i=0;i<arr.length-1;i++){
			int minIndex=i;
			for(int j=i+1;j<arr.length;j++){
				if(arr[j]< arr[minIndex]){
					minIndex=j;
				}
			}
			int temp=arr[i];
			arr[i]=arr[minIndex];
			arr[minIndex]=temp;
		}

	}
	public static void main(String[] args){

		int arr[]=new int[]{4,2,5,26,83,1};

		SelectionSort obj=new SelectionSort();

		obj.selsort(arr);

		System.out.println("Sorted Array becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}
