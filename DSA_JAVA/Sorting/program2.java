//Bubble Sort
//Optimized Way

class BubbleSort{
	void bubblesort(int arr[]){

		for(int i=0;i<arr.length;i++){
			int flag=0;
			for(int j=0;j<arr.length-1-i;j++){
				if(arr[j] > arr[j+1]){
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
					flag=1;
				}
			}
			if(flag==0){
				break;
			}
		}
	}

	public static void main(String[] args){
		
		int arr[]=new int[]{10,3,2,6,4,8,5,1};

		BubbleSort obj=new BubbleSort();
		obj.bubblesort(arr);

		System.out.println("Sorted Array becomes");

		for(int i=0;i<arr.length;i++){
		
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}
