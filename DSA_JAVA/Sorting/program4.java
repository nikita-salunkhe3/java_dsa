//Quick Sort

import java.io.*;
import java.util.Arrays;

class QuickSort{
	int partition(int arr[],int start,int end){
		
		int pivot=arr[end];
		int i=start-1;

		for(int j=start;j<end;j++){
			if(arr[j] < pivot){
				i++;
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}
		i++;
		int temp=arr[i];
		arr[i]=arr[end];
		arr[end]=temp;

		return i;

	}
	void quicksort(int arr[],int start,int end){

		if(start < end){

			int pivotIndex=partition(arr,start,end);
			quicksort(arr,start,pivotIndex-1);
			quicksort(arr,pivotIndex+1,end);
		}

	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of an array");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		QuickSort obj=new QuickSort();
		obj.quicksort(arr,0,arr.length-1);

		System.out.println("Sorted Array becomes");

		System.out.println(Arrays.toString(arr));

	}
}
