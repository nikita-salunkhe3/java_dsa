/*
 *Brute force approach
 */

import java.util.*;
class ArrayDemo{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int count=0;

		TreeSet ts=new TreeSet();
		for(int i=0;i<arr.length;i++){
			ts.add(arr[i]);
		}
		Iterator itr= ts.iterator();

		while(itr.hasNext()){
			int val=(int)itr.next();
			for(int i=0;i<arr.length;i++){
				if(val < arr[i]){
					count++;
					break;
				}
			}
		}
		System.out.println("Output :"+count);
	}
}



