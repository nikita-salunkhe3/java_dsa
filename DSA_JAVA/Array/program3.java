/*
 Q. Given an Array of size N count the no of elements having atleast 1 element is greater than itself
 arr[]:[2,5,1,4,8,0,8,1,3,8]

 Optimized Way 
 */
import java.io.*;
class Client{
	static int greaterEle(int arr[]){
	
		int count=0;
		int max=arr[0];
		for(int i=1;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		for(int i=0;i<arr.length;i++){
			if(max==arr[i]){
				count++;
			}
		}
		return arr.length-count;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println(greaterEle(arr));

	}
}

