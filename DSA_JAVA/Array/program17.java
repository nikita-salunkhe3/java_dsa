/*
 * Given an integer array of size N
 * Build an array rightMax of size N
 * rightmax of i contains the maximum of the index i to index N-1
 *
 * arr:[-3  6  2  4  5  2  8  -9  3  1]
 * N=10;
 * rightmax:[8  8  8  8  8  8  8  3  3  1]
 *
 *
 * BRUTEFORCE APPROACH
 */

import java.util.*;

class Client{

	public static void main(String[] args){

		Scanner sc =new Scanner(System.in);

		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int max=Integer.MIN_VALUE;

		int rightmax[]=new int[arr.length];

		rightmax[arr.length-1]=arr[arr.length-1];

		for(int i=arr.length-2;i>=0;i--){             ///////////  Optimized Way
			if(arr[i] > rightmax[i+1]){
				rightmax[i]=arr[i];
			}else{
				rightmax[i]=rightmax[i+1];
			}
		}
/*
		for(int i=arr.length-2;i>=0;i--){                   #### Optimized way
			if(max < arr[i]){
				max=arr[i];
			}
			rightmax[i]=max;
		}
			*/
		/*
		for(int i=arr.length-1;i>=0;i--){                     #####  BruteForce approach

			for(int j=arr.length-1;j>=i;j--){

				if(arr[j] > max){
					max=arr[j];
				}
			}
			rightmax[i]=max;
		}*/

		System.out.println("Rightmax array becomes");

		for(int i=0;i<rightmax.length;i++){
			System.out.println(rightmax[i]);
		}
	}
}



