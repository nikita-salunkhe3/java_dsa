/*
 * Given an integer array of size N.
 * Build an array leftmax of size N.
 * leftmax if i contains the maximum for the index 0 to index i;
 *
 * arr:[-3, 6  2  4  5  2  8  -9  8  1]
 * N=10;
 * leftmax:[-3  6  6  6  6  8  8  8  8]
 */

import java.util.*;
class LeftMaxArray{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int max=Integer.MIN_VALUE;

		int leftmax[]=new int[arr.length];
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<=i;j++){

				if(arr[j]>max){
					max=arr[j];
				}
			}
			leftmax[i]=max;
		}

		System.out.println("leftmax array becomes");

		for(int i=0;i<leftmax.length;i++){
			System.out.println(leftmax[i]);
		}
	}
}
