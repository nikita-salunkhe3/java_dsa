/*
 Equilibrium index of an array
 Array A:[-7,1,5,2,-4,3,0]
output: 3

Array A:[1,2,3]
output: -1


return minimum equilibrium index
 */

import java.util.*;
class Client{
	static int equilibriumIndex(int arr[]){

		for(int i=0;i<arr.length;i++){
			int leftsum=0;
			int rightsum=0;

			for(int j=0;j<i;j++){
				leftsum = leftsum + arr[j];
			}
			for(int j=i+1;j<arr.length;j++){
				rightsum = rightsum + arr[j];
			}
			if(leftsum==rightsum){
				return i;
			}
		}
		return -1;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter array size");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int ret=equilibriumIndex(arr);
		if(ret==-1){
			System.out.println("No any Pair found");
		}else{
			System.out.println("Pair count is: "+ret);
		}
	}
}




