/*
 Given an array of size N return the count of the pair[i,j] with arr[i]+arr[j]=k
 */
/****Brute Force approach*****/

import java.io.*;

class Client{

	static int pairCount(int arr[],int sum){

		int count=0;
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				if(arr[i]+arr[j]==sum && i!=j){
					count++;
				}
			}
		}
		return count;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter pair sum");

		int sum=Integer.parseInt(br.readLine());

		System.out.println("Sum is: "+pairCount(arr,sum));
	}
}


