class Solution {
    public int[] twoSum(int[] nums, int target) {
        
        int arr[]=new int[2];
        for(int i=0;i<nums.length-1;i++){
            if(arr[i]+arr[i+1] == target){
                arr[0]=i;
                arr[1]=i+1;
                break;
            }
        }
        return arr;
    }
    public static void main(String[] args){
	    Solution obj=new Solution();
	    int arr[]=new int[]{1,2};
	    int target=3;
	    int ret[]=obj.twoSum(arr,target);
	    for(int i=0;i<ret.length;i++){
		    System.out.println(ret[i]);
	    }
    }
}
