/*
 Given an array of size N return the count of the pair[i,j] with arr[i]+arr[j]=k
 */
/****Optimized Code Approach*********/
/********
 * ya code la O(N) madhe Optimized fkt Hashing cha Concept ne OR Two Pointer chya concept ne solve hoto*/

import java.io.*;
class Client{

	static int pairCount(int arr[],int sum){
	
		int count=0;	
		int j=arr.length-1;
		for(int i=0;i<arr.length-1;i++){
			if(arr[i]+arr[j]==sum){
				count++;
				j--;
			}
			if(j>0 && i==arr.length-1){
				i=-1;
				j--;
			}
		}
		return count;
	}
	 public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                System.out.println("Enter array elements");

                int arr[]=new int[size];

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                System.out.println("Enter pair sum");

                int sum=Integer.parseInt(br.readLine());

                System.out.println("Pairs are: "+pairCount(arr,sum));
        }

}
