/***
 * find Square root of given number
 */

import java.util.*;
class Client{

	static int squareRoot(int num){

		int ans=0;
		int itr=0;
		int start=1;
		int end=num;

		while(start <= end){
			itr++;
			int mid=start + end/2;
			int sqr=mid*mid;

			if(sqr == num){
				System.out.println(itr);
				return mid;
			}
			if(sqr > num){
				end=mid-1;
			}
			if(sqr < num){
				start = mid+1;
				ans = mid;
			}
		}
		System.out.println(itr);
		return ans;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();
		
		System.out.println(squareRoot(num));
	}
}


