/*
 find Second Large element form the array
 */
import java.io.*;
class Client{
	static int secondLarge(int arr[]){

		int max=arr[0];
		for(int i=1;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		int secmax=arr[0];
		for(int i=0;i<arr.length;i++){
			if(arr[i]>secmax && arr[i]!=max){
				secmax=arr[i];
			}
		}
		return secmax;
	}
	public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                System.out.println("Second large element is: "+secondLarge(arr));
        }


}
