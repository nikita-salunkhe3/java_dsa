/*
 * Write a code to print the sum of all elements presnt in array using prefix sum
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int presum[]=new int[size];

		presum[0]=arr[0];

		for(int i=1;i<presum.length;i++){
			presum[i]=presum[i-1]+arr[i];
		}
		System.out.println("Sum of all elements are:"+presum[presum.length-1]);
	}
}
