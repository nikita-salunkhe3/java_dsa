/*
 * Prefix Sum keva Vaperaych 
 * jeva range madhe dilelya elements chi sum pahije asel teva prefix Sum used keraych
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements:");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter number of Queries");

		int Q=sc.nextInt();

		int presum[]=new int[arr.length];

		presum[0]=arr[0];

		for(int i=1;i<presum.length;i++){
			presum[i]=presum[i-1]+arr[i];
		}

		for(int i=1;i<=Q;i++){
			int sum=0;
			System.out.println("Enter the starting index");
	
			int start=sc.nextInt();

			if(start < 0 || start > arr.length-1){
				System.out.println("Invalid Index");
				continue;
			}

			System.out.println("Enter ending Index");

			int end=sc.nextInt();

			if(end > arr.length-1 || end < 0){
				System.out.println("Invalid Index");
				continue;
			}
			if(start == 0){
				sum=presum[end];
			}else{
				sum=presum[end]-presum[start-1];
			}
			System.out.println("Sum is "+sum);
		}
	}
}
