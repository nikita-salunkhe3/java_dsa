/*
 return sum of pair 
 */

import java.io.*;
class Client{

	static int pairSum(int arr[]){
		int sum=0;
		int j=arr.length-1;
		if(arr.length % 2==0){
			for(int i=0;i<(arr.length-1)/2;i++){
				sum=sum + (arr[i]+arr[j]);
				j--;
			}
		}else{
			for(int i=0;i<=(arr.length-1)/2;i++){
				sum=sum + (arr[i]+arr[j]);
				j--;
			}
		}
		return sum;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Sum of pair is : "+pairSum(arr));
	}
}
