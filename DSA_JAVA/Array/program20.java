/*
SubArray: 
arr:[1,2,3,1,3,4,6,4,6,3]
output:4
 */

class Client{
	public static void main(String[] args){

		int arr[]=new int[]{1,2,3,1,3,4,6,3};

		int min=Integer.MAX_VALUE;
		int max=Integer.MIN_VALUE;

		for(int i=0;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			}
			if(arr[i]<min){
				min=arr[i];
			}
		}
		int minlen=Integer.MAX_VALUE;
		
		int len=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==min){
				for(int j=i+1;j<arr.length;j++){
					if(arr[j]==max){
						len=j-i+1;
						if(minlen > len ){
							minlen=len;
						}
					}
				}
			}else{
				if(arr[i]==max){
					for(int j=i+1;j<arr.length;j++){
						if(arr[j]==min){
							len=j-i+1;
						}
						if(minlen > len){
							minlen=len;
						}
					}
				}
			}
		}
		System.out.println("Minimun length is: "+len);
	}
}
