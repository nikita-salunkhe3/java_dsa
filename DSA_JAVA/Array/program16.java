
import java.util.*;

class Client{
	static int max(int num, int max){

		if(num > max){
			return num;
		}else{
			return max;
		}
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter size of an array ");

		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		int leftmax[]=new int[arr.length];

		int max=Integer.MIN_VALUE;

		leftmax[0]=arr[0];
/*
		for(int i=1;i<arr.length;i++){

			if(arr[i] > max){
				max=arr[i];
			}
			leftmax[i]= max(leftmax[i-1], max);
		}*/

		for(int i=1;i<arr.length;i++){
			if(leftmax[i-1] < arr[i]){
				leftmax[i]=arr[i];
			}else{
				leftmax[i]=leftmax[i-1];
			}
		}
		System.out.println("Array becomes");

		for(int i=0;i<leftmax.length;i++){
			System.out.println(leftmax[i]);
		}
	}
}


