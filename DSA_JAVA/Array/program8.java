/*
 *  Find Second Largest Element in the array
 */
/*********Brute Force Approach***********/

import java.io.*;
class Client{

	static int secondLarge(int arr[]){
		
		for(int i=0;i<arr.length-1;i++){
			if(arr[i]>arr[i+1]){
				int temp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
				i=-1;
			}
		}

		int j=arr.length-1;
		while(arr[j-1]==arr[j]){
			j--;
		}
		return arr[j-1];
	}

	public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                System.out.println("Second large element is: "+secondLarge(arr));
        }
}

