/*
 * Given an array size N reverse the array
 */

/********Brute Force approach*********/

import java.io.*;
class Client{

	static int[] reverseArray(int arr[]){

		int arr1[]=new int[arr.length];

		int j=0;
		for(int i=arr.length-1;i>=0;i--){
			arr1[j]=arr[i];
			j++;
		}

		return arr1;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int arr1[]=reverseArray(arr);

		System.out.println("Reverse Array becomes");

		for(int i=0;i<arr1.length;i++){
			System.out.println(arr1[i]);
		}
	}
}

