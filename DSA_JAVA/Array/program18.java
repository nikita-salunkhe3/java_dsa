//
//Given an character array (lower case)
//return thr count of pair(i,j) such that,
//-->>>  i < j
//--->>  arr[i]='a'
//--->>  arr[j]='g'
//
//arr:[a  b  e  g   a   g]
//output: 3

import java.util.*;
class Client{
	public static void main(String[] args){

		char arr[]=new char[]{'a','b','e','g','a','g'};

		char ch1='a';
		char ch2='g';

		/*
		 *bruteForce Approach
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(ch1 == arr[i]){
		
				for(int j=i+1;j<arr.length;j++){

					if(arr[j] == ch2){
						count++;
					}
				}
			}
		}
		*/

		//Optimized Approach

		int count=0;
		int pair=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'){
				count++;
			}else if(arr[i]=='g'){
				pair=pair+count;
			}
		}
		System.out.println("Pair count is :"+pair);
	}
}



