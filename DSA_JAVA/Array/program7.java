/*
 
 */

import java.io.*;
class Client{
	
	static void reverseArray(int arr[]){

		int j=arr.length-1;
		for(int i=0;i<(arr.length-1)/2;i++){
			int temp=arr[i];
			arr[i]=arr[j];
			arr[j]=temp;
			j--;
		}
	}
	public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                reverseArray(arr);

                System.out.println("Reverse Array becomes");

                for(int i=0;i<arr.length;i++){
                        System.out.println(arr[i]);
                }
        }
}
