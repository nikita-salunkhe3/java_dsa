/*
 Q. Given an Array of size N count the no of elements having atleast 1 element is greater than itself
 arr[]:[2,5,1,4,8,0,8,1,3,8]

 brute force
 */

import  java.io.*;
class Client{
	static int greaterEle(int arr[]){

		int count=0;
		int  max=arr[0];
		for(int i=0;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}

		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				if(arr[i]>arr[j] && arr[i]!=max){
					count++;
					break;
				}
			}
		}
		return count+1;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println(greaterEle(arr));
	}
}
