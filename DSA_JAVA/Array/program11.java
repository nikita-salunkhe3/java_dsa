/*
 Find Factors of given number
 */

import java.io.*;
class Client{
	static int findFactor(int num){

		int count=0;
		for(int i=1;i*i<=num;i++){
			if(num%i==0){
				if(num/i==i){
					count=count+1;
				}else{
					count=count+2;
				}
			}
		}
		return count;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number");

		int num=Integer.parseInt(br.readLine());

		System.out.println("Factor count are: "+findFactor(num));
	}
}
