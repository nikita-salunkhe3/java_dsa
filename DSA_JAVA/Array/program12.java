/*
 * Q.Write a code to find the squareroot of given number
 */

import java.io.*;
class Client{
	//brute Force
	static int squareRoot(int num){
		int val=0;
		for(int i=1;i<=num;i++){
			if(i*i==num)
				val=i;
		}
		return val;
	}

	//Optimized    Time complexity===>>>> 0(sq.root(N))
	static int sqR(int num){

		int val=0;
		for(int i=1;i<=num/2;i++){
			if(i*i <= num){
				val=i;
			}else{
				break;
			}
		}
		return val;
	}
				
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number");

		int num=Integer.parseInt(br.readLine());

		int ret1=squareRoot(num);
		if(ret1==0){
			System.out.println("Not Perfect sq. root");
		}else{
			System.out.println("Perfect sq. root");
		}
		
		int ret2=sqR(num);
		System.out.println(ret2);
	}
}

