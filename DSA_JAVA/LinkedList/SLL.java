import java.util.*;
class Node{
	int data;
	Node next=null;
	
	Node(int data){
		this.data=data;
	}
}
class LinkedList{
	Node head=null;
		
	int countNode(){
		Node temp=head;
		int count=0;
		while(temp != null){
			count++;
			temp=temp.next;
		}
		return count;
	}
	void addFirst(int data){
		Node newnode=new Node(data);

		if(head == null){
			head=newnode;
		}else{
			newnode.next = head;
			head=newnode;
		}
	}
	void addLast(int data){
		Node newnode = new Node(data);

		if(head==null){
			head=newnode;
		}else{
			Node temp=head;
			while(temp.next != null){
				temp=temp.next;
			}
			temp.next=newnode;
		}
	}
	int addAtPos(int pos,int data){
		if(pos <= 0 || pos >= countNode()+2){
			return -1;
		}else{
			if(head==null){
				addFirst(data);
			}else if(countNode()+1 == pos){
				addLast(data);
			}else{
				Node newnode=new Node(data);

				Node temp=head;
				while(pos-2 != 0){
					temp=temp.next;
					pos--;
				}
				newnode.next=temp.next;
				temp.next=newnode;
			}
		}
		return 0;
	}
	int deleteFirst(){
		if(head==null){
			return -1;
		}else if(head.next == null){
			head=null;
		}else{
			head=head.next;
		}
		return 0;
	}
	int deleteLast(){
		if(head==null){
			return -1;
		}else if(head.next == null){
			head=null;
		}else{
			Node temp=head;
			while(temp.next.next != null){
				temp=temp.next;
			}
			temp.next=null;
		}
		return 0;
	}
	int deleteAtPos(int pos){
		if(pos <= 0 || pos >= countNode()+2){
			return -1;
		}else if(pos == 1){
			deleteFirst();
		}else if(countNode() == pos){
			deleteLast();
		}else{
			Node temp=head;
			while(pos-2 != 0){
				temp=temp.next;
			}
			temp.next=temp.next.next;
		}
		return 0;
	}
	int printLL(){
		if(head==null){
			return -1;
		}else{
			Node temp=head;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
		return 0;
	}
}
class Client{
	public static void main(String[] args){
		
		char ch;
		Scanner sc=new Scanner(System.in);

		LinkedList obj=new LinkedList();

		do{
			System.out.println("1. addFirst");
			System.out.println("2. addLast");
			System.out.println("3. addAtPos");
			System.out.println("4. deleteFirst");
			System.out.println("5. deleteLast");
			System.out.println("6. deleteAtPos");
			System.out.println("7. printLL");
			System.out.println("8. countNode");

			System.out.println("Enter your choice");

			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();
						obj.addFirst(data);
					}
					break;
				case 2:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();
						obj.addLast(data);
					}
					break;
				case 3:
					{
						System.out.println("Enter position");
						int pos=sc.nextInt();
						System.out.println("Enter data");
						int data=sc.nextInt();

						int ret=obj.addAtPos(pos,data);
						if(ret == -1){
							System.out.println("Wrong Position");
						}
					}
					break;
				case 4:
					{
						int ret=obj.deleteFirst();
						if(ret ==-1){
							System.out.println("LinkedList is empty");
						}
					}
					break;
				case 5:
					{
						int ret=obj.deleteLast();
						if(ret == -1){
							System.out.println("Empty LinkedList");
						}
					}break;
				case 6:
					{
						System.out.println("Enter position");
						int pos=sc.nextInt();

						int ret=obj.deleteAtPos(pos);
						if(ret == -1){
							System.out.println("Empty LL");
						}
					}
					break;
				case 7:
					{
						int ret=obj.printLL();
						if(ret == -1){
							System.out.println("Empty Linkedlist");
						}
					}
					break;
				case 8:
					{
						int ret=obj.countNode();
						if(ret==-1){
							System.out.println("LL Empty");
						}else{
							System.out.println("Count of Node is : "+ret);
						}
					}
					break;
				default:
					System.out.println("Invalid Choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}




						


