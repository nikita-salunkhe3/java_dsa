//Doubly Linked List

import java.util.*;
class Node{
	
	Node prev=null;
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}
class LinkedList{
	
	Node head=null;

	int countNode(){
		
		int count=0;
		Node temp=head;
		while(temp != null){
			count++;
			temp=temp.next;
		}
		return count;
	}
	void addFirst(int data){

		Node newnode=new Node(data);

		if(head==null){
			head=newnode;
		}else{
			newnode.next=head;
			head.prev=newnode;
			head=newnode;
		}
	}
	void addLast(int data){

		Node newnode=new Node(data);

		if(head==null){
			head=newnode;
		}else{
			Node temp=head;
			while(temp.next != null){
				temp=temp.next;
			}
			newnode.prev=temp;
			temp.next=newnode;
		}
	}
	void addAtPos(int data,int pos){

		if(pos <= 0 || pos >= countNode()+2){
			System.out.println("Invalid position");
			return;
		}
		if(pos == 1){
			addFirst(data);
		}else if(pos == countNode()+1){
			addLast(data);
		}else{
			Node newnode = new Node(data);

			Node temp=head;
			while(pos-2 != 0){
				temp=temp.next;
				pos--;
			}
			newnode.next=temp.next;
			newnode.next.prev=newnode;
			newnode.prev=temp;
			temp.next=newnode;
		}
	}
	void delFirst(){

		if(head == null){
			System.out.println("Empty LL");
			return;
		}else if(head.next == null){
			head=null;
		}else{
			head=head.next;
			head.prev=null;
		}
	}
	void delLast(){
		if(head==null){
			System.out.println("Empty Linked List");
			return;
		}else if(head.next == null){
			head=null;
		}else{
			Node temp=head;
			while(temp.next.next != null){
				temp=temp.next;
			}
			temp.next=null;
		}
	}
	void delAtPos(int pos){
		if(pos <= 0 || pos >= countNode()+2){
			System.out.println("Invalid Position");
			return;
		}else{
			if(pos == 1){
				delFirst();
			}else if(pos == countNode()){
				delLast();
			}else{
				Node temp=head;
				while(pos-2 != 0){
					temp=temp.next;
					pos--;
				}
				temp.next.next.prev=temp;
				temp.next=temp.next.next;
			}
		}
	}
	void printDLL(){
		if(head==null){
			System.out.println("Empty LinkedList");
		}else{
			Node temp=head;
			while(temp != null){
				System.out.print(temp.data+"  ");
				temp=temp.next;
			}
			System.out.println();
		}
	}
}			

class DoublyLL{
	public static void main(String[] args){

		LinkedList ll=new LinkedList();

		Scanner sc=new Scanner(System.in);

		char ch;
		do{
			System.out.println("Doubly linkedlist");
                        System.out.println("1. addFirst");
                        System.out.println("2. addLast");
                        System.out.println("3. addAtPos");
                        System.out.println("4. delFirst");
                        System.out.println("5. delLast");
                        System.out.println("6. delAtPos");
                        System.out.println("7. countNode");
                        System.out.println("8. printDLL");

                        System.out.println("Enter your Choice");
                        int choice=sc.nextInt();

                        switch(choice){
                                case 1:
                                        {
                                                System.out.println("Enter the data");
                                                int data=sc.nextInt();
                                                ll.addFirst(data);
                                        }
                                        break;
                                case 2:
                                        {
                                                System.out.println("Enter the data");
                                                int data=sc.nextInt();
                                                ll.addLast(data);
                                        }
                                        break;
                                case 3:
                                        {
                                                System.out.println("Enter the data");
                                                int data=sc.nextInt();
                                                System.out.println("Enter the position");
                                                int pos=sc.nextInt();

                                                ll.addAtPos(data,pos);
                                        }
                                        break;
                                case 4:
                                        ll.delFirst();
                                        break;
                                case 5:
                                        ll.delLast();
                                        break;
                                case 6:
                                        {
                                                System.out.println("Enter the position");
                                                int pos=sc.nextInt();
                                                ll.delAtPos(pos);
                                        }
                                        break;
                                case 7:
                                        {
                                                int count=ll.countNode();
                                                System.out.println(count);
                                        }
                                        break;
                                case 8:
                                        ll.printDLL();
                                        break;
                                default:
                                        System.out.println("Invalid Choice");
                        }
                        System.out.println("Do you want to continue");
                        ch=sc.next().charAt(0);
                }while(ch == 'y' || ch == 'Y');
        }
}


