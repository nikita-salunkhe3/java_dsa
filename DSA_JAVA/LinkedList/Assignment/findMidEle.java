/*
 25. Finding middle element in a linked list
Given a singly linked list of N nodes.
The task is to find the middle of the linked list. For example, if the linked list is
1-> 2->3->4->5, then the middle node of the list is 3.
If there are two middle nodes(in case, when N is even), print the second middle element.
For example, if the linked list given is 1->2->3->4->5->6, then the middle node of the list is 4.
Example 1:
Input:
LinkedList: 1->2->3->4->5
Output: 3
Explanation: Middle of the linked list is 3.
Example 2:
Input:
LinkedList: 2->4->6->7->5->1
Output: 7
Explanation:
Middle of the linked list is 7.
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 <= N <= 5000
 */

import java.util.*;
class findMidEle{

	Node head=null;
        void addNode(int data){

                Node newnode = new Node(data);

                if(head==null){
                        head=newnode;
                }else{
                        Node temp=head;
                        while(temp.next != null){
                                temp=temp.next;
                        }
                        temp.next = newnode;
                }
        }

        void printLL(){
                if(head == null){
                        System.out.println("Empty LL");
                        return;
                }else{
                        Node temp=head;
                        while(temp.next != null){
                                System.out.print("|"+temp.data+"| -> ");
                                temp=temp.next;
                        }
                        System.out.println("|"+temp.data+"|");
                }
        }
	int findmid(){
		if(head == null){
			System.out.println("Empty LinkedList");
			return -1;
		}else{
			Node fast=head.next;
			Node slow=head;

			while( fast != null && fast.next != null){
				fast = fast.next;
				if(fast != null){
					fast = fast.next;
				}
				slow = slow.next;
			}
			slow=slow.next;
			return slow.data;
		}
	}
}
class Client{
	public static void main(String[] args){

	
		char ch;

		Scanner sc=new Scanner(System.in);


     		findMidEle obj=new findMidEle();

                do{
                        System.out.println("1. addNode");
                        System.out.println("2. printLL");
                        System.out.println("3. findmid");

                        System.out.println("Enter your choice");
                        int choice=sc.nextInt();

                        switch(choice){
                                case 1:
                                        {
                                                System.out.println("Enter the data");
                                                int data=sc.nextInt();
                                                obj.addNode(data);

                                        }
                                        break;
                                case 2:
                                        obj.printLL();
                                        break;
                                case 3:{
                                                int val=obj.findmid();
						if(val == -1){
							System.out.println("Empty LinkedList");
						}else{

							System.out.println("Mid is : "+val);
						}
                                        }
                                        break;
                                default:
                                        System.out.println("Invalid Choice");
                        }
                        System.out.println("Do you want to continue");
                        ch=sc.next().charAt(0);
                }while(ch == 'Y' || ch == 'y');
        }
}
