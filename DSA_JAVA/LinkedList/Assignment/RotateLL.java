/*
 16. Rotate a Linked List
Given a singly linked list of size N. The task is to left-shift the linked list by k nodes, where k is a
given positive integer smaller than or equal to length of the linked list.
Example 1:
Input:
N=5
value[] = {2, 4, 7, 8, 9}
k=3
Output: 8 9 2 4 7
Explanation:
Rotate 1: 4 -> 7 -> 8 -> 9 -> 2
Rotate 2: 7 -> 8 -> 9 -> 2 -> 4
Rotate 3: 8 -> 9 -> 2 -> 4 -> 7
Example 2:
Input:
N=8
value[] = {1, 2, 3, 4, 5, 6, 7, 8}
k=4
Output: 5 6 7 8 1 2 3 4
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 <= N <= 10^3
1 <= k <= N
 */

import java.util.*;
class Node{
	int data;
	Node next =null;

	Node(int data){
		this.data=data;
	}
}
class RotateLL{

	Node head=null;
	void addNode(int data){

		Node newnode = new Node(data);

		if(head==null){
			head=newnode;
		}else{
			Node temp=head;
			while(temp.next != null){
				temp=temp.next;
			}
			temp.next = newnode;
		}
	}

	void printLL(){
		if(head == null){
			System.out.println("Empty LL");
			return;
		}else{
			Node temp=head;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
	}
	void rotate(){
		Node temp=head;
		Node val=head.next;

		while(temp.next != null){
			temp=temp.next;
		}
		temp.next = head;
		head.next = null;
		head=val;

		printLL();
	}
	void rotateLL(int key){

		if(head == null){
			System.out.println("Empty LinkedList");
		}else{
			for(int i=0;i<key;i++){
				rotate();
			}
		}
	}
}
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		char ch;

		RotateLL obj=new RotateLL();

		do{
			System.out.println("1. addNode");
			System.out.println("2. printLL");
			System.out.println("3. rotateLL");

			System.out.println("Enter your choice");
			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();
						obj.addNode(data);

					}
					break;
				case 2:
					obj.printLL();
					break;
				case 3:{
					       	System.out.println("Enter the Number of rotation");
					       	int key=sc.nextInt();
						obj.rotateLL(key);
						System.out.println("Rotated LinkedList becomes: ");
						obj.printLL();
					}
					break;
				default:
					System.out.println("Invalid Choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'Y' || ch == 'y');
	}
}
