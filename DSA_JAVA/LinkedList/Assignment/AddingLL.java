/*
 6. Add two numbers represented by Linked List
Given two numbers represented by two linked lists, write a function that returns a Sum list. The
sum list is a linked list representation of addition of two input numbers.
Example 1:
Input:
S1 = 3, S2 = 3
ValueS1 = {2,3,4}
ValueS2 = {3,4,5}
Output: 5 7 9
Explanation: After adding the 2 numbers the resultant number is 5 7 9.Example 2:
Input:
S1 = 1, S2 = 2
ValueS1 = {9}
ValueS2 = {8,7}
Output: 9 6
Explanation: Add 9 and 7 we get 16. 1 is carried here and is added to 8. So the answer is 9 6
Constraints:
1 <= S1, S2 <= 100
 */

import java.util.*;
class AddingNoLL{
	
}
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter size for first LinkedList");
		int first=sc.nextInt();

		LinkedList<Integer> l1=new LinkedList<Integer>();
		
		System.out.println("Enter data for First LinkedList");
		for(int i=0;i<first;i++){
			int val=sc.nextInt();
			l1.add(val);
		}

		System.out.println("Enter size for Second LinkedList");
		int sec=sc.nextInt();

		LinkedList<Integer> l2=new LinkedList<Integer>();
		
		System.out.println("Enter data for Second LinkedList");
		for(int i=0;i<sec;i++){
			int val=sc.nextInt();
			l2.add(val);
		}
		System.out.println(l1);
		System.out.println(l2);

		LinkedList<Integer> l3=new LinkedList<Integer>();

		if(first == sec){
			for(int i=first-1; i>=0;i--){
				System.out.println(l1.peek());
				System.out.println(l2.peek());
				l3.add((l1.peek()+l2.peek()));
			}
		}
		System.out.println(l3);
	}
}
