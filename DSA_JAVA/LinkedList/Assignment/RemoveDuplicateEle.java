/*
 9. Remove duplicate element from sorted Linked List
Given a singly linked list consisting of N nodes. The task is to remove duplicates (nodes with
duplicate values) from the given list (if exists).Note: Try not to use extra space. The nodes are arranged in a sorted way.
Example 1:
Input:
LinkedList: 2->2->4->5
Output: 2 4 5
Explanation: In the given linked list 2 ->2 -> 4-> 5, only 2 occurs more than 1 time. So we need
to remove it once.
Example 2:
Input:
LinkedList: 2->2->2->2->2
Output: 2
Explanation: In the given linked list 2 ->2 ->2 ->2 ->2, 2 is the only element and is repeated 5
times. So we need to remove any four 2.
Expected Time Complexity : O(N)
Expected Auxiliary Space : O(1)
Constraints:
1 <= Number of nodes <= 10^5
 */

import java.util.*;
class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}

class RemoveDuplicate{
	Node head=null;

	void addNode(int data){
		Node newnode=new Node(data);

		if(head==null){
			head=newnode;
		}else{
			Node temp=head;
			while(temp.next != null){
				temp=temp.next;
			}
			temp.next=newnode;
		}
	}
	void printLL(){
		if(head==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node temp=head;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
	}

	void removedup(){
		if(head==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node forward=head;
			Node current=head;

			while(forward.next != null){
				forward=current.next;
				if(current.data >= forward.data){

					current.next=forward.next;	
				}else{
					current=current.next;
				}
			}
		}
	}

}
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		RemoveDuplicate obj=new RemoveDuplicate();
		
		char ch;
		do{
			System.out.println("1. addNode");
			System.out.println("2. printLL");
			System.out.println("3. removedup");

			System.out.println("Enter your choice");
			int choice=sc.nextInt();

			switch(choice){
				case 1:{					       	
						System.out.println("Enter the data");
						int data=sc.nextInt();
						obj.addNode(data);
					}
					break;
				case 2:
					obj.printLL();
					break;
				case 3:
					obj.removedup();
					break;
				default:
					System.out.println("Invalid Choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch =='Y');
	}
}
