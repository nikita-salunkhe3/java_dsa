/*
 1. Count nodes of linked list
Given a singly linked list. The task is to find the length of the linked list, where length is defined
as the number of nodes in the linked list.
Example 1:
Input:
LinkedList: 1->2->3->4->5
Output: 5
Explanation: Count of nodes in the linked list is 5, which is its length.
Example 2:
Input:
LinkedList: 2->4->6->7->5->1->0
Output: 7
Explanation: Count of nodes in the linked list is 7. Hence, the output
is 7.
Expected Time Complexity : O(N)
Expected Auxiliary Space : O(1)
Constraints:
1 <= N <= 10^5
1 <= value <= 10^3
*/

import java.util.*;
class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}

class CountNode{
	
	Node head=null;

	int countNode(){
		int count=0;
		Node temp=head;
		if(head==null){
			return -1;
		}else{
			while(temp.next != null){
				count++;
				temp=temp.next;
			}
		}
		return ++count;
	}
	void addNode(int data){
		Node newnode=new Node(data);

		if(head==null){
			head=newnode;
		}else{
			Node temp=head;
			while(temp.next != null){
				temp=temp.next;
			}
			temp.next=newnode;
		}
	}
	int printLL(){
		if(head==null){
			return -1;
		}else{
			Node temp=head;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
		return 0;
	}
}
class Client{
	public static void main(String[] args){

		char ch;
		Scanner sc=new Scanner(System.in);

		CountNode obj=new CountNode();
		do{
			System.out.println("1. addNode");
			System.out.println("2. countNode");
			System.out.println("3. printLL");
			System.out.println("enter your choice");

			int choice=sc.nextInt();

			switch(choice){
				case 1:{
						System.out.println("Enter the data");
						int data=sc.nextInt();
						obj.addNode(data);
					}
					break;
				case 2:{
						int count=obj.countNode();
						if(count==0){
							System.out.println("Empty LinkedList");
						}else{
							System.out.println("Count of Node is "+count);
						}
					}
				       	break;
				case 3:
					{
						int ret=obj.printLL();
						if(ret == -1){
							System.out.println("Empty LinkedList");
						}
					}
					break;
				default:
					System.out.println("Invalid choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}

						

