/*
 15. Remove duplicates from an unsorted linked list
Given an unsorted linked list of N nodes. The task is to remove duplicate elements from this
unsorted Linked List. When a value appears in multiple nodes, the node which appeared first
should be kept, all other duplicates are to be removed.
Example 1:
Input:
N=4
value[] = {5,2,2,4}
Output: 5 2 4
Explanation:Given linked list elements are 5->2->2->4, in which 2 is repeated only. So, we will
delete the extra repeated elements 2 from the linked list and the resultant linked list will contain
5->2->4
Example 2:
Input:
N=5
value[] = {2,2,2,2,2}
Output: 2
Explanation:Given linked list elements are 2->2->2->2->2, in which 2 is repeated. So, we will
delete the extra repeated elements 2 from the linked list and the resultant linked list will contain
only 2.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= size of linked lists <= 10^60 <= numbers in list <= 10^4
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		LinkedList<Integer> ll = new LinkedList<Integer>();

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of LinkedList");
		int size=sc.nextInt();

		System.out.println("Enter elements in linkedList");
		for(int i=1;i<=size;i++){
			int val=sc.nextInt();
			ll.add(val);
		}

		Set<Integer> s = new TreeSet<Integer>(ll);

		System.out.println("Sorted LL with no duplicates element is : "+s);
	}
}

