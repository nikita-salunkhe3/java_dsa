/*
 14. Given a linked list of 0s, 1s and 2s, sort it.
Given a linked list of N nodes where nodes can contain values 0s, 1s, and 2s only. The task is to
segregate 0s, 1s, and 2s linked lists such that all zeros segregate to the head side, 2s at the
end of the linked list, and 1s in the middle of 0s and 2s.
Example 1:
Input:
N=8
value[] = {1,2,2,1,2,0,2,2}
Output: 0 1 1 2 2 2 2 2
Explanation: All the 0s are segregated to the left end of the linked list, 2s to the right end of the
list, and 1s in between.
Example 2:
Input:
N=4value[] = {2,2,0,1}
Output: 0 1 2 2
Explanation: After arranging all the 0s,1s and 2s in the given format, the output will be 0 1 2 2.
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(N).
Constraints:
1 <= N <= 10^6
 */

import java.util.*;
class Node{
	int data;
	Node next = null;

	Node(int data){
		this.data=data;
	}
}
class SortLinkedList{	

	Node head=null;

	void addNode(int data){
		Node newnode = new Node(data);

		if(head==null){
			head=newnode;
		}else{
			Node temp=head;
			while(temp.next != null){
				temp=temp.next;
			}
			temp.next=newnode;
		}
	}
	LinkedList sortLL(){

		LinkedList<Integer> ll = new LinkedList<Integer>();

		if(head==null){
			System.out.println("Empty Linkedlist Nothing to sort data");
			return null;
		}else{
			Node temp=head;
			while(temp != null){
				if(temp.data == 0){
					ll.add(temp.data);
				}
				temp=temp.next;
			}
			Node temp1=head;

			while(temp1 != null){
				if(temp1.data == 1){
					ll.add(temp1.data);
				}
				temp1=temp1.next;
			}

			Node temp2=head;

			while(temp2 != null){
				if(temp2.data == 2){
					ll.add(temp2.data);
				}
				temp2=temp2.next;
			}
		}
		return ll;
	}
	void printLL(){
		if(head==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node temp=head;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
	}
}
class Client{
	public static void main(String[] args){

		char ch;

		SortLinkedList obj=new SortLinkedList();

		Scanner sc=new Scanner(System.in);

		do{
			System.out.println("1. addNode");
			System.out.println("2. printLL");
			System.out.println("3. sortLL");

			System.out.println("Enter your choice");
			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();

						obj.addNode(data);
					}
					break;
				case 2:
					obj.printLL();
					break;

				case 3:
					System.out.println("Sorted LL is : "+obj.sortLL());
					break;
				default:
					System.out.println("Invalid Choice");
					break;

			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}

