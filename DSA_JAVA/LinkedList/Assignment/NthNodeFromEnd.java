/*
 13. Nth node from end of linked list
Given a linked list consisting of L nodes and given a number N. The task is to find the Nth node
from the end of the linked list.
Example 1:
Input:
N=2LinkedList: 1->2->3->4->5->6->7->8->9
Output: 8
Explanation: In the first example, there are 9 nodes in the linked list and we need to find the 2nd
node from the end. the 2 nd node from the end is 8.
Example 2:
Input:
N=5
LinkedList: 10->5->100->5
Output: -1
Explanation: In the second example, there are 4 nodes in the linked list and we need to find 5th
from the end. Since 'n' is more than the number of nodes in the linked list, the output is -1.
Note:
Try to solve it in a single traversal.
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 <= L <= 10^6
1 <= N <= 10^6
 */
import java.util.*;
class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}
class NthNode{
	
	Node head=null;

	int countNode(){
		if(head == null){
			System.out.println("Empty Linkedlist");
		}
		Node temp=head;
		int count=0;
		while(temp != null){
			count++;
			temp=temp.next;
		}
		return count;
	}
	void addNode(int data){

		Node newnode=new Node(data);

		if(head==null){
			head=newnode;
			return;
		}else{
			Node temp=head;
			while(temp.next != null){
				temp=temp.next;
			}
			temp.next=newnode;
		}
	}
	void printLL(){
		if(head==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node temp=head;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
	}
	int findNthNode(int key){
		if(head==null){
			System.out.println("Empty LinkedList");
		}else{
			int count=countNode();

			if(key > count || key <= 0){
				return -1;
			}
			Node temp=head;
			int val=count-key;
			while(val != 0){
				temp=temp.next;
				val--;

			}
			return temp.data;
		}
		return 0;
	}
}
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		char ch;

		NthNode obj=new NthNode();

		do{

			System.out.println("1. addNode");
			System.out.println("2. printLL");
			System.out.println("3. findNthNode");

			System.out.println("Enter your choice");
			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();

						obj.addNode(data);
					}
					break;
				case 2:
					{
						obj.printLL();
					}
					break;
				case 3:
					{
						System.out.println("Enter nth number");
						int num=sc.nextInt();
						int ret=obj.findNthNode(num);
						if(ret == -1){
							System.out.println("Invalid key");
						}else{
							System.out.println("Nth Node is :"+ret);
						}
					}
					break;
				default:
					System.out.println("Invalid choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}


