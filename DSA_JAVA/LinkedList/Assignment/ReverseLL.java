/*
 5. Reverse a linked list
Given a linked list of N nodes. The task is to reverse this list.
Example 1:
Input:
LinkedList: 1->2->3->4->5->6
Output: 6 5 4 3 2 1
Explanation: After reversing the list, elements are 6->5->4->3->2->1.
Example 2:
Input:
LinkedList: 2->7->8->9->10
Output: 10 9 8 7 2
Explanation: After reversing the list, elements are 10->9->8->7->2.
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 <= N <= 10^4
 */

//Approach -1 : Implace reverse using Iteration

import java.util.*;
class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}
class ReverseLL{
	Node head=null;
	void addNode(int data){	
		Node newnode=new Node(data);

		if(head==null){
			head=newnode;
		}else{
			Node temp=head;
			while(temp.next != null){
				temp=temp.next;
			}
			temp.next=newnode;
		}
	}

	void printLL(){
		if(head==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node temp=head;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
	}

	void reverseLL(){
		if(head==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node prev=null;
			Node current=head;
			Node forward=null;

			while(current != null){
				forward=current.next;
				current.next=prev;
				prev=current;
				current=forward;
			}
			head=prev;
		}
	}
	
}
class Client{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
	
		ReverseLL obj=new ReverseLL();	
		char ch;
		do{

			System.out.println("1. addNode");
			System.out.println("2. reversell");
			System.out.println("3. printLL");

			System.out.println("Enter your choice");
			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();
						obj.addNode(data);
					}
					break;
				case 2:
					{
						obj.reverseLL();
					}
					break;
				case 3:
					{
						obj.printLL();
					}
					break;
				default:
					System.out.println("Invalid choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}


