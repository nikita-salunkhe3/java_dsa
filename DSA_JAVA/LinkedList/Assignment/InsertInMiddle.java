/*
 11. Insert in Middle of Linked List
Given a linked list of size N and a key. The task is to insert the key in the middle of the linked
list.
Example 1:
Input:
LinkedList = 1->2->4
key = 3
Output: 1 2 3 4
Explanation: The new element is inserted after the current middle element in the linked list.
Example 2:
Input:
LinkedList = 10->20->40->50
key = 30
Output: 10 20 30 40 50
Explanation: The new element is inserted after the current middle element in the linked list and
Hence, the output is 10 20 30 40 50.
Expected Time Complexity : O(N)
Expected Auxiliary Space : O(1)
Constraints:
1 <= N <= 10^4
 */

import java.util.*;
class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}
class InsertInMiddle{

	Node head=null;	
	void addNode(int data){
		Node newnode=new Node(data);

		if(head==null){
			head=newnode;
		}else{
			Node temp=head;
			while(temp.next != null){
				temp=temp.next;
			}
			temp.next=newnode;
		}
	}

	void printLL(){
		if(head==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node temp=head;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
	}
	void insertInMiddle(int data){
		Node newnode=new Node(data);

		if(head==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node slow=head;
			Node fast=head.next;

			while(fast != null){
				fast=fast.next;
				if(fast == null){
					break;
				}
				if(fast.next != null){
					fast=fast.next;
				}
				slow=slow.next;
			}
			newnode.next=slow.next;
			slow.next=newnode;
		}
	}
}
class Client{
	public static void main(String[] args){
		char ch;

		Scanner sc=new Scanner(System.in);
		
		InsertInMiddle obj=new InsertInMiddle();

		do{
			System.out.println("1. addNOde");
			System.out.println("2. printLL");
			System.out.println("3. InsertInMiddle");

			System.out.println("Enter your Choice");
			int choice= sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();

						obj.addNode(data);
					}
					break;
				case 2:
					{
						obj.printLL();
					}
					break;
				case 3:
					{
						System.out.println("Enter Inserted Data");
						int data=sc.nextInt();

						obj.insertInMiddle(data);
					}
					break;
				default:
					System.out.println("Invalid Choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch=='y' || ch=='Y');
	}
}
