/*
 10. Identical Linked Lists
Given two Singly Linked List of N and M nodes respectively. The task is to check whether two
linked lists are identical or not.
Two Linked Lists are identical when they have the same data and with the same arrangement
too.
Example 1:
Input:
LinkedList1: 1->2->3->4->5->6
LinkedList2: 99->59->42->20
Output: Not identical
Example 2:
Input:
LinkedList1: 1->2->3->4->5
LinkedList2: 1->2->3->4->5Output: Identical
Constraints:
1 <= N <= 10^3
Expected Time Complexity : O(N)
Expected Auxiliary Space : O(1)
 */

import java.util.*;
class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}
	
class IdenticalLL{

	Node head1=null;
	Node head2=null;

	void addNode1(int data){
		Node newnode=new Node(data);

		if(head1==null){
			head1=newnode;
		}else{
			Node temp=head1;
			while(temp.next != null){
				temp=temp.next;
			}
			temp.next=newnode;
		}
	}
	void printLL1(){
		if(head1==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node temp=head1;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
	}
	void addNode2(int data){

		Node newnode=new Node(data);

		if(head2==null){
			head2=newnode;
		}else{
			Node temp=head2;
			while(temp.next != null){
				temp=temp.next;
			}
			temp.next=newnode;
		}
	}
	void printLL2(){
		if(head2==null){
			System.out.println("Empty LinkedList");
			return;
		}else{
			Node temp=head2;
			while(temp.next != null){
				System.out.print("|"+temp.data+"| -> ");
				temp=temp.next;
			}
			System.out.println("|"+temp.data+"|");
		}
	}
	boolean isIdentical(){
		if(head1==null && head2==null){
			System.out.println("Empty LinkedList");
			return false;
		}else{
			if(head1 == null || head2 == null){
				return false;
			}else{
				Node temp1=head1;
				Node temp2=head2;

				while(temp1 != null && temp2 != null){
					if(temp1.data != temp2.data){
						return false;
					}
					temp1=temp1.next;
					temp2=temp2.next;
				}
				if(temp1 == null && temp2 == null){
					return true;
				}else{
					return false;
				}
			}
		}
	}

}
class Client{
	public static void main(String[] args){

		char ch;

		IdenticalLL obj=new IdenticalLL();
		Scanner sc=new Scanner(System.in);

		do{
			System.out.println("1. adding data in LinkList 1");
			System.out.println("2. adding data in LinkList 2");
			System.out.println("3. print data in LinkList 1");
			System.out.println("4. print data in LinkList 2");
			System.out.println("5. isIdentical");

			System.out.println("Enter your choice");
			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();

						obj.addNode1(data);
					}
					break;
				case 2:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();

						obj.addNode2(data);
					}
					break;
				case 3:
					{
						obj.printLL1();
					}
					break;
				case 4:
					obj.printLL2();
					break;
				case 5:
					{
						boolean val=obj.isIdentical();
						if(val){
							System.out.println("Identical");
						}else{
							System.out.println("Not Identical");
						}
					}
					break;
				default:
					System.out.println("Invalid Choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}
