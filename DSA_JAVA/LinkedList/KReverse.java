import java.util.*;
class Node{
        int data;
        Node next=null;

        Node(int data){
                this.data=data;
        }
}
class LinkedList{

        Node head=null;

        void addNode(int data){

                Node newnode=new Node(data);

                if(head==null){
                        head=newnode;
                }else{
                        Node temp=head;
                        while(temp.next != null){
                                temp=temp.next;
                        }
                        temp.next=newnode;
                }
        }
	void kreverse(int k){
		
		if(prev==null){
			return;
		}
		int val=k;
		Node current = head;
		Node forward=null;
		Node prev=null;

		while(val != 0){
			forward=current.next;
			current.next=prev;
			prev=current;
			current=forward;
			val--;
		}
		if(current != null){
			head.next=kreverse(current,prev);
			return prev;
		}
	}

	int printLL(){
                if(head==null){
                        return -1;
                }else{
                        Node temp=head;
                        while(temp.next != null){
                                System.out.print("|"+temp.data+"| -> ");
                                temp=temp.next;
                        }
                        System.out.println("|"+temp.data+"|");
                }
                return 0;
        }
}
class Client{
        public static void main(String[] args){

                LinkedList obj=new LinkedList();

                char ch;

                Scanner sc=new Scanner(System.in);

                do{
                        System.out.println("1. addNode");
                        System.out.println("2. kreverse");
                        System.out.println("4. printLL");

                        System.out.println("Enter your choice");
                        int choice=sc.nextInt();

                        switch(choice){
                                case 1:
                                        {
                                                System.out.println("Enter the data");
                                                int data=sc.nextInt();
                                                obj.addNode(data);
                                        }
                                        break;
                                case 2:
                                        obj.kreverse();
					break;
                                case 4:
                                        {
                                                int ret=obj.printLL();
                                                if(ret == -1){
                                                        System.out.println("Empty LL");
                                                }
                                        }
                                        break;
                                default:
                                        System.out.println("Invalid choice");
                        }
                        System.out.println("Do you want to continue");
                        ch=sc.next().charAt(0);
                }while(ch == 'y' || ch == 'Y');
        }
}


