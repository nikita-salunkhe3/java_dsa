//Operation to be implemented in array 
//1. enqueue
//2. dequeue
//3. printQueue
//4. front
//5. rear
//6. empty

import java.util.*;
class QueueImp{
	int queueArr[];
	int maxSize;
	int front;
	int rear;

	QueueImp(int size){
		queueArr = new int[size];
		maxSize=size;
		front=-1;
		rear=-1;
	}
	void enqueue(int data){

		if(rear == maxSize-1){
			System.out.println("Queue is full");
			return;
		}else{
			if(rear == -1){
				front++;
			}
			rear++;
			queueArr[rear]=data;
		}
	}
	int dequeue(){
		if(rear == -1){
			System.out.println("Nothing to Popped");
			return -1;
		}else{
			int val=queueArr[front];

			front++;
			if(front > rear){
				rear=front=-1;
			}
			return val;
		}
	}
	boolean empty(){
		if(front == -1){
			return true;
		}else{
			return false;
		}
	}
	int front(){
		if(front ==-1){
			return -1;
		}else{
			return queueArr[front];
		}
	}
	int rear(){
		if(rear == -1){
			return -1;
		}else{
			return queueArr[rear];
		}
	}
	void printQueue(){
		if(rear==-1){
			System.out.println("Empty Queue");
			return;
		}else{
			for(int i=front;i<=rear;i++){
				System.out.print(queueArr[i]);
			}
			System.out.println();
		}
	}
}
class Client{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of array");

		int size=sc.nextInt();

		char ch;

		QueueImp obj=new QueueImp(size);

		do{
			System.out.println("1. enqueue");	
			System.out.println("2. dequeue");	
			System.out.println("3. front");	
			System.out.println("4. rear");	
			System.out.println("5. empty");	
			System.out.println("6. printQueue");

			System.out.println("Enter your choice");
			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter the data");
						int data=sc.nextInt();
						obj.enqueue(data);
					}
					break;
				case 2:
					{
						int ret=obj.dequeue();
						if(ret != -1){
							System.out.println(ret + " is popped");
						}
					}
					break;
				case 3:
					{
						int val=obj.front();
						if(val != -1){
							System.out.println("Front is "+val);
						}
					}
					break;
				case 4:
					{
						int val=obj.rear();
						if(val != -1){
							System.out.println("rear is "+val);
						}
					}
					break;
				case 5:
					{
						boolean val = obj.empty();
						if(val){
							System.out.println("Queue is empty");
						}else{
							System.out.println("Queue is not empty");
						}
					}
					break;
				case 6:
					{
						obj.printQueue();
						
					}
					break;
				default:
					System.out.println("Invalid choice");
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch == 'y' || ch == 'Y');
	}
}

