/*
 Que 10 : First Repeating Element
Given an array arr[] of size n, find the first repeating element. The element should occur
more than once and the index of its first occurrence should be the smallest.
Note:- The position you return should be according to 1-based indexing.
Example 1:
Input:
n=7
arr[] = {1, 5, 3, 4, 3, 5, 6}
Output: 2
Explanation: 5 is appearing twice and its first appearance is at index 2 which is
less than 3 whose first occurring index is 3.
Example 2:
Input:
n=4
arr[] = {1, 2, 3, 4}
Output: -1
Explanation: All elements appear only once so the answer is -1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= n <= 10 6
0 <= A i <= 10 6
 */

import java.io.*;
class Client{
	static int firstRepeatEle(int arr[]){

		int store=-1;
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){
				if(arr[i]==arr[j]){
					int num=j;
					store=num;
					if(i!=j){
						return store;
					}
				}		
			}
		}
		return -1;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println(firstRepeatEle(arr));
	}

}


				
