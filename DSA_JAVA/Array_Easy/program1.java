/*
 Que 1 : Missing number in array
Given an array of size N-1 such that it only contains distinct integers in the range of 1 to
N. Find the missing element.
Example 1:
Input:
N=6
A[] = {1,2,4,5,6}
Output: 3
Example 2:
Input:
N = 11
A[] = {1,3,2,5,6,7,8,11,10,4}
Output: 9
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10 6
1 ≤ A[i] ≤ 10 6
 */

import java.io.*;
class Client{
	static int MissingNo(int arr[]){

		for(int i=arr[0];i<=arr[arr.length-1];i++){
			int flag=0;
			for(int j=0;j<arr.length;j++){
				if(i==arr[j]){
					flag=1;
					break;
				}
			}
			if(flag==0){
				return i;
			}
		}
		return 0;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int ret=MissingNo(arr);
		if(ret==0){
			System.out.println("No any element is Missing");
		}else{
			System.out.println("Missing element is:"+ret);
		}
	}
}



