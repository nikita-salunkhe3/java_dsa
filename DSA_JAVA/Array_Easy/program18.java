/*
 Que 18 : Move all zero two the end of Array
Given an array arr[] of N positive integers. Push all the zeros of the given array to the
right end of the array while maintaining the order of non-zero elements.
Example 1:
Input:
N=5
Arr[] = {3, 5, 0, 0, 4}
Output: 3 5 4 0 0
Explanation: The non-zero elements preserve their order while the 0 elements are
moved to the right.
Example 2:
Input:
N=4
Arr[] = {0, 0, 0, 4}
Output: 4 0 0 0
Explanation: 4 is the only non-zero element and it gets moved to the left.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 105
0 ≤ arr i ≤ 105
 */

import java.io.*;
class Client{
	static void moveAllZero(int arr[]){

		int j=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==0){
				j=i;
				while(arr[j] == 0 && j<arr.length){
					j++;
				}
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		moveAllZero(arr);

		System.out.println("Array becomes");

		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}



