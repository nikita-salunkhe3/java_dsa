/*
 Que 3 : Find Duplicates in an Array
Given an array of size N which contains elements from 0 to N-1, you need to find all the
elements occurring more than once in the given array. Return the answer in ascending
order. If no such element is found, return list containing [-1].
Note: The extra space is only for the array to be returned. Try and perform all operations
within the provided array.
Example 1:
Input:
N=4
a[] = {0,3,1,2}
Output:
-1
Explanation:
There is no repeating element in the array. Therefore output is -1.
Example 2:
Input:
N=5
a[] = {2,3,1,2,3}
Output:
23
Explanation:
2 and 3 occur more than once in the given array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10 5
0 <= A[i] <= N-1, for each valid i
 */

import java.io.*;
class Client{
	static int[] findDuplicateNo(int arr[]){

		for(int i=0;i<arr.length-1;i++){
			if(arr[i] > arr[i+1]){
				int temp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
				i=-1;
			}
		}
		int k=0;
		int arr1[]=new int[arr.length];
		int flag=0;
		for(int i=0;i<arr.length-1;i++){
			if(arr[i]==arr[i+1]){
				flag=1;
				arr1[k]=arr[i];
				k++;
			}
		}
		if(flag==0){
			arr1[k]=-1;
			return arr1;
		}
		return arr1;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		System.out.println("ENter array elements");
		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int arr1[]=findDuplicateNo(arr);

		if(arr1[0]==-1){
			System.out.println("Duplicate elements is Not found");
		}else{
			for(int i=0;i<arr1.length;i++){
				if(arr1[i]==0){
					continue;
				}else{
					System.out.print(arr1[i]+"  ");
				}
			}
			System.out.println();
		}
	}
}

