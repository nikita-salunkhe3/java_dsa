/*
 Que 19 : Elements with left side smaller and right side greater
Given an unsorted array of size N. Find the first element in the array such that all of its
left elements are smaller and all right elements to it are greater than it.
Note: Left and right side elements can be equal to required elements. And extreme
elements cannot be required.
Example 1:
Input:
N=4
A[] = {4, 2, 5, 7}
Output:
5
Explanation:
Elements on the left of 5 are smaller than 5 and on the right of it are greater than 5.
Example 2:
Input:
N=3
A[] = {11, 9, 12}
Output:
-1
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
3 <= N <= 10 6
1 <= A[i] <= 10 6Que 20 : Bitonic point
 */

import java.io.*;
class Client{
	static int 

