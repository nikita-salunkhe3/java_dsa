/*
 Que 17 : Find all Pairs with given Sum
Given two unsorted arrays A of size N and B of size M of distinct elements, the task is to
find all pairs from both arrays whose sum is equal to X.
Note: All pairs should be printed in increasing order of u. For eg. for two pairs (u1,v1)
and (u2,v2), if u1 < u2 then
(u1,v1) should be printed first else second.
Example 1:
Input:
A[] = {1, 2, 4, 5, 7}
B[] = {5, 6, 3, 4, 8}
X=9
Output:
18
45
54
Explanation: (1, 8), (4, 5), (5, 4) are the pairs which sum to 9.
Example 2:
Input:
A[] = {-1, -2, 4, -6, 5, 7}
B[] = {6, 3, 4, 0}
X=8
Output:
44
53
Expected Time Complexity: O(NLog(N))
Expected Auxiliary Space: O(N)
Constraints:
1 ≤ N, M ≤ 10 6
-10 6 ≤ X, A[i], B[i] ≤ 10 6
 */

import java.io.*;
class Client{
	static void sumPair(int arr1[],int arr2[],int sum){

		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]+arr2[j] == sum){
					System.out.println(arr1[i]+"  "+arr2[j]);
				}
			}
		}
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size for 1st array");

		int size1=Integer.parseInt(br.readLine());

		int arr1[]=new int[size1];

		System.out.println("Enter array elements");

		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter array size for 2nd array");

		int size2=Integer.parseInt(br.readLine());

		int arr2[]=new int[size2];

		System.out.println("Enter array elements");

		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("ENter sum of the pair");

		int sum=Integer.parseInt(br.readLine());

		sumPair(arr1,arr2,sum);
	}
}




