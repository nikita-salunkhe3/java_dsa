/*
 Que 2 :Sort an array of 0s, 1s and 2s
Given an array of size N containing only 0s, 1s, and 2s; sort the array in ascending order.
Example 1:
Input:
N=5
arr[]= {0 2 1 2 0}
Output:
00122
Explanation: 0s 1s and 2s are segregated into ascending order.
Example 2:
Input:
N=3
arr[] = {0 1 0}
Output:
001
Explanation: 0s 1s and 2s are segregated into ascending order.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10 6
0 <= A[i] <= 2
 */

import java.io.*;
class Client{
	static void sortArray(int arr[]){
		for(int i=0;i<arr.length-1;i++){
			if(arr[i] > arr[i+1]){
				int temp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
				i=-1;

			}
		}
	}
	public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                sortArray(arr);

                System.out.println("Sorted Array becomes");

                for(int i=0;i<arr.length;i++){
                        System.out.println(arr[i]);
                }
	}
}

