/*
 Que 15 : Union of two sorted Array
Union of two arrays can be defined as the common and distinct elements in the two
arrays.
Given two sorted arrays of size n and m respectively, find their union.
Example 1:
Input:
n = 5, arr1[] = {1, 2, 3, 4, 5}
m = 3, arr2 [] = {1, 2, 3}
Output: 1 2 3 4 5
Explanation: Distinct elements including both the arrays are: 1 2 3 4 5.
Example 2:
Input:
n = 5, arr1[] = {2, 2, 3, 4, 5}
m = 5, arr2[] = {1, 1, 2, 3, 4}
Output: 1 2 3 4 5
Explanation: Distinct elements including both the arrays are: 1 2 3 4 5.
Example 3:
Input:
n = 5, arr1[] = {1, 1, 1, 1, 1}
m = 5, arr2[] = {2, 2, 2, 2, 2}
Output: 1 2
Explanation: Distinct elements including both the arrays are: 1 2.
Expected Time Complexity: O(n+m).
Expected Auxiliary Space: O(n+m).
Constraints:
1 <= n, m <= 10 5
1 <= arr[i], brr[i] <= 10 6
 */

import java.io.*;
import java.util.HashMap;

class Client{
	static void UnionOf2Array(int arr1[],int arr2[]){

		int i=0;
		int j=0;
		while(i<arr1.length && j<arr2.length){
			if(arr1[i]<arr2[j]){
				System.out.print(arr1[i]+"  ");
				i++;
			}else if(arr2[j]<arr1[i]){
				System.out.print(arr2[j]+"  ");
				j++;
			}else{
				System.out.print(arr1[i]+"  ");
				i++;
				j++;
			}
		}
		while(i<arr1.length){
			System.out.print(arr1[i]+"  ");
			i++;
		}
		while(j<arr2.length){
			System.out.print(arr2[j]+"  ");
			j++;
		}
		System.out.println();
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size for 1st array");

		int size1=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr1[]=new int[size1];

		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter array size for 2nd array");

		int size2=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr2[]=new int[size2];

		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		UnionOf2Array(arr1,arr2);
	}
}



