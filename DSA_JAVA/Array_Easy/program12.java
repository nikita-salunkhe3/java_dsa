/*
 Que 12 : Number of occurance
Given a sorted array Arr of size N and a number X, you need to find the number of
occurrences of X in Arr.
Example 1:
Input:
N = 7, X = 2
Arr[] = {1, 1, 2, 2, 2, 2, 3}
Output: 4
Explanation: 2 occurs 4 times in the
given array.
Example 2:
Input:
N = 7, X = 4
Arr[] = {1, 1, 2, 2, 2, 2, 3}
Output: 0
Explanation: 4 is not present in the given array.
Expected Time Complexity: O(logN)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10 5
1 ≤ Arr[i] ≤ 10 6
1 ≤ X ≤ 10 6
 */

import java.io.*;

class Client{
	static int noOfOcc(int arr[],int num){
		
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(num==arr[i]){
				count++;
			}
		}
		return count;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter Occurence Element");

		int num=Integer.parseInt(br.readLine());

		System.out.println(noOfOcc(arr,num));
	}
}

