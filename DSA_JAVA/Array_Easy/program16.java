/*
 Que 16 : Rotation
Given an ascending sorted rotated array Arr of distinct integers of size N. The array is
right rotated K times. Find the value of K.
Example 1:
Input:
N=5
Arr[] = {5, 1, 2, 3, 4}
Output: 1
Explanation: The given array is 5 1 2 3 4. The original sorted array is 1 2 3 4 5.
We can see that the array was rotated 1 time to the right.
Example 2:
Input:
N=5
Arr[] = {1, 2, 3, 4, 5}
Output: 0
Explanation: The given array is not rotated.
Expected Time Complexity: O(log(N))
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <=10 5
1 <= Arr i <= 10 7
 */

import java.io.*;
class Client{
	static int rotationArray(int arr[]){

		int min=arr[0];
		int minIndex=0;
		for(int i=0;i<arr.length-1;i++){
			if(min > arr[i]){
				min=arr[i];
				minIndex=i;
			}
		}
		return minIndex;
	}
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("ENter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Number of rotation is: "+rotationArray(arr));
	}
}
