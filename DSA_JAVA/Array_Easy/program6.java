/*
 Que 6 : Second Largest
Given an array Arr of size N, print the second largest distinct element from an array.
Example 1:
Input:
N=6
Arr[] = {12, 35, 1, 10, 34, 1}
Output: 34
Explanation: The largest element of the array is 35 and the second largest element
is 34.
Example 2:
Input:
N=3
Arr[] = {10, 5, 10}
Output: 5
Explanation: The largest element of the array is 10 and the second largest element
is 5.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
2 ≤ N ≤ 10 5
1 ≤ Arr i ≤ 10 5
 */

import java.io.*;
class Client{
	static int secondLarge(int arr[]){

		for(int i=0;i<arr.length-1;i++){
			if(arr[i] > arr[i+1]){
				int temp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
				i=-1;
			}
		}
		if(arr[arr.length-1] > arr[arr.length-2]){
			return arr[arr.length-2];
		}
		int k=arr.length-1;
		while(arr[k] == arr[k-1]){
			k--;
		}
		return arr[k-1];
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Second Large  element is: "+secondLarge(arr));
	}
}
