import java.io.*;
class Client{
	static void sortArray(int arr[]){

		for(int i=0;i<arr.length-1;i++){

			if(arr[i]>arr[i+1]){
				int temp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
				i=-1;
			}
		}
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		sortArray(arr);

		System.out.println("Sorted Array becomes");

		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}


