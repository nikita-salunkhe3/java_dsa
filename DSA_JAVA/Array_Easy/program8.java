/*
 Que 8 : Rotate Array
Given an unsorted array arr[] of size N. Rotate the array to the left (counter-clockwise
direction) by D steps, where D is a positive integer.
Example 1:
Input:
N = 5, D = 2
arr[] = {1,2,3,4,5}
Output: 3 4 5 1 2
Explanation: 1 2 3 4 5 when rotated
by 2 elements, it becomes 3 4 5 1 2.
Example 2:
Input:
N = 10, D = 3
arr[] = {2,4,6,8,10,12,14,16,18,20}
Output: 8 10 12 14 16 18 20 2 4 6
Explanation: 2 4 6 8 10 12 14 16 18 20
when rotated by 3 elements, it becomes
8 10 12 14 16 18 20 2 4 6.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10 6
1 <= D <= 10 6
0 <= arr[i] <= 10 5
 */

import java.io.*;
class Client{
	static void rotateArray(int arr[],int num){

			for(int j=arr.length-2;j>1;j--){
				int store=arr[j-1];
				arr[j-1]=arr[j];
				int store1=arr[j-2];
				}

		
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array element");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter No to rotate Array");

		int num=Integer.parseInt(br.readLine());

		rotateArray(arr,num);

		System.out.println("Rotated Array Becomes");

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}






