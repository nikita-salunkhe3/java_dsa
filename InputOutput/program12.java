//take input at one line from user and at the output it becomes separate String

import java.io.*;
import java.util.*;

class InputDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter team of the match, Enter Man of the match,runs");

		String str=br.readLine();

		System.out.println("String is :"+str);

		StringTokenizer obj=new StringTokenizer(str," ");

		String t1=obj.nextToken();
		String t2=obj.nextToken();
		String t3=obj.nextToken();

		System.out.println("team is: "+t1);
		System.out.println("MOM team is: "+t2);
		System.out.println("runs are: "+Integer.parseInt(t3));
	}
}



