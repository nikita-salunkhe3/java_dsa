//realtime example


import java.io.*;
import java.util.*;
class RealTimeEx{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter name of Dmart,grade,Quantity,price");

		String str=br.readLine();

		StringTokenizer obj=new StringTokenizer(str," ");

		String t1=obj.nextToken();
		String t2=obj.nextToken();
		String t3=obj.nextToken();
		String t4=obj.nextToken();

		System.out.println();

		System.out.println("Name of Demart= "+t1);
		System.out.println("Grade for Demart= "+t2.charAt(0));
		System.out.println("Number of Quantity= "+Integer.parseInt(t3));
		System.out.println("Total Price= "+Float.parseFloat(t4));
	}
}

