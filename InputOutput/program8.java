//get input from user as name of player its jerNo and avg of the player

import java.io.*;
class InputDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the player name:");

		String name=br.readLine();

		System.out.println("Enter jerNo number of player:");

		int jerNo=Integer.parseInt(br.readLine());

		System.out.println("Enter avg of a player:");

		float avg=Float.parseFloat(br.readLine());

		System.out.println("Player name is :"+name);
		System.out.println("Player jerNo is :"+jerNo);
		System.out.println("Player avg is :"+avg);
	}
}
