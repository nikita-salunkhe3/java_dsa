/*
 * Write a program in which user should enter two numbers if both the numbers are positive
multiply them and provide to switch case to verify number is even or odd, if user enters any
negative number program should terminate saying “Sorry negative numbers not allowed”
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		InputStreamReader isr=new InputStreamReader(System.in);

		BufferedReader obj=new BufferedReader(isr);

		System.out.println("enter the two numbers:");

		int num1=Integer.parseInt(obj.readLine());
		int num2=Integer.parseInt(obj.readLine());

		if(num1>0 && num2>0){
			int mul=num1*num2;

			int rem=mul%10;

			switch(rem){
				case 1:
				case 3:
				case 5:
				case 7:
				case 9:System.out.println("the number is Odd number");
				       break;

				case 0:
				case 2:
				case 4:
				case 6:
				case 8:System.out.println("number is Even number");
				       break;
			}
		}else if(num1==0 || num2==0){
			System.out.println("Zero number is not Positive or negative number");
		}else{
			System.out.println("Sorry ! negative number is not allowed");
		}
	}
}



