/*
 *Q4
Take choice from user
Show this to user
What's your interest?
1.movies
2.Series
Enter your choice 1 or 2 :
If user enters 1 :

Movie you wish to watch today
1.Founder
2. Snowden
3.Jobs
4.Her
5.Social Network
6.Wall-E
7.AI

Enter your choice :2 
: Snowden
If user enters 2 :
Best series to watch
1.Silicon valley
2.Devs
3.the IT crowd
4.Mr Robot
Print users choice
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		System.out.println("What's your interest?");
		System.out.println("1. Movies");
		System.out.println("2. Series");
		System.out.println("Enter your choice 1 or 2:");
		
		InputStreamReader obj=new InputStreamReader(System.in);

		BufferedReader obj1=new BufferedReader(obj);

		int choice=Integer.parseInt(obj1.readLine());

		switch(choice){
			case 1:
				{
					System.out.println("Movie you wish to watch today ");
					System.out.println("1. Founder");
					System.out.println("2. Snowden");
					System.out.println("3. Jobs");
					System.out.println("4. Her");
					System.out.println("5. Social Network");
					System.out.println("6. Wall-E");
					System.out.println("7. AI");

					System.out.println("Enter your choice:");

					choice=Integer.parseInt(obj1.readLine());

					switch(choice){
						case 1:
							System.out.println("Enjoy Founder movie");
							break;
						case 2:
							System.out.println("Enjoy snowden movie");
							break;
						case 3:
							System.out.println("Enjoy Jobs movie");
							break;
						case 4:
							System.out.println("Enjoy Her movie");
							break;
						case 5:
							System.out.println("Enjoy Social Network movie");
							break;
						case 6:
							System.out.println("Enjoy Wall-E movie");
							break;
						case 7:
							System.out.println("Enjoy AI movie");
							break;
						default:
							System.out.println("Invalid choice");
							break;
					}
				}
				break;
			case 2:
				{
					System.out.println("Best series to watch");
					System.out.println("1. Silicon Valley");
					System.out.println("2. Devs");
					System.out.println("3. the IT crowd");
					System.out.println("4. Mr Robot");

					System.out.println("Enter your choice");
					choice=Integer.parseInt(obj1.readLine());

					switch(choice){
						case 1:
							System.out.println("Enjoy Silicon valley Series");
							break;
						case 2:
							System.out.println("Enjoy Devs Series");
							break;
						case 3:
							System.out.println("Enjoy the IT crowd Series");
							break;
						case 4:
							System.out.println("Enjoy Mr Robot Series");
							break;
						default:
							System.out.println("Invalid choice");
							break;
					}
				}
				break;
			default:
				System.out.println("Invalid choice");
				break;
		}
	}
}







