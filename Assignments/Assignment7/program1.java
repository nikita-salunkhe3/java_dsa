/*
 * Q1
Write a program in which students should enter marks of 5 different subjects. If all subject
having above passing marks add them and provide to switch case to print grades(first class
second class), if student get fail in any subject program should print “You failed in exam”
 */

import java.io.*;
import java.util.Scanner;
class SwitchDemo{
	public static void main(String[] args)throws IOException{

                	System.out.println("Enter the marks");
  
              		InputStreamReader isr=new InputStreamReader(System.in);
 
         		BufferedReader obj=new BufferedReader(isr);
   
        	      	float sub1=Float.parseFloat(obj.readLine());
        	      	float sub2=Float.parseFloat(obj.readLine());
        	      	float sub3=Float.parseFloat(obj.readLine());
        	      	float sub4=Float.parseFloat(obj.readLine());
        	      	float sub5=Float.parseFloat(obj.readLine());


		int flag=0;

		float sum=sub1+sub2+sub3+sub4+sub5;

		if(sub1>=35 && sub2>=35 && sub3>=35 && sub4>=35 && sub5>=35){
			if(sum>400 && sum<=500){
				flag=1;
			}else if(sum>300 && sum<=400){
				flag=2;
			}else if(sum>=300){
				flag=3;
			}
		}else{
			flag=4;
		}

		switch(flag){
			case 1:
				System.out.println("first class");
				break;
			case 2:
				System.out.println("second class");
				break;
			case 3:
				System.out.println("You are pass");
				break;
			case 4:
				System.out.println("You are fail");
				break;
			default:
				System.out.println("Invalid input");
				break;
			}
	}
}


