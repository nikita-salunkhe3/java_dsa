/*
 * Q2.
Write a program in which ask the user to enter a number from 0 to 5 and print it's spelling,if the
entered number is greater than 5 print entered number is greater than 5
 */

import java.io.*;
class SwitchDemo{
	public static void main(String[] args)throws IOException{
		
		InputStreamReader isr=new InputStreamReader(System.in);

		System.out.println("enter number between 0 to 5");

		BufferedReader obj=new BufferedReader(isr);

		int num=Integer.parseInt(obj.readLine());

		if(num>=0){

	        	switch(num){
		        	case 0:
			         	System.out.println("Zero");
			         	break;
		         	case 1:
			         	System.out.println("One");
			        	break;
              			case 2:
	          			System.out.println("Two");
		          		break;
               			case 3:
	          			System.out.println("Three");
		         		break;
               			case 4:
	               			System.out.println("Four");
		        		break;
               			case 5:
                			System.out.println("Five");
 		         		break;
		              	default:
			          	System.out.println("Entered number is greater than 5");
		               		break;
			}
		}else{
			System.out.println("number is less than 0");
		}
	}
}


