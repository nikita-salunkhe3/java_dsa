/*
 * write a program to print the following pattern
 * 0
 * 1  1
 * 2  3  5
 * 8  13 21 34
 */

import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number of rows:");

		int row=Integer.parseInt(br.readLine());

		int num1=0;
		int num2=1;
		int sum=0;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(sum+"  ");
				num1=num2;
				num2=sum;
				sum=num1+num2;
			}
			System.out.println();
		}
	}
}

