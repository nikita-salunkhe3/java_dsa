/*
 * write a program to print the following pattern
Row =8

$
@ @
& & &
# # # #
$ $ $ $ $
@ @ @ @ @ @
& & & & & & &
# # # # # # # # 

 */

import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number of rows:");

		int row=Integer.parseInt(br.readLine());

		int num=1;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(num==1){
					System.out.print("$  ");
				}else if(num==2){
					System.out.print("@  ");
				}else if(num==3){
					System.out.print("&  ");
				}else if(num==4){
					System.out.print("#  ");
				}
			}
			num++;
			if(num==5){
				num=1;
			}
			System.out.println();
		}
	}
}

					


