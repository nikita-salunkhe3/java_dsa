/*
WAP to print all even numbers in reverse order and odd numbers in the standard way. Both separately.
Within a range. Take the start and end from user
Input: Enter start number - 2
Enter End number - 9
Output:
8642
3579
*/

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter stating and ending number");

		int num1=Integer.parseInt(br.readLine());

		int num2=Integer.parseInt(br.readLine());

		for(int i=num2;i>=num1;i--){
			if(i%2==0){
				System.out.print(i+" ");
			}
		}

		System.out.println();

		for(int i=num1;i<=num2;i++){
			if(i%2==1){
				System.out.print(i+" ");
			}
		}
		System.out.println();
	}
}
