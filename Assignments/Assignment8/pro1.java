/*
 write a program to print the following pattern
D4 C3 B2 A1
A1 B2 C3 D4
D4 C3 B2 A1
A1 B2 C3 D4
*/

import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row");

		int row=Integer.parseInt(br.readLine());

		char ch=(char)(64+row);

		for(int i=1;i<=row;i++){
			int num=row;
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print(ch+""+num+"  ");
					num--;
					ch--;
				}else{
					ch++;
					System.out.print(ch+""+j+"  ");
				}
			}
			System.out.println();
		}
	}
}


