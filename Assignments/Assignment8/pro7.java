/*
 Q7
write a program to print the following pattern
Row =5;
O
14 13
L K J
9 8 7 6
E D C B A
 */ 

import java.io.*;
class Pattern{
        public static void main(String args[])throws IOException{

	BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

	System.out.println("Enter the rows");

	int row=Integer.parseInt(obj.readLine());

	int num=(row*(row+1))/2;
	char ch=(char)(64+num);

	for(int i=1;i<=row;i++){
		for(int j=1;j<=i;j++){

			if(row%2==1){
				if(i%2==1){
					System.out.print(ch+"  ");
				}else{
					System.out.print(num+"  ");
				}
				num--;
				ch--;
				
			}else{
				if(i%2==1){
					System.out.print(num+"  ");
				}else{
					System.out.print(ch+"  ");
				}
				ch--;
				num--;
			}
		}
		System.out.println();
	}
	}
}



	

