//fibonacci series
//approach - 2

class Fibonacci{
	public static void main(String[] args){
		int n1=1;
		int n2=1;
		int N=10;

		for(int i=1;i<=N;i++){
			n1=n2-n1;
			n2=n1+n2;
			System.out.println(n1);
		}
	}
}
