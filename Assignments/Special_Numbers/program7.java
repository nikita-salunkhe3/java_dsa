//Automorphic number 
//approach - 2

class Automorphic{
	public static void main(String[] args){
		int num=76;
		int temp=num;
		int square=num*num;

		int count=0;
		while(num != 0){
			count++;
			num=num/10;
		}

		int rev=0;

		for(int i=1;i<=count;i++){
			int rem=square%10;
			rev=rev*10 + rem;
			square=square/10;
		}
		int temp2=rev;
		int rev2=0;

		for(int i=1;i<=count;i++){
			int rem1=temp2%10;
			rev2=rev2*10 + rem1;
			temp2=temp2/10;
		}
		if(rev2==temp){
			System.out.println(temp+" is Automorphic number");
		}else{
			System.out.println(temp+" is NOT Automorphic number");
		}
	}
}

			
			
