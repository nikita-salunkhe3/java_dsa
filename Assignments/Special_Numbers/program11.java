//claculate factorial of given number ranges from 1 to 10
//and print sum of all factorial

class Factorial{
	public static void main(String[] args){
		
		int num=10;
		int sum=0;

		for(int i=1;i<=num;i++){
			int fact=1;
			for(int j=1;j<=i;j++){
				fact=fact*j;
			}
			System.out.println(i+"! "+"= "+fact);
			sum=sum+fact;
		}
		System.out.println("sum of factorial is "+sum);
	}
}
