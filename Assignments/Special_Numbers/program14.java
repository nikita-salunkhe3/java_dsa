//perfect number
//input:28
//output: 28 is perfect number
//
//input:6
//output: 6 is perfect number
//
//explain- 28 factors are
//         1+ 2 + 4+ 7+ 14 +28 ==== 56
//         28*2 ==== 56
//         then we can said that given number is perfect number


class Perfect{
	public static void main(String[] args){
		int num=496;

		int sum=0;
		for(int i=1;i<=num;i++){
			if(num%i==0){
				sum=sum+i;
			}
		}
		if(sum == num*2){
			System.out.println(num+" is perfect number");
		}else{
			System.out.println(num+" is NOT perfect number");
		}
	}
}

				
