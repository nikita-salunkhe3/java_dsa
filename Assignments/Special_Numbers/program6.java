//palindrome number
//121 reverse 121

class Palindrome{
	public static void main(String[] args){
		int num=1121;
		int rev=0;
		int temp=num;

		while(num !=0){
			int rem=num%10;
			rev=rev*10 + rem;
			num=num/10;
		}
		if(rev==temp){
			System.out.println(temp+" is palindrome number");
		}else{
			System.out.println(temp+" is NOT palindrome number");
		}
	}
}
