//Automorphic Number
//25 given number ter 25*25=625 last two number same so 25 is automorphic number
//76 given number ter 76*76=5776 last two number is same as 76 so it is automorphic number


class Automorphic{
	public static void main(String[] args){
		int num=76;
		int temp=num*num;
		int temp1=num;

		int flag=0;

		while(num != 0){
			int rem=num%10;
			while(temp != 0){
				int rem1=temp%10;
				if(rem==rem1){
					flag=1;
				}else{
					flag=0;
				}
				temp=temp/10;
				break;
			}
			num=num/10;
		}
		if(flag==1){
			System.out.println(temp1+" is Automorphic Number");
		}else{
			System.out.println(temp1+" is NOT Automorphic Number");
		}
	}
}
