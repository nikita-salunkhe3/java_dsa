//Strong Number
//145= 1! + 4! + 5! = 1+24+120 =145

class Strong{
	public static void main(String[] args){
		int num=145;
		int temp=num;

		int sum=0;
		while(num != 0){
			int rem=num%10;
			int fact=1;
			for(int i=1;i<=rem;i++){
				fact=fact*i;
			}
			sum=sum+fact;
			num=num/10;
		}
		if(temp==sum){
			System.out.println(temp+" is Strong Number");
		}else{
			System.out.println(temp+" is NOT Strong Number");
		}
	}
}

