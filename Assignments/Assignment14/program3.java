//write a program to print the series  of prime number ranging between 1 to 100

import java.io.*;
class PrimeNumber{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter starting range");

		int start=Integer.parseInt(br.readLine());
	
		System.out.println("Enter ending range");

		int end=Integer.parseInt(br.readLine());

		System.out.println("Prime numbers are:");

		for(int i=start;i<=end;i++){
			int count=0;
			for(int j=2;j<=i/2;j++){
				if(i%j==0){
					count++;
					break;
				}
			}
			if(count==0 && i != 1){
				System.out.println(i+"  ");
			}
		}
	}
}
