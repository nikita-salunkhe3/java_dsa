//Write a program to print following program
//take row from user
/*
E  a  D  b
   c  C  d
      B  e
         f

*/


import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row");

		int row=Integer.parseInt(br.readLine());

		char ch1=(char)(row+65);
		char ch2='a';

		for(int i=1;i<=row;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print("   ");
			}
			for(int j=1;j<=row-i+1;j++){
				if((i+j)%2==0){
					System.out.print(ch1+"  ");
					ch1--;
				}else{
					System.out.print(ch2+"  ");
					ch2++;
				}
			}
			System.out.println();
		}
	}
}

