//String basics
//write a java program to check wether two strings are Anagram of each other or not
//input : shahsi     ashish


import java.io.*;
class StringAnagram{
	int countChar(char arr[],int i){
		int count=0;
		for(int j=0;j<arr.length;j++){
			if(arr[j]==arr[i]){
				count++;
			}
		}
		return count;
	}
	int StringAnagram(String str1,String str2){
		char arr1[]=str1.toCharArray();
		char arr2[]=str2.toCharArray();

		if(arr1.length==arr2.length){

		        for(int i=0;i<arr1.length;i++){
		        	int count=0;
		        	for(int j=0;j<arr2.length;j++){
					if(arr1[i]==arr2[j]){
						count++;
					}
				}
				if(count==countChar(arr1,i)){
					continue;
				}else{
					return -1;
				}
			}
			return 0;
		}else{
			return -1;
		}

	}

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the string");

		String str1=br.readLine();

		System.out.println("Enter second String");

		String str2=br.readLine();

		StringAnagram obj=new StringAnagram();

		int ret=obj.StringAnagram(str1,str2);
		if(ret==-1){
			System.out.println("String is Not Anagram");
		}else{
			System.out.println("Anagram String");
		}
	}
}

