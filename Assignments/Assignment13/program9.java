//write a program in java to print duplicate character in given string

import java.io.*;
class duplicateStr{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");

		String str=br.readLine();

		char arr[]=str.toCharArray();

		System.out.println("Duplicate character are:");
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=i+1; j<arr.length;j++){
				
				if(arr[i]==arr[j] && arr[i]!='1'){
					count++;
					arr[j]='1';
				}

			}
			if(count>0 && arr[i]!= '1'){
				System.out.println(arr[i]);
			}
		}
	}
}
