//Write a java program in which check given string is palindrome or not.

import java.io.*;
class Palindrome{
	int Palindrome(String str){
		char arr[]=str.toCharArray();
		int j=arr.length-1;
		for(int i=0;i<arr.length;i++){
			if(i<=j){
				if(arr[i]==arr[j]){
					j--;
					continue;
				}else{
					return -1;
				}
			}
		}
		return 0;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter string");

		String str=br.readLine();

		Palindrome obj=new Palindrome();

		int ret=obj.Palindrome(str);
		if(ret==-1){
			System.out.println("Not Palindrome String");
		}else{
			System.out.println("Palindrome String");
		}
	}
}



