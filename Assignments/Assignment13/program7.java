//write a java program to print largest character in given string

import java.io.*;
class LargeChar{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");

		String str=br.readLine();

		char arr[]=str.toCharArray();

		char max=arr[0];
		for(int i=1;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		System.out.println("largest character is: "+max);
	}
}

