//Array basics
/*
 * Take all inputs from user
 * write a java program to count enev and odd numbers
 */

import java.io.*;
class EvenOdd{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		int Ecount=0;
		int Ocount=0;
		System.out.println("enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				Ecount++;
			}else{
				Ocount++;
			}
		}
		System.out.println("Number of even count is: "+Ecount);
		System.out.println("Number of odd count is: "+Ocount);
	}
}

