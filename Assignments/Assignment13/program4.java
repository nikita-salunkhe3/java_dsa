//reverse an array in java

import java.io.*;
class ReverseDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of an array");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int j=arr.length-1;
		for(int i=0;i<=j;i++){
			int temp=arr[i];
			arr[i]=arr[j];
			arr[j]=temp;
			j--;
		}
		System.out.println("Reverse array is :");
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]+"  ");
		}
	}
}



