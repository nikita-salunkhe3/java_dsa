//write a java program to check whether number is positive or negative
//input:3
//output:3 is positive number
//input: 0
//output:Neither positive nor negative

class PosNeg{
	public static void main(String[] args){
		int num=0;
		if(num>0){
			System.out.println(num+" is positive number");
		}else if(num<0){
			System.out.println(num+" is negative number");
		}else{
			System.out.println(num+" is neither positive nor negative number");
		}
	}
}

