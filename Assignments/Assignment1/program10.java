//Write a real-time example of ifelse if ladder

class Example{
	public static void main(String[] args){
		float rating=-2f;

		if( rating>=0.0f && rating<=5.0f){
			System.out.println("You have to see prem ratan dhan payo movie");
		}else if(rating<=7.0f && rating>5.0f){
			System.out.println("You have to see tumbbad Movie");
		}else if(rating<=9.0f && rating>7.0f){
			System.out.println("You have to see RRR Movie");
		}else if(rating>9.0f && rating<=10.0f){
			System.out.println("You have to see Kantara Movie");
		}else{
			System.out.println("Invalid rating");
		}
	}
}


