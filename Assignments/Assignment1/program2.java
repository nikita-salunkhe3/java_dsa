//Write a java program,take a number and print whether it is less than 10 
//or greater than 10
//input:5
//output:5 is less than 10
//input:10
//output:both are equal

class GreaterLess{
	public static void main(String[] args){
		int num=10;

		if(num>10){
			System.out.println(num+" is greater than 10");
		}else if(num<10){
			System.out.println(num+" is less than 10");
		}else{
			System.out.println("Both are equal");
		}
	}
}


