//Write a program that takes the cost price and selling price calculates its
// profit and loss
// input: selling price= 1200
// input: cost price= 1000
// output: profit of 200


class ProfitLoss{
	public static void main(String[] args){
		int sp=1000;
		int cp=1000;

		int price=0;
		if(sp>cp){
			price=sp-cp;
			System.out.println("profit of "+price);
		}else if(sp<cp){
			price=cp-sp;
			System.out.println("Loss of "+price);
		}else{
			System.out.println("NO profit NO Loss");
		}
	}
}

