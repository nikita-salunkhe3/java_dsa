/*
 *Write a program to take range as input from the user and print Armstrong numbers. ( Take a start and
end number from a user )
Input: Enter start: 1
Enter end: 1650
Output: Armstrong numbers between 1 and 1650
1 2 3 4 5 6 7 8 9 153 370 371 407 1634
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start number:");

		int start=Integer.parseInt(br.readLine());

		System.out.println("Enter end number:");

		int end=Integer.parseInt(br.readLine());

		System.out.println("Armstrong number are:");

		for(int i=start;i<=end;i++){
			int num=i;
			int store=i;

			int count=0;
			while(num!=0){
				count++;
				num=num/10;
			}
			int sum=0;
			while(store !=0){
				int rem=store%10;
				int p=1;
				for(int j=1;j<=count;j++){
					p=p*rem;
				}
				sum=sum+p;
				store=store/10;
			}
			if(sum==i){
				System.out.print(i+"  ");
			}
		}
		System.out.println();
	}
}


				


