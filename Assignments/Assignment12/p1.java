/*
 *Write a program to print the numbers divisible by 5 from 1 to 50 & the number is even also print the
count of even numbers.
Input: Enter a lower limit: 1
Enter upper limit: 50
Output: 10, 20, 30, 40, 50
Count = 5
 */

import java.io.*;
class Number{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter lower limit");

		int num1=Integer.parseInt(br.readLine());

		System.out.println("Enter upper limit");

		int num2=Integer.parseInt(br.readLine());

		int count=0;
		System.out.println(" Even Number divisible by 5 :");
		for(int i=num1;i<=num2;i++){
			if(i%5==0 && i%2==0){
			       	count++;
			        System.err.println(i+"  ");
			}
		}
		System.out.println("count of even number: "+count);
	}
}


