/*
 *Write a program to take 5 numbers as input from the user and print the count of digits in those
numbers.
Input: Enter 5 numbers :
5
The digit count in 5 is 1
25
The digit count in 25 is 2
225
The digit count in 225 is 3
6548
The digit count in 6548 is 4
852347The digit count in 852347 is 6
 */

import java.io.*;
class Demo{
	static int DigitCount(int num){

		int count=0;
		while(num != 0){
			count++;
			num=num/10;
		}
		return count;
	}
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter five number");

		int arr[]=new int[5];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			int ret=DigitCount(arr[i]);
			System.out.print("the digit count in "+arr[i]+" is "+ret);
			System.out.println();
		}
	}
}

