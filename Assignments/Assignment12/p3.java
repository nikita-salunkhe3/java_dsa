/*
 *Write a program to take a range as input from the user and print perfect squares between that range.
Input: Enter start: 1
Enter end: 100
Output: perfect squares between 1 and 100
 */

import java.io.*;
class Demo{
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter starting number");

                int start=Integer.parseInt(br.readLine());

                System.out.println("Enter ending number");

                int end=Integer.parseInt(br.readLine());

		System.out.println("Perfect square number are:");

		for(int i=start;i*i<=end;i++){
			System.out.print(i*i+"  ");
		}
		System.out.println();
	}
}


