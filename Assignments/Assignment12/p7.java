/*
 *Write a program to take range as input from the user and print the reverse of all numbers. ( Take a
start and end number from a user )
Input: Enter start: 100
Enter end: 200
Output: Reversed numbers
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter starting number");

		int start=Integer.parseInt(br.readLine());

		System.out.println("enter ending number:");

		int end=Integer.parseInt(br.readLine());

		System.out.println("Reverse number are:");

		for(int i=start;i<=end;i++){
			int num=i;

			int rev=0;
			while(num!=0){
				int rem=num%10;
				rev=rev*10+rem;
				num=num/10;
			}
			System.out.print(rev+"  ");
		}
		System.out.println();
	}
}
