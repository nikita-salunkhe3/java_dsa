/*
 *Write a program to take a range as input from the user and print perfect cubes between that range.
Input: Enter start: 1
Enter end: 100
Output: perfect cubes between 1 and 100
 */

import java.io.*;
class Demo{
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter starting number");

                int start=Integer.parseInt(br.readLine());

                System.out.println("Enter ending number");

                int end=Integer.parseInt(br.readLine());

		for(int i=start;i<=end;i++){

			for(int j=1;j*j*j<=i;j++){
				if(j*j*j==i){
					System.out.println(i+"  ");
				}
			}
		}
	}
}
/*
for(int i=start;i*i*i<=end;i++){
	System.out.println(i*i*i+"  ");
}
*/


