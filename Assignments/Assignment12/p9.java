/*
 *write a program to print a series of strong numbers from entered range. ( Take a start and end number
from a user )
Input:-
Enter starting number: 1
Enter ending number: 150
Output:-
Output: strong numbers between 1 and 150
1
2
145
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter start number:");

		int start=Integer.parseInt(br.readLine());

		System.out.println("enter end number:");

		int end=Integer.parseInt(br.readLine());

		System.out.println("Strong number between "+start+" and "+end+" are: ");

		for(int i=start;i<=end;i++){

			int num=i;
			int sum=0;
			while(num!=0){
				int rem=num%10;
				int fact=1;
				for(int j=1;j<=rem;j++){
					fact=fact*j;
				}
				sum=sum+fact;
				num=num/10;
			}
			if(i==sum){
				System.out.print(i+"  ");
			}
		}
		System.out.println();
	}
}

