//WAP to take size of an array from user and also take integer elements from user print sum of odd 
//elements only
//input: 5
//input: 1 2 3 4 5
//output:9   ---->>>(1+3+5)


import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter size of an array:");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter array elements:");

		int sum=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==1){
				sum=sum+arr[i];
			}
		}
		System.out.println("sum of odd elements is :"+sum);
	}
}


