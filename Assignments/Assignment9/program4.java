//take 7 elements from user and print vowels of given character array
//input: a b c o d p e
//output:a o e


import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size:");

		int size=Integer.parseInt(br.readLine());

		char arr[]=new char[size];

		System.out.println("enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=(char)br.read();
			br.skip(1);
		}

		System.out.println("vowels are:");
		for(int i=0;i<arr.length;i++){

			if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u'||
					arr[i]=='A'||arr[i]=='E'||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){
				System.out.print(arr[i]+"  ");
			}
		}
		System.out.println();
	}
}


