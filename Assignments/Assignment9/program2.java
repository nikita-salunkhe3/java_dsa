//take array size from user also take element from user print product od even number only

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter size of an array:");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter array elements:");

		int pro=1;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				pro=pro*arr[i];
			}
		}
		System.out.println("Product of even number is:"+pro);
	}
}

