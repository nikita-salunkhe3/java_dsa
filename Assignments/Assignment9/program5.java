//Write a program take 10 input from the user and print only elements that are divisible by 5
//input: 10  2  2  3  3   3  4   4  25  55
//output: 10  25  55

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size:");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.print("divisible by 5 numbers are: ");
		for(int i=0;i<arr.length;i++){
			if(arr[i]%5==0){
				System.out.print(arr[i]+"  ");
			}
		}
		System.out.println();
	}
}
