//take size and array elements from user and print product of odd index only
//input: Enter size :6
//input: Enter array elements: 1 2 3 4 5 6
//output:  48  (1=2  3=4 5=6)

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter array elements");

		int product=1;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(i%2==1){
				product=product*arr[i];
			}
		}
		System.out.println("odd index element product is:"+product);
	}
}
				
