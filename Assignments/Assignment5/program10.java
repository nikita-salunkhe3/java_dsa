/*
 * 1  2  3  4
 * 4  5  6
 * 6  7 
 * 7
 */
class Pattern{
	public static void main(String[] args){
		int num=1;
		int row=4;

		for(int i=1;i<=row;i++){
			for(int j=row;j>=i;j--){
				System.out.print(num+++"  ");
			}
			System.out.println();
			num--;
		}
	}
}
