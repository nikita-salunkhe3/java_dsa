/*
 * 10  10  10  10
 * 11  11  11 
 * 12  12  
 * 13
 */

class Pattern{
	public static void main(String[] args){
		int row=4;
		int num=(row*(row+1))/2;

		for(int i=1;i<=row;i++){
			for(int j=row;j>=i;j--){
				System.out.print(num+"  ");
			}
			num++;
			System.out.println();
		}
	}
}
