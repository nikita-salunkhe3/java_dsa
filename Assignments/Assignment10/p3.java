/*
 * Write a Java program to find the sum of even and odd numbers in an array.
Display the sum value.
Input: 11 12 13 14 15
Output
Odd numbers sum = 39
Even numbers sum = 26
 */

import java.io.*;
class Sum{
       public static void main(String[] args)throws IOException{
	       BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

	       System.out.println("Enter array size:");

	       int size=Integer.parseInt(br.readLine());

	       int arr[]=new int[size];

	       System.out.println("enter array elements");

	       int sum1=0;
	       int sum2=0;
	       for(int i=0;i<arr.length;i++){
		       arr[i]=Integer.parseInt(br.readLine());
		       if(arr[i]%2==1){
			       sum1+=arr[i];
		       }else{
			       sum2+=arr[i];
		       }
	       }
	       System.out.println("even number sum is "+sum2);
	       System.out.println("Odd number sum is "+sum1);
       }
}



