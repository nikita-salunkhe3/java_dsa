/*
WAP to find the number of even and odd integers in a given array of integers
Input: 1 2 5 4 6 7 8
Output:
Number of Even Elements: 4
Number of Odd Elements : 3
 */

import java.io.*;
class Sum{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size:");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("enter array elements:");

		int count1=0;
		int count2=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				count1++;
			}else{
				count2++;
			}
		}
		System.out.println("even number count is: " +count1);
		System.out.println("odd number count is: " +count2);
	}
}
