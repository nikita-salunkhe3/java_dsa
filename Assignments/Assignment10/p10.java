/*
 * WAP to print the elements whose addition of digits is even.
Ex. 26 = 2 + 6 = 8 (8 is even so print 26)
Input :
Enter array : 1 2 3 5 15 16 14 28 17 29 123
Output: 2 15 28 17 123
 */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size:");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Elements are:");

		for(int i=0;i<arr.length;i++){
			int sum=0;
			int store=arr[i];
			while(arr[i]!=0){
				int rem=arr[i]%10;
				sum=sum+rem;
				arr[i]=arr[i]/10;
			}
			if(sum%2==0){
				System.out.print(store+"  ");
			}
		}
		System.out.println();
	}
}



