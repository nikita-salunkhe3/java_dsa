/*
 *Write a Java program to merge two given arrays.
Array1 = [10, 20, 30, 40, 50]
Array2 = [9, 18, 27, 36, 45]
Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
Hint: you can take 3rd array
 */

import java.io.*;
class MergeArray{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("enter array size for 1st array:");

                int size1=Integer.parseInt(br.readLine());

                int arr1[]=new int[size1];

                System.out.println("Enter array elements for 1st array:");

                for(int i=0;i<arr1.length;i++){
                        arr1[i]=Integer.parseInt(br.readLine());
                }

                System.out.println("enter array size for 2nd array:");

                int size2=Integer.parseInt(br.readLine());

                int arr2[]=new int[size2];

                System.out.println("Enter array elements for 2nd array:");

                for(int i=0;i<arr2.length;i++){
                        arr2[i]=Integer.parseInt(br.readLine());
                }

		int merge[]=new int[size1+size2];

		int j=0;
		for(int i=0;i<merge.length;i++){
			if(i<arr1.length){
				merge[i]=arr1[i];
			}else{
				merge[i]=arr2[j];
				j++;
			}
		}

		System.out.println("Merge array is:");
		for(int i=0;i<merge.length;i++){
			System.out.print(merge[i]+"  ");
		}
		System.out.println();
	}
}


