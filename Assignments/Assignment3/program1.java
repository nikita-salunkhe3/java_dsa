/*
 * 14  15  16  17 
 * 15  16  17  18
 * 16  17  18  19
 * 17  18  19  20
 */

class Pattern{
	public static void main(String[] args){
		int row=4;

		for(int i=1;i<=4;i++){
			int num=row*row-3+i;
			for(int j=1;j<=4;j++){
				System.out.print(num++ +"  ");
			}
			System.out.println();
		}
	}
}

