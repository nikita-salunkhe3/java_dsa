/*
 * F  5  D  3  B  1
 * F  5  D  3  B  1
 * F  5  D  3  B  1
 * F  5  D  3  B  1
 * F  5  D  3  B  1
 * F  5  D  3  B  1
 */

class Pattern{
	public static void main(String[] args){
		
		int row=6;

		for(int i=1;i<=6;i++){
			int num=row-1;
			char ch='F';
			for(int j=1;j<=6;j++){
				if(j%2==1){
					System.out.print(ch--+"\t");
					ch--;
				}else{
					System.out.print(num--+"\t");
					num--;
				}
			}
			System.out.println();
		}
	}
}

