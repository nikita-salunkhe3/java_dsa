/*
 * A  b  C  d
 * E  f  G  h
 * I  j  K  l
 * M  n  O  p
 */

class Pattern{
	public static void main(String[] args){
		char ch=65;
		char small=97;

		for(int i=1;i<=4;i++){
			for(int j=1;j<=4;j++){
				if(j%2==1){
			        	System.out.print(ch+"\t");
				}else{
					System.out.print(small+"\t");
				}
				small++;
				ch++;
			}
			System.out.println();
		}
	}
}

