/*
 * 1
 * 2 4
 * 3 6 9
 * 4 8 12 16
 */

class Pattern{
        public static void main(String[] args){
		
		for(int i=1;i<=4;i++){
			int num=i;
			for(int j=1;j<=i;j++){
				System.out.print(num+"\t");
				num=num+i;
			}
			System.out.println();
		}
	}
}


