/*
 * 1 
 * 1 2
 * 1 2 3
 * 1 2 3 4
 */

class Pattern{
        public static void main(String[] args){
		int row=4;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(j+"\t");
			}
			System.out.println();
		}
	}
}

