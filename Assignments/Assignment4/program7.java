/*
 * D  D  D  D
 *    c  c  c
 *       B  B
 *          a
 */

class Pattern{
	public static void main(String[] args){
		char ch='D';
		char small='d';

		for(int i=1;i<=4;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			for(int j=4;j>=i;j--){
				if(i%2==1)
			        	System.out.print(ch+"\t");
				else
					System.out.print(small+"\t");
			}
			
			ch--;
			small--;
			System.out.println();
		}
	}
}
