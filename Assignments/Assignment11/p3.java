/*
 * WAP to a composite number from an array and return its index.
Take size and elements from the user
Input: 1 2 3 5 6 7
Output: composite 6 found at index: 4
 */
import java.io.*;
class Demo{

	void Composite(int arr[]){

		for(int j=0;j<arr.length;j++){

	        	int count=0; 
               		for(int i=2;i<=arr[j]/2;i++){
	            		if(arr[j]%2==0){
		         		count++;
			        	break;
              			}
               		}
	           	if(count>=1){
		        	System.out.println("composite "+arr[j]+" found at index: "+j);
			}
		}
	}
	public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		Demo obj=new Demo();

		obj.Composite(arr);

	}
}

