/*
 *WAP to nd a Strong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564 145
Output: Strong no 145 found at index: 5
 */

import java.io.*;
class Demo{
	void Strong(int arr[]){
		for(int i=0;i<arr.length;i++){
			int store=arr[i];

			int sum=0;
			while(store !=0){
				int rem=store%10;
				int fact=1;
				for(int j=1;j<=rem;j++){
					fact=fact*j;
				}
				sum=sum+fact;
				store=store/10;
			}

			if(sum==arr[i]){
				System.out.println("Strong no "+arr[i]+" found at index: "+i);
			}
			
		}
	}

	public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                Demo obj=new Demo();
		obj.Strong(arr);
	}
}

