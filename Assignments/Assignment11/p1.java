/*
 * Write a program to print count of digits in elements of array.
Input: Enter array elements : 02 255 2 1554
Output: 2 3 1 4
 */

import java.io.*;
class CountDigit{

	void count(int arr[]){

		for(int i=0;i<arr.length;i++){

			int count=0;
			while(arr[i] != 0){
				count++;
				arr[i]=arr[i]/10;
			}
			arr[i]=count;
		}
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		CountDigit obj=new CountDigit();
		obj.count(arr);
		System.out.println("count of digit is:");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}






