/*
 *WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465
 */

import java.io.*;
class Demo{

	void Reverse(int arr[]){

		for(int i=0;i<arr.length;i++){

			int rev=0;

			while(arr[i] != 0){
				int rem=arr[i]%10;
				if(rem==0){
					arr[i]='0';
				}
				rev=(rev*10)+rem;
				arr[i]=arr[i]/10;
			}
			arr[i]=rev;
		}
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		Demo obj=new Demo();
		obj.Reverse(arr);

		System.out.println("Reverse array is:");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}





