/*
 *Write a program to print the second max element in the array
Input: Enter array elements: 2 255 2 1554 15 65
Output: 255
 */

import java.io.*;
class Demo{
	int SecondMax(int arr[]){

		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){

				if(arr[i]>arr[j]){
					int temp=arr[j];
					arr[j]=arr[i];
					arr[i]=temp;
				}
			}
		}
		return arr[arr.length-2];
	}
       	public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                Demo obj=new Demo();
		int ret=obj.SecondMax(arr);
		System.out.println("Second maximum element is: "+ret);
	}
}


