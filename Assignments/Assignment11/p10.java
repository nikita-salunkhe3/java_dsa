/*
 *Write a program to print the second min element in the array
Input: Enter array elements: 255 2 1554 15 65 95 89
Output: 15
 */

import java.io.*;
class Demo{
	int SecondMin(int arr[]){

		for(int i=0;i<arr.length;i++){

			for(int j=i+1;j<arr.length;j++){

				if(arr[i]>arr[j]){
					int temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
			
		}
		return arr[1];
	}
        public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                Demo obj=new Demo();
		System.out.println("second minimum is: "+obj.SecondMin(arr));
	}
}


