/*
 *WAP to nd a Perfect number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 496 564
Output: Perfect no 496 found at index: 3
 */

import java.io.*;
class Demo{
	void Perfect(int arr[]){

		for(int i=0;i<arr.length;i++){
			int sum=0;
			for(int j=1;j<=arr[i]/2;j++){
				if(arr[i]%j==0){
					sum=sum+j;
				}
			}
			if(sum==arr[i]){
				System.out.println("Perfect no "+arr[i]+" found at index: "+i);
			}
		}
	}
	public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                Demo obj=new Demo();

		obj.Perfect(arr);
	}
}

