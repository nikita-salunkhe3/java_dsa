/*
 *WAP to nd an ArmStong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 153 55 89
Output: Armstrong no 153 found at index: 4
 */

import java.io.*;
class Demo{
	void Armstrong(int arr[]){

		for(int i=0;i<arr.length;i++){
			int store=arr[i];
			int num=arr[i];

			int count=0;
			while(store !=0){
				count++;
				store=store/10;
			}

			int sum=0;
			while(num != 0){
				int rem=num%10;
				int p=1;
				for(int j=1;j<=count;j++){
					p=p*rem;
				}
				sum=sum+p;
				num=num/10;
			}
			if(sum==arr[i]){
				System.out.println("Armstrong no "+arr[i]+" found at index "+i);
				flag=1;
			}
		}
	}

	 public static void main(String[] args)throws IOException{

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                Demo obj=new Demo();
		obj.Armstrong(arr);
	 }
}

