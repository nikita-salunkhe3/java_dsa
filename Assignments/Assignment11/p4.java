/*
 *WAP to a prime number from an array and return its index.
Take size and elements from the user
Input: 10 25 36 566 34 53 50 100
Output: prime no 53 found at index: 5
 */

import java.io.*;
class Demo{
	void Prime(int arr[]){
		for(int i=0;i<arr.length;i++){

			int count=0;
			for(int j=2;j<=arr[i]/2;j++){
				if(arr[i]%j==0){
					count++;
					break;
				}
			}
			if(count==0 && arr[i]!=1){
				System.out.println("prime no "+arr[i]+" found at index: "+i);
			}
		}
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                Demo obj=new Demo();
		obj.Prime(arr);

	}
}


					


