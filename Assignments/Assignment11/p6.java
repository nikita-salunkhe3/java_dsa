/*
 * WAP to nd a palindrome number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564
Output: Palindrome no 252 found at index: 2
 */

import java.io.*;
class Demo{
	void Palindrome(int arr[]){
		for(int i=0;i<arr.length;i++){
			int store=arr[i];
			int rev=0;

			while(store !=0){
				int rem=store%10;
				rev=rev*10+rem;
				store=store/10;
			}
			if(rev==arr[i]){
				System.out.println("Palindrome no "+arr[i]+ " found at index: "+i);
			}
		}
	}
	public static void main(String[] args)throws IOException{
		 BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=Integer.parseInt(br.readLine());

                int arr[]=new int[size];

                System.out.println("enter array elements");

                for(int i=0;i<arr.length;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
                Demo obj=new Demo();
		obj.Palindrome(arr);
	}
}

		


	

