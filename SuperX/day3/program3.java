/*
 Que 3 : WAP to check whether the given no is a palindrome number or not.
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		int temp=num;
		int rev=0;
		while(temp != 0){
			rev=rev*10 + temp%10;
			temp=temp/10;
		}
		if(rev==num){
			System.out.println(num+" is Pallindrome number");
		}else{
			System.out.println(num+" is not Pallindrome Number");
		}
	}
}

