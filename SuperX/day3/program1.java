/*
 Que 1 : WAP to print the following pattern
Take input from user
A B C D
D C B A
A B C D
D C B A
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the row");

		int row=sc.nextInt();

		int num=65;
		for(int i=1;i<=row;i++){
			
			for(int j=1;j<=row;j++){
				if(i%2 == 1){
					System.out.print((char)(num)+"  ");
					num++;
				}else{
					num--;
					System.out.print((char)(num)+"  ");
				}
			}
			System.out.println();
		}
	}
}

