/*
 Que 4 : WAP to print each reverse numbers in the given range
Input: start:25435
end: 25449
 */

import java.util.*;
class Client{
	static int reverseNo(int num){
		int rev=0;
		while(num != 0){
			rev=rev*10 + num%10;
			num=num/10;
		}
		return rev;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the starting number");

		int start=sc.nextInt();

		System.out.println("Enter ending Number");

		int end=sc.nextInt();

		for(int i=start;i<=end;i++){
			System.out.print(reverseNo(i)+"   ");
		}
		System.out.println();
	}
}

