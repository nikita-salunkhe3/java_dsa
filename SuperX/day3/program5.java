/*
 Que 5 : WAP to check whether the string contains characters other than
letters.
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc= new Scanner(System.in);

		System.out.println("Enter the string");

		String str=sc.next();

		char arr[]=str.toCharArray();

		int flag=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]>= 'A' && arr[i]<='Z'){
				flag=0;
			}else if(arr[i]>='a' && arr[i]<='z'){
				flag=0;
			}else{
				flag=1;
				break;
			}
		}
		if(flag==1){
			System.out.println("String contain Special Character");
		}else{
			System.out.println("No Special Character are present");
		}
	}
}

