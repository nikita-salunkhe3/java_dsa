/*
 WAP to reverse a number and put successive number sum int an array and print it.
input: 4 5 6 8 9 
output: 17, 14, 11, 9, 4
 */

import java.util.*;
class Client{
	static void successiveSum(int arr[]){

		for(int i=0;i<arr.length-1;i++){
			arr[i]=arr[i]+arr[i+1];
		}
	}		
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		int count=0;

		int temp=num;

		while(num != 0){
			count++;
			num=num/10;
		}

		int arr[]=new int[count];

		int j=0;
		while(temp != 0){
			arr[j]=temp%10;
			j++;
			temp=temp/10;
		}

		successiveSum(arr);

		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}
