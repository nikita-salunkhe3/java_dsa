/*
 WAP to find a number which has numbers on its left is less than or equal to itself.
 456975962
 9
 */

import java.util.*;

class Client{
	static int maxEle(int arr[]){

		int max=Integer.MIN_VALUE;
		for(int i=0;i<arr.length;i++){
			if(arr[i] > max){
				max=arr[i];
			}
		}
		return max;
	}

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		int rev=0;
		int temp=num;

		int count=0;
		while(temp != 0){
			count++;
			temp=temp/10;
		}
		
		int arr[]=new int[count];

		int i=0;
		while(num != 0){
			arr[i]=num%10;
			i++;
			num=num/10;
		}

		System.out.println(maxEle(arr));
	}
}


