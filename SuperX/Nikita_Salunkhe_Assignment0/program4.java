/*
 A   b   C   d   E
 e   D   c   B
 B   c   D
 d   C
 c
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the row");

		int row=sc.nextInt();

		int num=65;
		for(int i=1;i<=row;i++){
			for(int j=row;j>=i;j--){

				if(i%2==1){
					if((i+j)%2 == 0){
						System.out.print((char)(num)+"  ");

					}else{
						System.out.print((char)(num+32)+"  ");
					}
					num++;
				}else{
					num--;
					if((i+j)%2==0){
						System.out.print((char)(num)+"  ");
					}else{
						System.out.print((char)(num+32)+"  ");
					}
				}
			}
			System.out.println();
		}
	}
}

