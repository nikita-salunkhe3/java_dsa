/*
Array:[4 2 3 5 6 7]
Array output:[1 1 0 0 1 0]
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter size of an array");

		int size=sc.nextInt();

		System.out.println("ENter array elements");

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		
		int arr2[]=new int[size];

		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				arr2[i]=1;
			}
		}
		System.out.println("Array becomes");

		for(int i=0;i<arr2.length;i++){
			System.out.print(arr2[i]+"  ");
		}
		System.out.println();
	}
}
