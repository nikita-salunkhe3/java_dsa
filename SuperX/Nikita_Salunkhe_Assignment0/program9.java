/*
 WAP to take String from user and convert all even indexes of String to Uppercase and odd Indexes of a
 String to lowercase
Input: dfTbnSrOvryt
Output:DfTbNsRoVrYt
 */

import java.util.*;
class Client{
	static void UpperLowerIndex(String str){

		char arr[]=str.toCharArray();

		for(int i=0;i<arr.length;i++){
			if(i%2==0){
				if(arr[i]>=(char)(65) && arr[i]<=(char)(90)){
					continue;
				}else{
					arr[i]=(char)(((int)(arr[i]))-32);
				}
			}else{
				if(arr[i]>=(char)(97)  && arr[i]<=(char)(122)){
					continue;
				}else{
					arr[i]=(char)(((int)(arr[i]))+32);
				}
			}
		}
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]);
		}
		System.out.println();
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the string");

		String str=sc.next();

		UpperLowerIndex(str);
	}
}
