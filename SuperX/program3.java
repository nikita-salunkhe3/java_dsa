/*
 *  D   C   B  A
 *  e   f   g  h
 *  F   E   D  C
 *  g   h   i  j
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the row");

		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=63+row+i;
			for(int j=1;j<=row;j++){

				if(i%2==1){
					System.out.print((char)(num)+"  ");
					num--;
				}else{
					System.out.print((char)(num+32)+"  ");
					num++;
				}
			}
			System.out.println();
		}
	}
}

