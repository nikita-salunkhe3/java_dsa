/*
 Que 3 : WAP to check whether the given no is odd or even
 */

import java.io.*;
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number");

		int num=Integer.parseInt(br.readLine());

		if(num % 2 == 0){
			System.out.println(num+" is even number");
		}else{
			System.out.println(num+" is odd number");
		}
	}
}
