/*
 Que 1 : WAP to print the following pattern
Take input from user
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the row");

		int row=sc.nextInt();

		int num=1;

		int j=1;
		for(int i=1;i<=row*row;i++){
			if(i%row==0){
				System.out.print(num+"  ");
				j++;
				num=j;
				System.out.println();
			}else{
				System.out.print(num+"  ");
				num++;
			}
		}
	}
}

				
