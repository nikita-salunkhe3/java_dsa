/*
 Que 5 : WAP to count the size of given string
(without using inbuilt method)
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the string");

		String str=sc.next();

		char arr[]=str.toCharArray();

		int count=0;
		for(int i=0;i<arr.length;i++){
			count++;
		}
		System.out.println(count);
	}
}
