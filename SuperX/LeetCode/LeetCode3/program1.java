/*
 1. To Lower Case (Leetcode-709)
Given a string s, return the string after replacing every uppercase
letter with the same lowercase letter.
Example 1:
Input: s = "Hello"
Output: "hello"
Example 2:
Input: s = "here"
Output: "here"
Example 3:
Input: s = "LOVELY"
Output: "lovely"
Constraints:
1 <= s.length <= 100
s consists of printable ASCII characters
 */

import java.util.*;
class Client{
	static String LowerCase(String str){

		char str1[]=str.toCharArray();
		for(int i=0;i<str1.length;i++){
			if(str1[i] >= 'a' && str1[i] <='z'){
				continue;
			}else{
				str1[i] = ((char)((int)(str1[i])+32));
			}
		}
		String str3=String.valueOf(str1);
		return str3;	
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the String");
		
		String str=sc.next();

		System.out.println("Lower case String becomes: "+LowerCase(str));
	}
}

