/*
 Search Insert Position (LeetCode 35)
Given a sorted array of distinct integers and a target value, return the index if the target
is found. If not, return the index where it would be if it were inserted in order.
You must write an algorithm with O(log n) runtime complexity.
Example 1:
Input: nums = [1,3,5,6], target = 5
Output: 2
Example 2:
Input: nums = [1,3,5,6], target = 2
Output: 1
Example 3:
Input: nums = [1,3,5,6], target = 7
Output: 4
Constraints:
1 <= nums.length <= 104
-104 <= nums[i] <= 104
nums contains distinct values sorted in ascending order.
-104 <= target <= 104
 */

import java.util.*;
class Client{
	static int InsertPos(int arr[],int target){

		for(int i=0;i<arr.length-1;i++){
			if(arr[arr.length-1] == target){
				return arr.length-1;
			}else if(arr[arr.length-1] < target){
				return arr.length;
			}else if(arr[i] < target && arr[i+1] > target){
				return i+1;
			}else if(arr[i]==target){
				return i;
			}else if(arr[0] > target){
				return 0;
			}
		}
		return 0;
	}

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter array size");

		int size=sc.nextInt();

		System.out.println("Enter array elements");

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter target element position");

		int target=sc.nextInt();

		System.out.println("Position is at : "+InsertPos(arr,target));
	}
}

