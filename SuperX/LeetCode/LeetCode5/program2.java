/*
 Single Number (LeetCode-136)
Given a non-empty array of integers nums, every element appears twice
except for one. Find that single one.
You must implement a solution with a linear runtime complexity and use
only constant extra space.
Example 1:
Input: nums = [2,2,1]
Output: 1
Example 2:
Input: nums = [4,1,2,1,2]
Output: 4
Example 3:
Input: nums = [1]
Output: 1
Constraints:
1 <= nums.length <= 3 * 104
-3 * 104 <= nums[i] <= 3 * 104
Each element in the array appears twice except for one element which
appears only once. 
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the array size");

		int size=sc.nextInt();

		System.out.println("ENter array elements");

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int arr1[]=new int[arr.length];
		
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr.length;j++){
				if(arr[i]==arr[j]){
					count++;
				}
			}
			arr1[i]=count;
		}
		int store=-1;
		for(int i=0;i<arr1.length;i++){
			if(arr1[i] == 1){
				store=i;
			}
		}
		if(store==-1){
			System.out.println("Single Occurence element is not found");
		}else{
			System.out.println("Element is : "+arr[store]);
		}
				
	}
}
