/*
 Contains Duplicate (LeetCode-217)
Given an integer array nums, return true if any value appears at least twice
in the array, and return false if every element is distinct.
Example 1:
Input: nums = [1,2,3,1]
Output: true
Example 2:
Input: nums = [1,2,3,4]
Output: false
Example 3:
Input: nums = [1,1,1,3,3,4,3,2,4,2]
Output: true
Constraints:
1 <= nums.length <= 105
-109 <= nums[i] <= 109
 */

import java.util.*;
class Client{
	static boolean containsDuplicate(int arr[]){

		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr.length;j++){

				if(arr[i]==arr[j]){
					count++;
				}
			}
			if(count >= 2){
				return true;
			}
		}
		return false;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the size of an array");

		int size=sc.nextInt();

		int arr[]=new int[size];
		
		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println(containsDuplicate(arr));
	}
}

