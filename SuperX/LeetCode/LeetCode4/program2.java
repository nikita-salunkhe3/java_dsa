/*
 Reverse String (LeetCode - 344)
Write a function that reverses a string. The input string is given as an array
of characters s.
You must do this by modifying the input array in-place with O(1) extra
memory.
Example 1:
Input: s = ["h","e","l","l","o"]
Output: ["o","l","l","e","h"]
Example 2:
Input: s = ["H","a","n","n","a","h"]
Output: ["h","a","n","n","a","H"]
Constraints:
1 <= s.length <= 10
5
s[i] is a printable ascii character.
 */

import java.util.*;
class Client{
	static String[] reverseString(String arr[]){

		int j=arr.length-1;
		for(int i=0;i<=(arr.length/2);i++){
			String str=arr[i];
			arr[i]=arr[j];
			arr[j]=str;
			j--;
		}
		return arr;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the array size");

		int size=sc.nextInt();

		String str[]=new String[size];

		System.out.println("Enter array elements as a String");

		for(int i=0;i<str.length;i++){
			str[i]=sc.next();
		}

		String ret[]=reverseString(str);
		for(int i=0;i<ret.length;i++){
			System.out.print(ret[i]+"  ");
		}
		System.out.println();
	}
}




