/*
 Day-4
Multiply Strings (LeetCode - 43)
Given two non-negative integers num1 and num2 represented as strings,
return the product of num1 and num2, also represented as a string.
Note: You must not use any built-in BigInteger library or convert the inputs
to integers directly.
Example 1:
Input: num1 = "2", num2 = "3"
Output: "6"
Example 2:
Input: num1 = "123", num2 = "456"
Output: "56088"
Constraints:
1 <= num1.length, num2.length <= 200
num1 and num2 consist of digits only.
Both num1 and num2 do not contain any leading zero, except the number 0
itself.
 */

import java.util.*;
class Client{
	static void MultiplyString(String num1,String num2){

		char arr1[]=new char[num1.length()];
		for(int i=0;i<num1.length();i++){
			arr1[i]=num1.charAt(i);
		}
		char arr2[]=new char[num2.length()];
		for(int i=0;i<num2.length();i++){
			arr2[i]=num1.charAt(i);
		}

		int x = (((int)(arr1[0]))-48);
		int y = (((int))(arr2[0])-48);

	//	System.out.println(x*y);
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter two String ");

		String num1=sc.next();
		String num2=sc.next();

		MultiplyString(num1,num2);
	}
}

