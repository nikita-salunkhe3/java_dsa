/*
 2. Find the Index of the First Occurrence in a String(LeetCode 28)
Given two strings needle and haystack, return the index of the first occurrence of needle
in haystack, or -1 if needle is not part of haystack.
Example 1:
Input: haystack = "sadbutsad", needle = "sad"
Output: 0
Explanation: "sad" occurs at index 0 and 6.
The first occurrence is at index 0, so we return 0.
Example 2:
Input: haystack = "leetcode", needle = "leeto"
Output: -1
Explanation: "leeto" did not occur in "leetcode", so we return -1.
Constraints:
1 <= haystack.length, needle.length <= 104
haystack and needle consist of only lowercase English characters.
 */

import java.util.*;
class Client{
	static int firstOcc(String str,String target){

		int j=0;
	
                for(int i=0;i<str.length();i++){
                        if(str.charAt(i) == target.charAt(j)){
             
                                j++;

                                if(target.length() == j){
                                        return i-(target.length()-1);
                                }

                        }else{
                                j=0;
                        }
                }
		return -1;
	}

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the string");

		String str=sc.next();

		System.out.println("Enter targetString");

		String target=sc.next();

		int ret=firstOcc(str,target);

		if(ret==-1){
			System.out.println("Target String index not found");
		}else{
			System.out.println("Target String found at index : "+ret);
		}
	}
}




