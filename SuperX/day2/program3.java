/*
 Que 3 : WAP to check whether the given no is prime or composite
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		int count=0;
		for(int i=2;i<=num/2;i++){
			if(num%i == 0){
				count++;
				break;
			}
		}
		if(num==1){
			System.out.println("1 is Co-prime Number");
		}else if(count == 0){
			System.out.println(num+" is prime Number");
		}else{
			System.out.println(num+" is Composite Number");
		}
	}
}
