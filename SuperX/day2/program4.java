/*
 Que 4 : WAP to print the composite numbers in the given range
Input: start:1
end:100
 */

import java.util.*;
class Client{
	static void validComposite(int num){

		int count=0;
		for(int i=2;i<=num/2;i++){
			if(num%i == 0){
				count++;
				break;
			}
		}
		if(count > 0){
			System.out.println(num);
		}
	}
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the start number");

		int start=sc.nextInt();
		
		System.out.println("Enter the end number");

		int end=sc.nextInt();

		System.out.println("Composite number are");
		for(int i=start;i<=end;i++){
			validComposite(i);
		}
	}
}
