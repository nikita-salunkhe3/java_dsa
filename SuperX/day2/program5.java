/*
 Que 5 : WAP to check whether the string contains vowels and return
the count of vowels.
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the String");

		String str=sc.next();

		char arr[]=str.toCharArray();

		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a' || arr[i]=='A'|| arr[i]=='E'|| arr[i]=='e'||arr[i]=='i' || arr[i]=='I'|| arr[i]=='o'||arr[i]=='O' || arr[i]=='u'|| arr[i]=='U'){
				count++;
			}
		}
		System.out.println(count);
	}
}
