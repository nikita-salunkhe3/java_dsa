/*
 Que 1 : WAP to print the following pattern
Take input from user
A B C D
B C D E
C D E F
D E F G
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the row");

		int row=sc.nextInt();

		int num=65;

		int j=1;

		for(int i=1;i<=row*row;i++){
			if(i%row == 0){
				System.out.print((char)(num)+"  ");
				num=65+j;
				j++;
				System.out.println();
			}else{
				System.out.print((char)(num)+"  ");
				num++;
			}
		}
	}
}
				
