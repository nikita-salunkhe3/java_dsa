/*
 WAP to find weather the number is armstrong number or not.
 */

import java.util.*;
class Client{

	static int validArmStrong(int num){

		int temp=num;
		int count=0;
		while(num != 0){
			count++;
			num=num/10;
		}

		int sum=0;
		while(temp != 0){
			int rem=temp%10;
			int pro=1; 
			for(int i=1;i<=count;i++){
				pro=pro*rem;
			}
			sum=sum+pro;
			temp=temp/10;
		}
		return sum;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		int ret=validArmStrong(num);
		if(ret==num){
			System.out.println("Armstrong number");
		}else{
			System.out.println("Not Armstrong number");
		}
	}
}

