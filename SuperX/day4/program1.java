/*
 Que 1: WAP to print the following pattern
Take input from the user

A  B  C  D
#  #  #  # 
A  B  C  D
#  #  #  #
A  B  C  D

 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the row");

		int row=sc.nextInt();

		System.out.println("Enter the column");

		int col=sc.nextInt();

		for(int i=1;i<=row;i++){
			char ch='A';
			for(int j=1;j<=col;j++){
				if(i%2 == 1){
					System.out.print(ch+"  ");
					ch++;
				}else{
					System.out.print("#  ");
				}
			}
			System.out.println();
		}
	}
}
