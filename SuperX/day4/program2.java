/*
 Que 2: WAP to print the following pattern
Take row input from the user
A
B  A  
C  B  A
D  C  B  A
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the row");

		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=64+i;
			for(int j=1;j<=i;j++){
				System.out.print((char)(num)+"  ");
				num--;
			}
			System.out.println();
		}
	}
}
