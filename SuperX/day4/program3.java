/*
 Que 3: WAP to find the factorial of a given number.
 */

import java.util.*;
class Client{
	static int findFactorial(int num){

		int fact=1;
		for(int i=1;i<=num;i++){
			fact=fact*i;
		}
		return fact;
	}	
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		System.out.println("Factorial of "+num+" is : "+findFactorial(num));
	}
}

