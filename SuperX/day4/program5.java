/*
 Que 5: WAP to toggle the String to uppercase or lowercase
Input: Java 
output: jAVA

Input: data
output: DATA
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the String");

		String str=sc.next();

		char arr[]=str.toCharArray();

		for(int i=0;i<arr.length;i++){
			if(arr[i] >= 'A' && arr[i] <='Z'){
				arr[i]=((char)((int)(arr[i])+32));
			}else{
				arr[i]=((char)((int)(arr[i])-32));
			}
		}
		System.out.println(arr);
	}
}
