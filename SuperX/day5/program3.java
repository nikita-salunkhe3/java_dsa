/*
 Que 3: WAP to check whether the given number is a strong number or not.
 */

import java.io.*;
class Client{
	static int ValidStrongNo(int num){

		int sum=0;
		while(num > 0){
			int fact=1;
			for(int i=1;i<=num%10;i++){
				fact=fact*i;
			}
			sum=sum+fact;
			num=num/10;
		}
		return sum;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number");

		int num=Integer.parseInt(br.readLine());

		int ret=ValidStrongNo(num);
		if(ret == num){
			System.out.println(num+" is a Strong number");
		}else{
			System.out.println(num+" is Not Strong Number");
		}
	}
}



