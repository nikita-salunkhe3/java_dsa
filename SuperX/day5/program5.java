/*
 Que 5: WAP to print the occurrence of a letter in given String.
Input String: “Know the code till the core”
Alphabet : o
Output: 3
 */

import java.io.*;
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");

		String str=br.readLine();

		System.out.println("Enter searching Alphabet");

		char ch=(char)br.read();

		int count=0;
		for(int i=0;i<str.length();i++){
			if(str.charAt(i) == ch){
				count++;
			}
		}
		System.out.println("Occurence Count of "+ch+" is : "+count);
	}
}	
