/*
 Que 2: WAP to print the following pattern
Take row input from the user
a
A B
a b c
A B C D
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the row");

		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=65;
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(((char)(num+32))+"  ");
				}else{
					System.out.print(((char)(num))+"  ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
