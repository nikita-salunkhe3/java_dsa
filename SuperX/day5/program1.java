/*
 Que 1: WAP to print the factorial of digits in a given range.
Input: 1-10
 */

import java.util.*;
class Client{
	static int findFactorial(int num){

		int fact=1;
		for(int i=1;i<=num;i++){
			fact=fact*i;
		}
		return fact;
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the starting number");

		int start=sc.nextInt();
		
		System.out.println("Enter the ending number");

		int end=sc.nextInt();

		for(int i=start;i<=end;i++){
			System.out.println("Factorial of "+i+" is : "+findFactorial(i));
		}
	}
}

