/*
Que 4: WAP to print the following pattern
Take input from the user
1 3 5 7
2 4 6 8
9 11 13 15
10 12 14 16 
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the row");

		int row=sc.nextInt();

		int num1=1;
		int num2=2;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){

				if(i % 2 == 1){
					System.out.print(num1+"   ");
					num1=num1+2;
				}else{
					System.out.print(num2+"   ");
					num2=num2+2;
				}
			}
			System.out.println();
		}
	}
}

