/*
 WAP to print the factorial of even number in a given number 
input: 256946
output: 2,270,24,720
 */

import java.util.*;
class Client{
	static void factSum(int num){

		while(num != 0){
			int fact=1;
			int rem=num%10;

			if(rem % 2==0){
				for(int i=1;i<=rem;i++){
					fact=fact*i;
				}
				System.out.print(fact+"  ");
			}
			num=num/10;
		}
		System.out.println();
	}
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the number");

		int num=sc.nextInt();

		int rev=0;

		while(num != 0){
			rev=rev*10 + num%10;
			num=num/10;
		}
		factSum(rev);
	}
}

