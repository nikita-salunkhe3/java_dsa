/*
 1  2  3  4
 4  5  6  7
 6  7  8  9
 7  8  9  10
 */

import java.io.*;
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row ");

		int row=Integer.parseInt(br.readLine());

		int num=1;
		int j=0;

		for(int i=1;i<=row*row;i++){

			if(i%row==0){
				System.out.print(num+"  ");
				num=num-j;
				j++;
				System.out.println();
			}else{
				System.out.print(num+"  ");
				num++;
			}
		}
	}
}

