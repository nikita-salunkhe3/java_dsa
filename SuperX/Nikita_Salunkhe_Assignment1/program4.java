/*
 4 
 5  7  
 6  9  12
 7  11 15  19
 */

import java.io.*;
class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("ENter the row");

		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){

			int num=row-1+i;

			for(int j=1;j<=i;j++){
				System.out.print(num+"  ");
				num=num+i;
			}
			System.out.println();
		}
	}
}
			
			
