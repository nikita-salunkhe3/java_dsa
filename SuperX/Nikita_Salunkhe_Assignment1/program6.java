/*
 WAP tp print prime number in range take range from user
 */

import java.util.*;
class Client{
	static void validPrimeNo(int num){

		int count=0;
		for(int i=2;i<=num/2;i++){
			if(num%i==0){
				count++;
				break;
			}
		}
		if(num==1 || num==0 || num==-1){
			System.out.println(num+ " is Not a Prime Number");
		}else if(count==0){
			System.out.println(num+ " is a prime number");
		}else{
			System.out.println(num+" is Not a Prime Number");
		}
	}
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the starting number");

		int start=sc.nextInt();

		System.out.println("Enter ending number");

		int end=sc.nextInt();

		for(int i=start;i<=end;i++){
			validPrimeNo(i);
		}
	}
}
