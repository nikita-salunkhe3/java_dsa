/*
 * 5
 * 6  8
 * 7  10  13
 * 8  12  16  20
 * 9  14  19  24  29
 */

import java.util.*;
class Client{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter the row");

		int row=sc.nextInt();

		for(int i=1;i<=row;i++){
			int num=row-1+i;
			for(int j=1;j<=i;j++){
				System.out.print(num+"   ");
				num=num+i;
			}
			System.out.println();
		}
	}
}
