/*
 WAP to find Occurence of vowels in a given String.

 where the letter are only lowercase 
 */

import java.util.*;
class Client{
	static void printVowel(String str){

		int counta=0;
		int counte=0;
		int counti=0;
		int counto=0;
		int countu=0;

		for(int i=0;i<str.length();i++){
			if(str.charAt(i)=='a' || str.charAt(i)=='A'){
				counta++;
			}
			if(str.charAt(i)=='e' || str.charAt(i)=='E'){
				counte++;
			}
			if(str.charAt(i)=='i' || str.charAt(i)=='I'){
				counti++;
			}
			if(str.charAt(i)=='o' || str.charAt(i)=='O'){
				counto++;
			}
			if(str.charAt(i)=='u' || str.charAt(i)=='U'){
				countu++;
			}
		}
		System.out.println("a = "+counta);
		System.out.println("e = "+counte);
		System.out.println("i = "+counti);
		System.out.println("o = "+counto);
		System.out.println("u = "+countu);
	}
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the string in lowercase letter");

		String str=sc.next();

		printVowel(str);
	}
}
