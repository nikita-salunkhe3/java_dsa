//find the sum and avg of given number

class Demo{
	public static void main(String[] args){
		int sum=0;
		float avg=0f;

		int count=0;
		int num=123;
		while(num != 0){
			int rem=num%10;
			sum=sum+rem;
			count++;
			num=num/10;
		}
		System.out.println("sum is "+sum);
		
		avg=sum/count;
		System.out.println("avg is "+avg);
	}
}
