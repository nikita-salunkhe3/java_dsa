/*
 * 9  64  7
 * 36 5   16
 * 3  4   1
 */

class Pattern{
	public static void main(String[] args){

		int row=3;
		int num=row*row;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){

				if(i%2==1){
					if(j%2==1){
						System.out.print(num+" ");
					}else{
						System.out.print(num*num+" ");
					}
			
				}else{
					if(j%2==1){
						System.out.print(num*num+" ");
					}else{
						System.out.print(num+" ");
					}
					
				}
				num--;

			}
			System.out.println();
		}
	}
}



