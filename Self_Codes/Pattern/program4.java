/*
 *       1
 *    4  7
 * 10 13 16
 */

class Pattern{
	public static void main(String[] args){
		int num=1;
		int row=3;

		for(int i=1;i<=row;i++){
			for(int sp=1;sp<row-i+1;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=i;j++){
				System.out.print(num+"\t");
				num=num+row;
			}
			System.out.println();
		}
	}
}
