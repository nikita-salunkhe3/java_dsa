/*
 *        d
 *      c c
 *    b b b
 *  a a a a
 */

class Pattern{
	public static void main(String[] args){
		int row=4;
		char ch='d';

		for(int i=1;i<=row;i++){
			for(int sp=1;sp<row-i+1;sp++){
				System.out.print("  ");
			}
			for(int j=1;j<=i;j++){
				System.out.print(ch+" ");
			}
			ch--;
			System.out.println();
		}
	}
}
			
