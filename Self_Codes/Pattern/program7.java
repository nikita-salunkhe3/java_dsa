/*
 * 1  2  3  4
 * a  b  c  d
 * 5  6  7  8
 * e  f  g  h
 */

import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter no. of rows");

		int row=Integer.parseInt(br.readLine());
		int num=1;
		char ch='a';
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print(num+++"  ");
				}else{
					System.out.print(ch+++"  ");
				}
			}
			System.out.println();
		}
	}
}


