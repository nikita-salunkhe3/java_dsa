/*
 *             5
 *           5 6
 *         5 4 3
 *       5 6 7 8
 *     5 4 3 2 1
 */
class Pattern{
	public static void main(String[] args){
		int row=5;

		for(int i=1;i<=row;i++){
			int num=row;

			for(int sp=1;sp<row-i+1;sp++){
				System.out.print("  ");
			}
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(num--+" ");
				}else{
					System.out.print(num+++" ");
				}
			}
			System.out.println();
		}
	}
}

