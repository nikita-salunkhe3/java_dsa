/*
 *        4
 *      4 3
 *    4 3 2
 *  4 3 2 1
 */

class Pattern{
	public static void main(String[] args){
		int row=4;

		for(int i=1;i<=row;i++){
			int num=row;
			for(int sp=1;sp<row-i+1;sp++){
				System.out.print("  ");
			}
			for(int j=1;j<=i;j++){
				System.out.print(num--+" ");
			}
			System.out.println();
		}
	}
}

