class Demo{
	public static void main(String[] args){

		int x=10;
		{
			int y=20;
			System.out.println(x+" "+y);//10 20
		}
		{
			System.out.println(x+" "+y);//out of scope->symbol not found
		}
		System.out.println(x+" "+y);//out of scope....error->symbol not found
	}
}
		

