/*
 *            1
 *          A b
 *        1 2 3
 *      A b C d  
 */

class Pattern{
	public static void main(String[] args){
		int row=4;

		for(int i=1;i<=row;i++){
			int num=1;
			char ch1='A';
			char ch2='a';

			for(int sp=1;sp<row-i+1;sp++){
				System.out.print("  ");
			}

			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(num+++" ");
				}else{
					if((i+j)%2==1){
						System.out.print(ch1+" ");
					}else{
						System.out.print(ch2+" ");
					}
					ch1++;
					ch2++;
				}	
			}
			System.out.println();
		}
	}
}

