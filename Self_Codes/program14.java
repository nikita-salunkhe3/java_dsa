/*
 *           D
 *         c D
 *       B c D
 *     a B c D
 */
class Pattern{
	public static void main(String[] args){
		int row=4;
		char small='d';
		char big='D';

		for(int i=1;i<=row;i++){
			for(int sp=1;sp<row-i+1;sp++){
				System.out.print("  ");
			}

			for(int j=1;j<=i;j++){
				if((i+j)%2==0){
					System.out.print(big+++" ");
					big++;
				}else{
					System.out.print(small+++" ");
					small++;
				}
				//big-=2;
			//	small-=2;
			}
			big-=2;
			small-=2;
			System.out.println();
		}
	}
}

