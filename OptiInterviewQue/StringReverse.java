/*
 * String Reverse
 */

import java.io.*;

class Client{
	static String reverseString(String str){
		
		char arr[]=str.toCharArray();

		int j=arr.length-1;
		for(int i=0;i<arr.length/2;i++){
			char ch=arr[i];
			arr[i]=arr[j];
			arr[j]=ch;
			j--;
		}
		str=String.valueOf(arr);
		return str;
	}
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String");

		String str=br.readLine();

		String ret=Client.reverseString(str);

		System.out.println(ret);
	}
}
