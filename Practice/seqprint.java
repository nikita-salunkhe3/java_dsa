final class Demo{

	int x=10;
	static int y=20;


	static{
		System.out.println("In static block1");
	}
	static{
		System.out.println("In static block2");
	}
	{
		System.out.println("In instance block1");
	}
	{
		System.out.println("In instance block2");
	}

	Demo(){
		System.out.println("In constructor");
	}
	public static void main(String[] args){

		{
			System.out.println("In main instance block");
		}
		System.out.println("In main");
		Demo obj=new Demo();
		Demo obj2=new Demo();
	}
}
//output
//In static block1
//In static block2
//In main instance block
//In main
//In instance block1
//In instance block2
//constructor
