class Parent{
	final void fun(){

		System.out.println("In PArent");
	}

}
class Child extends Parent{

	void fun(){
		System.out.println("In final child");
	}

}
class Client{

	public static void main(String[] args){

		Parent obj=new Child();
		obj.fun();
	}
}
