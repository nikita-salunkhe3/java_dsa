class Parent{
	int x=5;

	Parent(){
		System.out.println("In Parent No-args Constructor");
	}
	Parent(int x){
		System.out.println("In Parent para constructor");
	}
}
class Demo extends Parent{

	int x=10;
	static int y=20;

	Demo(){
	//	this(x);
		System.out.println("In child No-args Constructor");
	}

	Demo(int x){
	
		System.out.println("In child Para-constructor");
	}

	Demo(int x,int y){
		
		System.out.println(this.y);
	}
	void fun(){
		this();
	}
}
class Client{
	public static void main(String[] args){

		System.out.println("In main");

		Demo obj2=new Demo(10,20);
	}
}
/*
 *error: cannot reference x before supertype constructor has been called
		this(x);
 */

