interface Demo1{

	static void fun(){
		System.out.println("In Interface fun static");
	}
}
interface Demo2{

}

class Child implements Demo1,Demo2{
	public void fun(){
		System.out.println("In Child");
	}

}
class Client{
	public static void main(String[] args){
		
		Child obj=new Child();
		obj.fun();
	//	Demo1.fun();
	}
}	

