class CompanyLoss extends RuntimeException{

	CompanyLoss(String msg){
		super(msg);
	}
}
class Client{

	static int pro = -5;
	public static void main(String[] args){
	
		try{	
			if(pro < 0){
				throw new CompanyLoss("Loss");
			}
		}catch(CompanyLoss ex){
			System.out.println("Exception Handled");
			System.out.println(ex);
		}finally{
			System.out.println("Connection cloased");
		}
	}
}




