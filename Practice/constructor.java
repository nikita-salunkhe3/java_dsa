class Parent{
	Parent(){
		System.out.println("Parent this : "+this);
		System.out.println("In parent No args-Constructor");
	}
}
class Demo extends Parent{

	int x=10;

	Demo(){
		this(x);
		System.out.println("In No-args Constructor");
	}
	Demo(int x){
	
	//	super();  // internally ya line la invokespecial menun rewrite kerto bytecode madhe
		System.out.println("In Para-Constructor");
		System.out.println("Child this : "+this);
		this.x=x;
		System.out.println(x);
	}
}
class Client{
	public static void main(String[] args){

		Demo obj=new Demo(20);
	}
}
