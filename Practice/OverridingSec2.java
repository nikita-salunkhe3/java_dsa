class Parent{

	final String fun(){
		System.out.println("In Parent fun");
		return "nikita";
	}
}
class Child extends Parent{

	Object gun(){
		System.out.println("In child fun");
		return 12;
	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();
		obj.fun();
	}
}
