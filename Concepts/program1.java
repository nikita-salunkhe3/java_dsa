class Demo{
	public static void main(String[] args){
		
		int arr1[]={100,200,300,400};
		float arr2[]={1.2f,2.2f};
		char arr3[]={'A','B','C','D'};
		double arr4[]={123.121,33213.113,321.3112};
		short arr5[]={1,2,3,4};
		byte arr6[]={1,2,3,4,5,6};
		boolean arr7[]={true,false};
		long arr8[]={1232,345422,456321234};
		String arr9[]={"Sarthak","Nikita"};

		System.out.println(arr1);
		System.out.println(arr2);
		System.out.println(arr3);
		System.out.println(arr4);
		System.out.println(arr5);
		System.out.println(arr6);
		System.out.println(arr7);
		System.out.println(arr8);
		System.out.println(arr9);


		System.out.println();


		Integer ar1[]={100,200,300,400};
		Float ar2[]={1.2f,2.2f};
		Character ar3[]={'A','B','C','D'};
		Double ar4[]={123.121,33213.113,321.3112};
		Short ar5[]={1,2,3,4};
		Byte ar6[]={1,2,3,4,5,6};
		Boolean ar7[]={true,false};
		Long ar8[]={1232,345422,456321234};
		String ar9[]={"Sarthak","Nikita"};


		System.out.println(ar1);
		System.out.println(ar2);
		System.out.println(ar3);
		System.out.println(ar4);
		System.out.println(ar5);
		System.out.println(ar6);
		System.out.println(ar7);
		System.out.println(ar8);
		System.out.println(ar9);


	}
}



