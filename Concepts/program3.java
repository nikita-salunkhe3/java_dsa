class Demo{
	public static void main(String[] args){
		int x=10;
		int y=10;
		int z=10;
		Integer a=10;
		float b=10;

		System.out.println(System.identityHashCode(x));//same
		System.out.println(System.identityHashCode(y));//same
		System.out.println(System.identityHashCode(z));//same
		System.out.println(System.identityHashCode(a));//same
		System.out.println(System.identityHashCode(b));//diff
	}
}

