import arithfun.Addition;
import java.io.*;

public class Client{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter 2 numbers:");

		int num1=Integer.parseInt(br.readLine());
		int num2=Integer.parseInt(br.readLine());

		Addition obj=new Addition(num1,num2);

		System.out.println("Addition is:");
		System.out.println(obj.add());
	}
}

