package spnum;

public class SpNumber{

	public class PrimeNo{

		public int isprimeNo(int num){
		
			if(num<=0 || num==1){
				return -1;
			}
			int count=0;
			for(int i=2;i<=num/2;i++){
				if(num%i==0){
					count++;
				}
			}
			if(count==0)
				return 1;
			else
				return -1;
		}
	}
	public class PallindromeNo{
		public int ispallindromeNo(int num){
			int y=num;
	
			int rev=0;
			while(y != 0){
				int rem=y%10;
				rev=rev*10 + rem;
				y=y/10;
			}
			if(rev==num)
				return 1;
			else
				return -1;
		}
	}
}


