//print number digits product
// input: 6531  (6*5*3*1)
// output: 90
// used only while loop

class Demo{
	public static void main(String[] args){
		int num=6531;
		int rem=1;

		while(num != 0){
			rem=rem*(num%10);
			num=num/10;
		}
		System.out.println(rem);
	}
}
