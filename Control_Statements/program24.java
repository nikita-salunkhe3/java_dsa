//take a integer as an input
//print perfect square till N
//input:30
//output:1  4  9  16  25
//used only while loop

class Demo{
	public static void main(String[] args){
		int num=30;
		int i=1;

		while(i*i<=num){
			System.out.println(i*i);
			i++;
		}
	}
}
			

