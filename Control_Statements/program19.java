//print the count of factor where number is given as an input
//input:6
//output:4
//used for loop only

class Demo{
	public static void main(String[] args){
		int num=6;
		int count=0;

		for(int i=1;i<=num;i++){
			if(num%i==0){
				count++;
			}
		}
		System.out.println(count);
	}
}
