/*
 * * _ _ *
 * * _ _ *
 * * _ _ *
 * * _ _ *
 */


class Pattern{
	public static void main(String args[]){
		int row=4;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(j==1 || j==4){
					System.out.print("*\t");
				}else{
					System.out.print("_\t");
				}
			}
			System.out.println();
		}
	}
}

	 
