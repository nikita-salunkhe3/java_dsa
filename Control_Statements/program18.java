//print all the factors of given number 
//input: 6
//output:1 2 3 6 
//uesd for loop as a control statement

class Demo{
	public static void main(String[] args){
		int num=6;

		for(int i=1;i<=num;i++){
			if(num%i==0){
				System.out.println(i);
			}
		}
	}
}
