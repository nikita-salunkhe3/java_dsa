/*
 * 1 
 * 2  c
 * 4  e  6
 * 7  h  9  j
 */

class Pattern{
	public static void main(String[] args){
		int num=1;
		char ch='a';
		int row=4;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(num+"  ");
				}else{
					System.out.print(ch+"  ");
				}
				ch++;
				num++;
			}
			System.out.println();
		}
	}
}


