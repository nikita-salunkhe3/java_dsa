//print given number is perfect number or not
//input: 6
//output: perfect number
//input: 5
//output :Not perfect number

class Demo{
	public static void main(String[] args){
		int num=6;

		int sum=0;
		for(int i=1;i<num;i++){
			if(num%i==0){
				sum=sum+i;
			}
		}
		if(num==sum){
			System.out.println(num+" is perfect number");
		}else{
			System.out.println(num+" is NOT perfect number");
		}
	}
}

