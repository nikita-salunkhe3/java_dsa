//whether element is divisible by 4 or not

class Divisible{
	public static void main(String[] args){
		int num=5;

		if(num%4==0){
			System.out.println(num+" is divisible by 4");
		}else{
			System.out.println(num+" is Not divisible by 4");
		}
	}
}
