//print factorial of given number
//using for loop

class Demo{
	public static void main(String[] args){
		int num=5;
		int fact=1;

		for(int i=1;i<=num;i++){
			fact=fact*i;
		}
		System.out.println(fact);
	}
}
