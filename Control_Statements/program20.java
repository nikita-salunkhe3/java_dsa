//print the number which are divisible by 3
//input:50
//output:3 6 9 12 18 21 24 27 30 33 36 39 42 45 48
//used only while loop

class Divisible{
	public static void main(String[] args){
		int i=1;
		int num=50;

		while(i<=num){
			if(i%3==0){
				System.out.println(i);
			}
			i++;
		}
	}

}

		
