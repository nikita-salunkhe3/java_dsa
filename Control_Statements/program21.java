//given a integer number N 
//print all its digits
//input: 6531
//output: 1   3   5   6
//used only while loop

class Demo{
	public static void main(String[] args){
		int num=6531;

		while(num != 0){
			System.out.println(num%10);
			num=num/10;
		}
	}
}
