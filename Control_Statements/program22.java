//Given the integer N 
//print sum of digit present in N
//input: 6531
//output: 15
//used only while loop

class Demo{
	public static void main(String[] args){
		int num=6531;
		int rem=0;

		while(num != 0){
			rem=rem+num%10;
			num=num/10;
		}
		System.out.println(rem);
	}
}
			
