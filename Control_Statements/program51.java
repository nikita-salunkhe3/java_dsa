/*
 * 1
 * 4  3
 * 16 5  36
 * 49 8  81  10
 */

class Pattern{
	public static void main(String[] args){
		int num=1;
		int row=4;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(num*num+"\t");
				}else{
					System.out.print(num+"\t");
				}
				num++;
			}
			System.out.println();
		}
	}
}
