//electricity bill
//if unit==100 ===== 1 rs for 1 unit
//if unit>100 ==== 2 rs for 1 unit
//input: 100  
//output:100
//
//input: 200
//output: 300  (100+100*2)

class Bill{
	public static void main(String[] args){
		int unit=600;

		if(unit<=100){
			System.out.println(unit*1);
		}else{
			System.out.println((unit*2)-100);
		}
	}
}

