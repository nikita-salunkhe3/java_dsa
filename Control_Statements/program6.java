/*
if num divisible by 3 & 5 === fizz-buzz
   num divisible by 3 only ====fizz
   num divisible by 5 only ====buzz
   NOT divisible by 3 & 5====Not divisible by both
*/

class Divisible{
	public static void main(String[] args){
		int num=5;

		if(num % 3==0 && num%5==0){
			System.out.println("fizz-buzz");
		}else if(num%3==0){
			System.out.println("fizz");
		}else if(num%5==0){
			System.out.println("buzz");
		}else{
			System.out.println("Not divisible by 3 & 5");
		}
	}
}



