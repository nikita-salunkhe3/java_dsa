//print odd numbers from given range using for loop
//input: 6
//output: 1 3 5

class Integer{
	public static void main(String[] args){
		int N=6;
		for(int i=1;i<=N;i++){
			if(i%2==1){
				System.out.println(i);
			}
		}
	}
}
