class Outer{ //Outer.class
	void getdata(){
		System.out.println("In getdata");
	}
	void m2(){

	}
	class In{  //Outer$In

	}
	class Inner{  //Outer$Inner
		class Inner1{//Outer$Inner$Inner1

		}

	  	void m1(){
			System.out.println("In m1");
		}
	}
}
class Client{  //Client.class
	public static void main(String[] args){

		Outer obj=new Outer();

		Outer.Inner obj1=obj.new Inner();

		obj.getdata();
		obj1.m1();
	}
}
