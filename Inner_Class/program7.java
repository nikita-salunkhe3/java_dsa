// version 1.8 OR version 11 
class Outer{

	static int y=20;

	class Inner{
		static int x=10;
	}
}
/*
 * output:error
 * error: Illegal static declaration in inner class Outer.Inner
		static int x=10;
		           ^
  modifier 'static' is only allowed in constant variable declarations
*/

//version 17 and version 19
//No error 

