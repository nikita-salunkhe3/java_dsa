//Normal inner class

class Outer{
	void m1(){
		System.out.println("In m1-Outer");
	}
		class Inner1{
			void m1(){
				System.out.println("In m1-Inner1");
			}
				class Inner2{
					void m1(){
						System.out.println("In m1-Inner2");
					}
				}
			}
			void m2(){
				System.out.println("In m2-Inner1");
			}
		}
class Client{
	public static void main(String[] args){

		Outer obj=new Outer();

		Outer.Inner1 obj1=obj.new Inner1();
		obj1.m1();

		Outer.Inner1.Inner2 obj2=obj1.new Inner2();
		obj2.m1();
	}
}
/*
 * output:
 * In m1-Inner1
 * In m1-Inner2
 */




