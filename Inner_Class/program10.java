class Outer{
	class Inner{
		void gun(){
			System.out.println("In fun");
		}
		Inner obj=new Inner(){
			final static int x=10;
			void gun(){
				System.out.println("In gun");
				System.out.println(x);
			}
		};
		obj.gun();
	}
}
class Client{
	public static void main(String[] args){

		Outer obj=new Outer();
	//	obj.Inner=obj.new Inner();//error obj.Inner ->> Cannot find symbol
		Outer.Inner obj1=new Outer().new Inner();
	}
}

