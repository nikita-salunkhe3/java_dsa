//Annonymous Inner class 

class Demo{
	void marry(){
		System.out.println("Kriti Sanon");
	}
}
class Client{
	public static void main(String[] args){

		Demo obj=new Demo(){//eth jeri Demo cha reference aasla teri annonymous class cha object
		       //bento ya madhe parent cha reference ani child cha object ass relation asst
		       //internally compiler annonymous class sathi object tayar karun deto
		       //compiler new Demo() la Client$1() ass kerto

			void marry(){
				System.out.println("Disha Patni");
			}
		};
		obj.marry();
	}
}



