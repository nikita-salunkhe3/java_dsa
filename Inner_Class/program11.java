//method local Inner class

class Outer{
	void m1(){
		System.out.println("In m1-outer");
		class Inner{
			void m1(){
				System.out.println("In m1-inner");
			}	
		}
		Inner obj=new Inner();
		obj.m1();
	}

	void m2(){
		System.out.println("In m2-Outer");
	}
}
class Client{
	public static void main(String[] args){

		Outer obj=new Outer();
		obj.m1();
		obj.m2();
	}
}

/*output:
 * In m1-outer
 * In m1-inner
 * In m2-Outer
 */
//.class file name are
//class Client-->> Client.class 
//class Outer-->> Outer.class
//class Inner -->> Outer$1Inner.class
