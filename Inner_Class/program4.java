class Outer{
	int x=10;
	static int y=20;

	class Inner{
		int a=30;
		final static int b=40;
	}
}
class Client{
	public static void main(String[] args){

		Outer obj=new Outer();
		System.out.println(obj.x);//10
		System.out.println(obj.y);//20
		System.out.println(Outer.Inner.b);//40
		System.out.println(obj.new Inner().b);//40

	}
}
/*
 * output:
 *  error: Illegal static declaration in inner class Outer.Inner
		static int b=40;
		           ^
  modifier 'static' is only allowed in constant variable declarations
1 error
*/
/*
 * Explanation:
 * 1.Inner class madhe static ani non static variable chalat pn static variable la final lihavch lagt
 * 2.Inner class madhe direct jer me static lihil terr ka nahi chalat karen-->>
 *   jer apan Inner class madhil variable access keraych asel ter main method madhe apan ass lihu
 *
 *   System.out.println(Outer.Inner.b)//b print keraycha asel ter
 *
 *   Outer ha ek class ahe ani class cha navane tyach gosti access hotat jya static ahet
 *   ani aplya secnario madhe Inner class ha apla static nahiye tyamule error ahe 
 *   ajun ek gost menje apan jer Inner class cha object tayar nahi kela ter Inner class cha constructor
 *   madhe 2 parameter asstat ter (this) menjech object cha addresss copy honar nahi tasech
 *   inner class cha concept nusar """"outer class chya object varun Inner class chya object ha depend
 *   ahe ter outer class cha address store karanyasathi this$0 chi cocept picture madhe aali ahe""""
 *   
 *   ****Konta object class ahe jo ki Inner class cha object la call kertoy he represent kernyasathi 
 *   this$0 chi concpt aali ahe****
 *   
 *   compiler Inner class cha aat navin ek non-static variable add kerto to Final aasto
 *   final Outer(Class_name) this$0;
 *
 *   ha variable final ka thevla ahe karen tyala koni change karu naye menun
 *   tasech final madhe jer ekhadi value first initialized zali ter parat ti change kerta yet nahi.
 */   
