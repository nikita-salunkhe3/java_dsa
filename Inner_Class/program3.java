//Realtime example of Inner class 

class Defence{

	class Army{
		void workPlace(){
		      	System.out.println("Border of India");
			Purpose();//inner class cha antun outer class madhil variable or method access
			//hotat
		}
	}
	class Navy{
		void workPlace(){
			System.out.println("Under the water");
		}
	}
	void Purpose(){
		      System.out.println("Protect the india");
	}
}
class Main{
	public static void main(String[] args){

		Defence.Army obj=new Defence().new Army();
		obj.workPlace();
	//	obj.Purpose();//error...inner class cha aat Purpose() he method sapadat nahiye menun
	//	cannot find symbol chi error yetiye

		Defence obj1=new Defence();

		Defence.Navy obj2=obj1.new Navy();
		obj2.workPlace();
	//	obj2.Purpose();//error
		
		obj1.Purpose();
	}
}
/*
 *Border of India
 Protect the india
 Under the water
 Protect the india
 */





