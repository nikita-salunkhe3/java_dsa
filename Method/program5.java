class Parent{

	void fun(){
		System.out.println("In parent fun");
	}
}
class Child extends Parent{

	void fun(){
		System.out.println("In Child fun");
	}
	void gun(){
		System.out.println("In Child gun");
	}
}
class Client{
	public static void main(String[] args){

		Parent obj=new Child();
		obj.fun();
		obj.gun();
	}
}
