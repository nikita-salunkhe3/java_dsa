/*
 Program 1:
Write a program to create an array of 5 integer elements.
And print all 5 elements from an array (Take Hardcoded values) 
Output :
1
2
3
4
5
 */

class CreateArray{
	public static void main(String[] args){

		int arr[]=new int[]{1,2,3,4,5};

		for(int i=0;i<arr.length;i++){
	
			System.out.println(arr[i]);
		}


	}
}


