/*
 Program 8:
Write a program to create an array of ‘n’ integer elements.
Where ‘n’ value should be taken from the user.
Insert the values from the user and find the frequency of digit
Input:
n=5
Enter elements in the array:
2
3
6
3
5
2
Output:
frequency of 2 is 2
frequency of 3 is 2
frequency of 6 is 1
frequency of 5 is 1
 */

import java.io.*;
class FreqOfDigit{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of an array");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

	
		for(int i=0;i<arr.length && arr[i]!=-1;i++){
			int store=arr[i];
			int count=0;

			for(int j=0;j<arr.length;j++){
				if(arr[i]==arr[j]){
					count++;
					if(i!=j){
						arr[j]=-1;
					}
					if(j==arr.length-1){
						arr[i]=-1;
					}
				}
			}
			System.out.println("Frequency of "+store+" is : "+count);
		}
	}
}

