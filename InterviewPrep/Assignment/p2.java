/*
 Program 2:
Write a program to create an array of 5 integer elements.
Insert from the user and print 5 elements from an array
Input:
1
2
3
4
5
Output :
1
2
3
4
5
 */

import java.io.*;
class CreateArray{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int arr[]=new int[5];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Array Elements are:");
		for(int x: arr){
			System.out.println(x);
		}
	}
}
