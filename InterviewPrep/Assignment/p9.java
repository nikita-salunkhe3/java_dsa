/*
 Program 9:
Write a program to create an array of ‘n’ integer elements.
Where ‘n’ value should be taken from the user.
Insert the values from the user and find the strong number from them
Input:
n=5
Enter elements in the array:
2
145
6
3
123
2
Output:
145
 */

import java.io.*;
class StrongNo{
	
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader( new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size]; 
		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Strong Number are:");
		for(int i=0;i<arr.length;i++){
			int num=arr[i];

			int sum=0;
			while(num != 0){
				int rem=num%10;
				int fact=1;
				for(int j=1;j<=rem;j++){
					fact=fact*j;
				}
				sum=sum+fact;
				num=num/10;
			}
			if(arr[i]==sum){
				System.out.println(arr[i]);
			}
		}
	}
}
				

