/*
WAP to remove a specific element from an array.
Input:
1
2
4
5
6
Enter element to remove : 4
Output:
1
2
5
6
*/

import java.io.*;

class RemoveEle{
        public static void main(String[] args){

                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size=0;
                try{
                        size=Integer.parseInt(br.readLine());
                }catch(IOException obj){

                }

                int arr[]=new int[size];

                System.out.println("Enter array elements");

                for(int i=0;i<arr.length;i++){
                        try{
                                arr[i]=Integer.parseInt(br.readLine());
                        }catch(IOException obj1){

                        }
                }
		System.out.println("Enter remove elements");
		int num=0;
		try{
			num=Integer.parseInt(br.readLine());
		}catch(IOException obj){

		}

		for(int i=0;i<arr.length;i++){
			if(arr[i]==num){
				while(i<arr.length-1){
				        arr[i]=arr[i+1];
					i++;
				}
			}
		}
		System.out.println("Array elements are:");

		for(int i=0;i<arr.length-1;i++){
			System.out.println(arr[i]);
		}
	}
}

