/*
 Write a program to create an array of ‘n’ integer elements.
Where ‘n’ value should be taken from the user.
Insert the values from users and print even numbers from the array.
Input:
n=5
Enter elements in the array:
1
2
3
4
5
Output:
2
4
 */

import java.io.*;
class EvenNo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Even numbers are:");
		for(int x:arr){
			if(x%2==0){
				System.out.println(x);
			}
		}
	}
}

