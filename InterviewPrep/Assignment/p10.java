/*
 Program 10:
Write a program to sort the array in ascending order
Input:
Enter the length of the array
n=3
Enter elements in the array
6
8
3
Output:
3
6
8
 */

import java.io.*;

class AscendOrder{
	public static void main(String[] args){

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=0;
		try{
			size=Integer.parseInt(br.readLine());
		}catch(IOException obj){

		}

		int arr[]=new int[size];

		System.out.println("Enter array elements");

		for(int i=0;i<arr.length;i++){
			try{
				arr[i]=Integer.parseInt(br.readLine());
			}catch(IOException obj1){
				
			}
		}
		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){
				if(arr[i]>arr[j]){
					int temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
		System.out.println("Array elements are:");
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}



