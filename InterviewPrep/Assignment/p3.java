/*
 Program 3:
Write a program to create an array of ‘n’ integer elements.
Where ‘n’ value should be taken from the user.
Insert the values from users and print accordingly
Input:
n=5
Enter elements in the array :
1
2
3
4
5
Output:
1
2
3
4
5
 */

import java.io.*;
class CreateArray{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=Integer.parseInt(br.readLine());

		System.out.println("Enter array elements");

		int arr[]=new int[size];

		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Array elements are:");
		for(int x:arr){
			System.out.println(x);
		}
	}
}


