/*
WAP to take a character array as input, but only print characters do not
print special characters
Input: a b $ % c & d 1 e
Output : a b c d e
Hint: you can take two arrays
*/

import java.io.*;
class PrintCharacter{
	public static void main(String[] args){

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size");

		int size=0;
		try{
			size=Integer.parseInt(br.readLine());
		}catch(IOException obj){

		}
		char arr[]=new char[size];

		System.out.println("Enter array elements");
		for(int i=0;i<arr.length;i++){
			try{
				arr[i]=(char)br.read();
				br.skip(1);
			}catch(IOException obj){

			}
		}
		char arr2[]=new char[size];
		int j=0;
		int count=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]>='a' && arr[i]<='z'|| arr[i]>='A' && arr[i]<='Z'){
				arr2[j]=arr[i];
				j++;
			}else{
				count++;
			}
		}
		System.out.println("Array elements are:");
		for(int i=0;i<arr2.length-count;i++){
			System.out.println(arr2[i]);
		}		
	}
}

