class Parent{
	 void fun(){
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
	final void fun(){
		System.out.println("Child Fun");
	}
}
class Client{
	public static void main(String[] args){

		Parent obj2=new Parent();
		obj2.fun();

		Child obj=new Child();
		obj.fun();

		Parent obj1=new Child();
		obj.fun();
	}
}

