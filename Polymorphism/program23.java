class Parent{
	private void fun(){
		System.out.println("In Parent Fun");
	}
}
class Child extends Parent{
	private void fun(){
		System.out.println("In Child Fun");
	}
}
class Client{
	public static void main(String[] args){

		Parent obj=new Child();
		obj.fun();
	}
}

