class Parent{
	void fun(){
		System.out.println("In Parent Fun");
	}
}
class Child extends Parent{
	void fun(){
		System.out.println("In Child Fun");
	}
}
class Client{
	public static void main(String[] args){

		Parent obj=new Child();
		obj.fun(10);//error
	}
}
//error : cannot find Symbol
