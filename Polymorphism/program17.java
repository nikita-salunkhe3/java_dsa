class Parent{
	char fun(){
		System.out.println("Parent Fun");
		return 'a';
	}
}
class Child extends Parent{
	char fun(){
		System.out.println("Char fun");
		return 'a';
	}
}
class Client{
	public static void main(String[] args){

		Parent obj=new Child();
		obj.fun();
	}
}

