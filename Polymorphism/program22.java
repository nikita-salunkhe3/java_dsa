class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void fun(){
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
	Child(){
		super(this);
		System.out.println("In Child Constructor");
	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();
	}
}
		
