class One{
	int x=400;
	void fun(int x){
		System.out.println(System.identityHashCode(x));//100
	}
}
class Two{
	public static void main(String[] args){

		int x=400;
		One obj=new One();
		obj.fun(x);
		System.out.println(System.identityHashCode(x));//200
	}
}
