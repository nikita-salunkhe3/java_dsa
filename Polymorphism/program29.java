class Parent{
	void fun(){
		System.out.println("In Parent fun");
	}
}
class Child extends Parent{
	static void fun(){
		System.out.println("In Child fun");
	}
}
class Clinet{
	public static void main(String[] args){

		Parent obj=new Child();
		obj.fun();
	}
}
