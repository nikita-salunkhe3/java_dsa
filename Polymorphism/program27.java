class Parent{
	static void fun(){
		System.out.println("Parent fun");
	}
}
class Child extends Parent{
       void gun(){
       		System.out.println("In Child gun");
       }
}
class Client{
	public static void main(String[] args){

		Child obj1=new Child();
		obj1.fun();
		
		Parent obj2=new Child();
		obj2.fun();
	}
}
