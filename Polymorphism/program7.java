class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void fun(){
		System.out.println("In Parent Fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In Child constructor");
	}
	void fun(){
		System.out.println("In Child Fun");
	}
}
class Client{
	public static void main(String[] args){

		Parent obj=new Child();
		obj.fun();
	}
}

/*
 * output:
 * In Parent constructor
 * In Child Constructor
 * In child Fun
 *
 */
