class Parent{
	String fun(){
		System.out.println("In Parent fun");
		return "Shashi";
	}
}
class Child extends Parent{
	Object fun(){
		System.out.println("In Child Fun");
		return new Object();
	}
}
class Client{
	public static void main(String args[]){

		Parent obj=new Child();
		obj.fun();
	}
}
