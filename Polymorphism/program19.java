class Parent{
	Object fun(){
		System.out.println("In Parent fun");
		return new Object();
	}
}
class Child extends Parent{
	String fun(){
		System.out.println("In Child Fun");
		return "Shahsi";
	}
}
class Client{
	public static void main(String args[]){

		Parent obj=new Child();
		obj.fun();
	}
}
