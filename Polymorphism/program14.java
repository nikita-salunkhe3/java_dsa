class Parent{
	void fun(int x){
		System.out.println("In parent Fun");
	}
}
class Child extends Parent{
	void fun(int x){
		System.out.println("In Child Fun");
	}
}
class Client{
	public static void main(String[] args){

		Parent obj=new Child();
		obj.fun();
	}
}
