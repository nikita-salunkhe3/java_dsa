class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void ParentProperty(){
		System.out.println("Flat,car,gold");
	}
	void Marry(){
		System.out.println("deepika Padukon");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In child Constructor");
	}
	void Marry(){
		System.out.println("Alia Bhatt");
	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();

		obj.Marry();
		obj.ParentProperty();
	}
}
