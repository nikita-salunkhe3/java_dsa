class Parent{
	String fun(){
		System.out.println("String");
		return "Shashi";
	}
}
class Child extends Parent{
	StringBuffer fun(){
		System.out.println("StringBuffer");
		return new StringBuffer("shashi");
	}
}
class Client{
	public static void main(String[] args){

		Parent p=new Child();
		p.fun();
	}
}
		
