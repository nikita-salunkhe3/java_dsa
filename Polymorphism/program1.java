/*
 * Polymorphism
 *
 * jer eka class madhe same name but method ch behviour disfferent assel ter tyala polymorphism
 * ass mentayt
 * (1 name ,Many Forms)
 *
 * polymorphism 2 type
 * 1.overloading --->>> jer ekach class Madhe polymorphism assel ter overloading mentayt
 *                      overloading cha secnario madhe method cha return type consider kela jat nahi
 *
 * 2.overridding --->>> jer 2 different class madhe polymorphism occurs hot asel tyatla overridding
 *                      ass mentyat
 *                      overridding cha secnario madhe method cha return type cha sambandh ahe
 */
class Demo{
	int fun(int x){

	}
	float fun(int x){

	}
}
/*
 *  error: method fun(int) is already defined in class Demo
	float fun(int x){
	  	^

   Explanation:
   jeri apan tya class cha object nahi benvla main method nahi lihil , call nahi kela method la teri
   error yete ass ka?
   karen compiler swtahun method sathicha method table create kerto tyamadhe pretek methods cha
   method signature asstat jeri ek teri method signature double zali ter to error deto
   compiler adhich bind kerun thevto ki konala kothun call keraycha ahe te

   Method signature madhe 2 gosti involved asstat:
   1. Method cha nav
   2. Method la dilelya parameter cha datatype

   compiler la method signature create karat asstana kahi garaj lagat nahi ki method ne ky ky return 
   kel ahe tyacha sambandh fkt 2 gosti sathi cha assto
 */
