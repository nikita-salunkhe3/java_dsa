class Demo{
	private String name="Virat";

	Demo(String str){

		System.out.println("Instance String Virat:"+System.identityHashCode(name));//0x100
	
		name="Rohit";
		System.out.println("Instance String Rohit:"+System.identityHashCode(name));//0x200
	
		name="Virat";
		System.out.println("Instance String Virat:"+System.identityHashCode(name));//0x100

		System.out.println("Local String :"+System.identityHashCode(str));//0x100
	}
}
class Client{
	public static void main(String[] args){

		Demo obj=new Demo("Virat");
		System.out.println("In Main");
		System.out.println("Virat = "+System.identityHashCode("Virat"));//0x100
	}
}
