//hidden this reference


class Demo{
	int x=10;

	Demo(){
		System.out.println("In No-args constructor");
	}
	Demo(int x){
		System.out.println("In para constructor");
	}

	public static void main(String[] args){
		Demo obj=new Demo();
	}
}
/*
 * output:
 * In No-args constructor
 */

