class Employee{
	int empId=10;
	String name="Kanha";

	static int y=50;

	void EmpInfo(){
		System.out.println("Id is = "+empId);
		System.out.println("name is = "+name);
		System.out.println("y is = "+y);
	}
}
class MainDemo{
	public static void main(String[] args){

		Employee emp1=new Employee();
		Employee emp2=new Employee();

		emp1.EmpInfo();
		emp2.EmpInfo();

		System.out.println("------------------------------");

		emp2.empId=20;
		emp2.name="Sarthak";
		emp2.y=5000000;

		emp1.EmpInfo();
		emp2.EmpInfo();

		System.out.println(emp1);
		System.out.println(emp2);

		System.out.println(System.identityHashCode(emp1));
		System.out.println(System.identityHashCode(emp2));

		System.out.println(hashCode(emp1));
		System.out.println(hashCode(emp2));
	}
}


