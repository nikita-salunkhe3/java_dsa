class Demo{
	int x=10;
	static int y=20;

	{
		System.out.println("In Instance block1");
	}
	static{
		System.out.println("In Static block1");
	}
}
class Client{
	public static void main(String[] args){

		int x=10;

		Demo obj=new Demo();
	
		{
			
	        	System.out.println("In Instance block2");
        	}
	}

	static{
		System.out.println("In Static block2");
	}
}
/*
 * output:
 * In static Block2
 * In static Block1
 * In Instance Block1
 *In Instance Bolck2
 */
