class Employee{
	int empId=10;
	String str="Kanha";

	void empInfo(){
		System.out.println("Id is= "+empId);
		System.out.println("name is= "+str);
	}
}
class MainDemo{
	public static void main(String[] args){

		Employee emp=new Employee();
		emp.empInfo();

		System.out.println(emp.empId);
		System.out.println(emp.str);
	}
}

/*
 * output->
 * Id is= 10
 * name is=Kanha
 * 10 
 * Kanha
 */


