class Demo{

	Demo(){
		System.out.println("In constructor");
	}
	void fun(){
		System.out.println("In fun");
	}
	void gun(){
		this();
		System.out.println("In gun");
	}

	public static void main(String[] args){
		Demo obj=new Demo();
		obj.gun();
	}
}
