class Demo{

	int x=10;
	Demo(){
		System.out.println("In constructor1");
	}
	Demo(){
		System.out.println("In constructor2");
	}
}
/*
 * compile time error :
 * Constructor Demo() is already defined in class Demo
 */

