//combination of static and Instatnce in parent class

class Parent{
	int x=10;
	static int y=20;

	static{
		System.out.println("Parent static block");
	}
	Parent(){
		System.out.println("In Parent constructor");
	}
	void MethodOne(){
		System.out.println(x);
		System.out.println(y);
	}
	static void MethodTwo(){
		System.out.println(y);
	}
}
class Child extends Parent{
	static{
		System.out.println("In child static block");
	}
	Child(){
		System.out.println("In child Constructor");
	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();
		obj.MethodOne();
		obj.MethodTwo();
	}
}
/*
 * output:
 * Parent static Block 
 * Child static Block
 * In Parent Constructor
 * In child Constructor
 * 10
 * 20
 * 20
 */

