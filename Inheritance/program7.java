class Parent{
	static{
		System.out.println("In Parent Static block");
	}
}
class Child extends Parent{
	static{
		System.out.println("In child static block");
	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();
	}
}
/*
 * output:
 * In Parent Static block
 * In child Static block
 */
