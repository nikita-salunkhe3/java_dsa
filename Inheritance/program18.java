/*
 * jer child class ani parent class ya doni class madhe variable same asstil ani jer apan sop(x) kel
 * ter aplyala child class madhil x chi value display hoil pn mala parent class madhil x chi value
 * print keraychi assel ter me super ha keyword used kerel
 * i.e. (super.x)  yamule parent class madhil static instance variable mala child class madhe access
 * hotil
 *
 * Parent class ha Super shi related ahe
 *
 */

class Parent{
	int x=100;
	static int y=200;

	Parent(){
		System.out.println("In Parent Constructor");
	}
}
class Child extends Parent{
	int x=10;
	static int y=20;

	Child(){
		System.out.println("In child Constructor");
	}
	void access(){
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();
		obj.access();
	}
} 
/*
 * output:
 *
 *In Parent constructor
 In child Constructor
 100
 200
 10
 20
 */
