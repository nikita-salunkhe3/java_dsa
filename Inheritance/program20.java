//Multi-level Inheritance

class Parent{
	Parent(){
		System.out.println("In Parent constructor");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In Child Constructor");
	}
}
class Client{
	public static void main(String[] args){

		System.out.println("In main");
		Child obj=new Child();
	}
}
/*
 * 	INTERVIEW IMP QUESTION
 * ya secnario madhe child class cha parent Parent class ahe pn Parent class cha Parent object class ahe
 * pn aplyala mahit ahe ki object class serv class cha parent ahe Except:Object class Swata
 * pn actual madhe child class ha Indirectly Object class cha parent class ahe 
 * ani parent class ha object class cha direct parent class ahe 
 */


