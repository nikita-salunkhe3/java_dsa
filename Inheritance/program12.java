//Realtime example for Inheritance

class Defence{
	Defence(){
		System.out.println("Defence Rules");
	}
	void Purpose(){
		System.out.println("Protect India");
	}
}
class Navy extends Defence{
	Navy(){
		System.out.println("Navy Rules");
	}
}
class Army extends Defence{
	Army(){
		System.out.println("Army Rules");
	}
}
class AirForce extends Defence{
	AirForce(){
		System.out.println("AirForce Rules");
	}
}
class Main{
	public static void main(String[] args){
		Army obj=new Army();
	}
}



