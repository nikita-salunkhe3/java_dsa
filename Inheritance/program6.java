//Instance in Parent Class

class Parent{
	int x=10;
	Parent(){
		System.out.println("In parent Constructor");
	}
	void Access(){
		System.out.println("In Parent Instance");
	}
}
class Child extends Parent{
	int y=20;
	Child(){
		System.out.println("In Child Constructor");
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();
		obj.Access();
	}
}
/*
 * output:
 *In Parent Constructor
 In Child Constructor
 10
 20
 In Parent Instance
 */

