class Parent{
	Parent(){
		System.out.println("Parent Constructor");
	}
	void ParentProperty(){
		System.out.println("Flat gold Car");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child Constructor");
	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();
		obj.ParentProperty();
	}
}
/*
 * output:
 * Parent Constructor
 * Child Constructor
 * Flat gold Car
 */
