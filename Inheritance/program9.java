class Parent{
	int x=10;
	static int y=20;

	static{
		System.out.println("Parent static block");
	}
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void MethodOne(){
		System.out.println(x);
		System.out.println(y);
	}
	static void MethodTwo(){
		System.out.println(y);
	}
}
class Child extends Parent{
	static {
		System.out.println("In Child static block");
	}
	Child(){
		System.out.println("In Child Constructor");
	}
}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
		obj.MethodOne();
		obj.MethodTwo();
	}
}

/*
 * output:
 *Parent static block 
In Child static block
In Parent Constructor
In child Constructor
10 
20
20
*/
