class Parent{
	int x=10;
	static int y=20;

	Parent(){
		System.out.println("Parent");
	}
}
class Child extends Parent{
	int x=100;
	static int y=200;

	Child(){
		System.out.println("Child");
	}
	void Access(){
		System.out.println(super.x);//10
		System.out.println(super.x);//20
		System.out.println(this.x);//100
		System.out.println(this.y);//200
		System.out.println(x);//100
		System.out.println(y);//200
	}
}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
		obj.Access();
	}
}
