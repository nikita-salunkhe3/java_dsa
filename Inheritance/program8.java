class Parent{
	static int x=10;
	static{
		System.out.println("In Parent static block");
	}
	static void Access(){
		System.out.println(x);
	}
}
class Child extends Parent{
	static{
		System.out.println("In Child Static block");
		System.out.println(x);
		Access();
	}
}
class Client{
	public static void main(String[] args){

		System.out.println("In Main");
		Child obj=new Child();
	}
}
/*
 * output:
 * In main
 * In Parent static block
 * In child Static block
 * 10
 * 10
 *
 * Apan jer static block client class cha aat lihila assta ter main() method cha adhi static block
 * exceute zala assta pn ya secnario madhe apan parent child class la tevach call jail jeva apan object 
 * tayar karu
 * jeva Child() constuctor la implicitly cl jail teva tyala extend disto to mento parent class madhe 
 * static gosti ahet ka jer asstil ter parent class madhil static block execute hoyla jatil 
 * parent class madhil variable adddhi initialized hotil ani nanter mg child class madhil static block
 * ani static variable intialized hoil 
 */ 

