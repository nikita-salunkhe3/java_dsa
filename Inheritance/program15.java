//static in parent Class

class Parent{
	static {
		System.out.println("In Parent static1");
	}
	Parent(){
		System.out.println("In Parent Constructor");
	}
	static {
		System.out.println("In Parent static2");
	}
	{
		System.out.println("In parent Instance");
	}
}
class Child extends Parent{
	static{
		System.out.println("In child static1");
	}
	Child(){
		System.out.println("In Child Constructor");
	}
	static {
		System.out.println("In child static2");
	}
	{
		System.out.println("In Child Instance");
	}
}
class Client{
	public static void main(String[] args){
		Child obj=new Child();
	}
}
/*
 * output:
 * In parent static1 
 * In parent static2
 * In Child static1  
 * In Child static2
 * In Parent Instance 
 * In Parent Constructor
 * In child Instance
 * In child Constructor
 */ 




