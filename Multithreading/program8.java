class Parent{
	Parent(){
		System.out.println("In Parent constructor");
	}
}
class Child extends Parent implements Runnable{

	static void fun(){
		System.out.println("fun:"+Thread.currentThread().getName());
		System.out.println("In fun");
	}
	public void run(){
		
		System.out.println("run: "+Thread.currentThread().getName());
		System.out.println("In run");
		fun();
	}

	public static void main(String[] args){

		System.out.println("Child: "+Thread.currentThread().getName());
		
		Child obj=new Child();
		Thread t=new Thread(obj);
		t.start();

		fun();

	}
}
/*
 * Child: main
In Parent constructor
fun:main
In fun
run: Thread-0
In run
fun:Thread-0
In fun
*/
