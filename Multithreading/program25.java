class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println("In run");
	}
}
class ThreadGroupDemo{
	public static void main(String[] args){

		ThreadGroup tg=new ThreadGroup("Core2web");

		MyThread obj=new MyThread(tg,"Core2web");
		System.out.println(obj.getThreadGroup());
		System.out.println(obj.getName());

		MyThread obj1=new MyThread(tg,"Core2web");
		System.out.println(obj1.getThreadGroup());
		System.out.println(obj1.getName());
		
		MyThread obj2=new MyThread(tg,"Core2web");
		System.out.println(obj2.getThreadGroup());
		System.out.println(obj2.getName());

		System.out.println(obj2.getThreadGroup());
		System.out.println(obj2.getThreadGroup());
		
		obj.start();
		obj1.start();
		obj2.start();
	}
}
