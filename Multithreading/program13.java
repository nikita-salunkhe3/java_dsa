class MyThread implements Runnable{
	public void run(){

		System.out.println("MyThread: "+Thread.currentThread().getName());
	}
}
class ThreadDemo{
	public static void main(String[] args){

		MyThread obj=new MyThread();

		Thread obj1=new Thread(obj);//Thread(Runnable) constructor is call

		System.out.println("ThreadDemo:"+Thread.currentThread().getName());

		System.out.println(obj);//
		System.out.println(obj1);//

		obj1.start();
	}
}

