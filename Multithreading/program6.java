//Runnable interface jast preferred ahe Thread class la extends kernya peksha 
//karen Runnable interface Extra chi power deto 

/******Multithreading using Runnable interface********/

class MyThread implements Runnable{
	public void run(){
		System.out.println("In run");
		System.out.println("MyThread:-"+Thread.currentThread().getName());
	}
}
class ThreadDemo{
	public static void main(String[] args){

		MyThread obj=new MyThread();

		System.out.println(obj);//MyThread@6ff3c5b5

		Thread Tobj=new Thread();
		System.out.println(Tobj);//Thread[Thread-0,5,main]

		Thread t=Thread.currentThread();
		System.out.println(t);//Thread[main,5,main]
	//	t.start();//IllegalThreadStateException
	}
}
/*
 * output:
 * MyThread@6ff3c5b5
Thread[Thread-0,5,main]
Thread[main,5,main]
*/
