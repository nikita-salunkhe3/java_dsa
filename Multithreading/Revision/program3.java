//Creating Thread by implementing Runnable interface

class MyThread implements Runnable{
	public void run(){
		System.out.println("In run");
	}
}
class ThreadDemo{
	public static void main(String[] args){

		System.out.println("Start main");
		MyThread obj=new MyThread();
		Thread t=new Thread(obj);

		t.start();
		System.out.println("End main");
	}
}
