class MyThread implements Runnable{
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());
	}
}
class ThreadDemo{
	public static void main(String[] args){

		System.out.println("Start main");

		MyThread obj = new MyThread();

		Thread t1 = new Thread(obj);
		System.out.println(t1.getName());
		t1.setName("Nikita");

		t1.start();

		MyThread obj2 = new MyThread();
		Thread t2 = new Thread(obj2);

		System.out.println(t2.getName());
		t2.setName("Shweta");

		t2.start();
		System.out.println("End main");
	}
}
