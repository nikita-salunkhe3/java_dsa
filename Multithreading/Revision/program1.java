//ThreadPool
//ThreadPool create kernyasathi exceute(Runnable) ya method la call kerava lagto
//ya method mule thread tyaar houn start() la sudha call jato
//exceute method cha paramter ha runnable ahe so tyamule apan Runnable la compulsory implements keravch
//lagnar ahe

import java.util.concurrent.*;
class MyThread implements Runnable{
	int num=0;
	MyThread(int num){
		this.num=num;;
	}
	public void run(){
		System.out.println(Thread.currentThread()+" Thread start ");
		Delay();
		System.out.println(Thread.currentThread()+" Thread end ");
	}
	void Delay(){
		try{
			Thread.sleep(4000);
		}catch(InterruptedException ie){

			System.out.println(ie.toString());
		}
	}
}
class ThreadPoolDemo{
	public static void main(String[] args){

		ExecutorService ser=Executors.newCachedThreadPool();

		for(int i=0;i<4;i++){
			MyThread obj=new MyThread(i);
			ser.execute(obj);
		}
		System.out.println("**********End main*************");
	}
}


