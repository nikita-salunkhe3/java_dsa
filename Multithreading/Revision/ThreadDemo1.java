//Creation of Thread by using Thread class

class MyThread extends Thread{
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());
	}
}
class ThreadDemo{
	public static void main(String[] args){

		System.out.println("In main");
		System.out.println("Main Thread : "+Thread.currentThread().getName());

		MyThread t1 = new MyThread();
		System.out.println(t1.getName());
		t1.setName("Nikita");

		t1.start();

		MyThread t2 = new MyThread();
		System.out.println(t2.getName());
		t2.setName("Shweta");

		t2.start();

		System.out.println("End main");
	}
}
