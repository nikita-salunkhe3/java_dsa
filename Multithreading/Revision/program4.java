//sleep() ==>> Throws InterruptedException

class MyThread implements Runnable{
	public void run(){
		System.out.println("In run");
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
	
			System.out.println(ie.toString());
		}
		System.out.println("*********");
	}
}
class ThreadDemo{
	public static void main(String[] args){

		MyThread obj=new MyThread();
		Thread t=new Thread(obj);
		t.start();
	}
}
