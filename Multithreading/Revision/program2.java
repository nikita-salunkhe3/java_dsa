// Creating Thread by using Thread class
/*
 * public static final int MIN_PRIORITY;
  public static final int NORM_PRIORITY;
  public static final int MAX_PRIORITY;
*/

class MyThread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread().getName()+" = "+Thread.currentThread());
		System.out.println("Min Priority: "+Thread.MIN_PRIORITY);
		System.out.println("Norm Priority: "+Thread.NORM_PRIORITY);
		System.out.println("Max Priority: "+Thread.MAX_PRIORITY); 
	}
}
class ThreadDemo{
	public static void main(String[] args){

		MyThread obj=new MyThread();
		obj.start();

		System.out.println("Min Priority: "+Thread.MIN_PRIORITY);
		System.out.println("Norm Priority: "+Thread.NORM_PRIORITY);
		System.out.println("Max Priority: "+Thread.MAX_PRIORITY); 
	}
}
