class MyThread implements Runnable{
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread());
	}
}
class Client{
	public static void main(String[] args){

		ThreadGroup tg=new ThreadGroup("India");

		MyThread obj1=new MyThread();
		Thread t1=new Thread(tg,obj1,"MH");

		MyThread obj2=new MyThread();
		Thread t2 = new Thread(tg,obj2,"Goa");

		System.out.println("Active Count : "+tg.activeCount());//0
		System.out.println("Active Group Count : "+tg.activeGroupCount());//0
	
		t1.start();
		t2.start();

		tg.interrupt();

		System.out.println("Active Count : "+tg.activeCount());//2
		System.out.println("Active Group Count : "+tg.activeGroupCount());//0
	}
}

