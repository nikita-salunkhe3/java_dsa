class Parent implements Runnable{
	public void run(){
		System.out.println("In run");
		System.out.println("run:"+Thread.currentThread().getName());
	}
}
class Child extends Parent {
	public static void main(String[] args){

		System.out.println("In main");
		Child obj=new Child();
		Thread obj1=new Thread(obj);
		System.out.println("main:"+Thread.currentThread().getName());
		obj1.start();
	}
}
