class Parent extends Thread{

	public void run(){
		System.out.println(Thread.currentThread().getName());
		System.out.println("In parent run");
	}
	Parent(){
		System.out.println("In Parent constructor");
	}
}
class Child extends Parent implements Runnable{

	Child(){
		System.out.println("In Child Constructor");
	}

	public void run(){
		System.out.println(Thread.currentThread().getName());
		System.out.println("In run");		

	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();
		System.out.println(Thread.currentThread().getName());
		obj.start();
	}
}

		

	
