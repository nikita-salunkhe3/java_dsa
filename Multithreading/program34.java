import java.util.concurrent.*;
class MyThread implements Runnable{
	int num=0;

	MyThread(int num){
		this.num=num;
	}
	public void run(){

		System.out.println(Thread.currentThread()+ " num: "+num);
		try{
			Thread.sleep(4000);
		}catch(InterruptedException ie){

		}
	}
}
class ThreadPoolDemo{
	public static void main(String[] args){
		System.out.println("Start main");
		
		ExecutorService ser=Executors.newCatchThreadPool();

		for(int i=0;i<10;i++){
			System.out.println("i :"+i);
			MyThread obj=new MyThread(i);
			ser.execute(obj);
		}
		System.out.println("End main");
	}
}
