class MyThread extends Thread{
	
	public void run(){
		System.out.println("In run");
		System.out.println("In Mythread : " +Thread.currentThread().getName());
	}
}
class Client{
	public static void main(String[] args){

		MyThread obj=new MyThread();

		obj.start();

		obj.setPriority(7);
//		obj.setPriority(11);//IllegalArgumentException

		obj.start();//IllegalThreadStateException

		System.out.println("In main : " +Thread.currentThread().getName());//
	}
}
