class Demo{
	void fun(){

		System.out.println(Thread.currentThread().getName());
		System.out.println("In fun");
	}
}
class TDemo extends Demo implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread().getName());
		fun();
	}
}
class ThreadDemo{
	public static void main(String[] args){

		System.out.println("In main: "+Thread.currentThread().getName());

		TDemo obj=new TDemo();
		Thread T=new Thread(obj);
		T.start();
	}
}

/*
 * output:
 * In main: main
 * thread-0
 * thread-0
 * In fun
 */
