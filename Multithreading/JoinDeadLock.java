  
class MyThread implements Runnable{
	static Thread tmain = null;

	public void run(){
		System.out.println("In run : "+Thread.currentThread().getName());

		try{
			tmain.join();
		}catch(Exception ex){
			System.out.println("Handled");
		}
		for(int i=0;i<10;i++){
			System.out.println("In Mythread");
		}
	}
}
class Client extends Thread{
	public static void main(String[] args)throws InterruptedException{

		System.out.println("In main");

		MyThread.tmain = Thread.currentThread();

		MyThread obj=new MyThread();

		Thread t=new Thread(obj);
		System.out.println("Thread started");
		t.start();
			
		t.join();

		for(int i=0;i<10;i++){
			System.out.println("In main");
		}
	}
}


