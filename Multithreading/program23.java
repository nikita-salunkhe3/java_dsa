//MyThread class cha jer apan Parameterized constructor lihila ter aplyala Compiler mento ki 
//no-args constructor sudha tuch lihi


class MyThread extends Thread{
	MyThread(String str){
		super(str);
	}
	public void run(){

		System.out.println("In run:"+getName());
	}
}
class ThreadDemo {
	public static void main(String[] args){

		MyThread obj1=new MyThread("XYZ");
		obj1.start();

		MyThread obj2=new MyThread("PQR");
		obj2.start();

		MyThread obj3=new MyThread();
		obj3.start();
	}
}
/*
 * output:
 * error: constructor MyThread in class MyThread cannot be applied to given types;
		MyThread obj3=new MyThread();
		              ^
  required: String
  found:    no arguments
  reason: actual and formal argument lists differ in length
1 error
*/
