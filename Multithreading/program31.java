//ThreadGroup class methods are:
//1.activeCount()
//2.activeThreadCount()

/**********by extending Thread class***********/
class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(40000);
		}catch(InterruptedException ie){

		}
	}
}
class ThreadGroupDemo{
	public static void main(String[] args){
		ThreadGroup tg=new ThreadGroup("India");

		MyThread obj1=new MyThread(tg,"Maha");
		MyThread obj2=new MyThread(tg,"Goa");

		obj1.start();
		obj2.start();

		ThreadGroup ctg1 = new ThreadGroup("Pakistan");

		MyThread obj3=new MyThread(ctg1,"Karachi");
		MyThread obj4=new MyThread(ctg1,"Lahore");

		obj3.start();
		obj4.start();
	
		ThreadGroup ctg2 = new ThreadGroup("Bangladesh");

		MyThread obj5=new MyThread(ctg1,"Dhaka");
		MyThread obj6=new MyThread(ctg1,"mirpur");

		obj5.start();
		obj6.start();
	}
}

