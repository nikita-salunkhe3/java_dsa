class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadDemo{
	public static void main(String[] args){

		ThreadGroup PTG = new ThreadGroup("HighSchool");

		MyThread T1=new MyThread(PTG,"Commerce");
		MyThread T2=new MyThread(PTG,"Science");
		MyThread T3=new MyThread(PTG,"Arts");

		T1.start();
		T2.start();
		T3.start();

		System.out.println("**"+T1);
		System.out.println("**"+T2);
		System.out.println("**"+T3);
	}
}
