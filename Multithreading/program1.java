//MultiThreading he performance vadvnyasathi used kela jato
//Multithreading madhe threads he Synchronized OR Asyanchronized work kertat
//Multithreading madhe Thread la Priority nusar Schedule kernyasathi Thread schedular assto
//MultiThreading used to execute code Fastly
//Multhreading apan 2 method ne karu shakato 1.Thread class 2.Runnable Interface
//***Marker Interface madhe ekhi method naste he asshe Interface ahet ki JVM yana special Functionality provide kerto
//****Thread class cha Parent ha Runnable interface ahe tasech Thread class cha Parent Object class sudha ahe======""Thread class cha Parent Runnable interface ahe ass ka menaych karan --->> Thread class ha Runnable class la implement kertoy yamule Runnable interface madhil serv methods ya Thread class madhe yatat tyamule concept nusar baghitl ter Parent cha serv gosti Child kade yetat menun ya thikani Thread class cha Parent Runnable interface ass menl ahe"""******
//unit time madhe kiti number of Process RAM varun run houn gelya yaver CPU / OS cha performance thervla jato jevdya jast Process RAM varun run houn jatil unit time madhe tevda CPU cha Performance changla ahe ass thervl jat
//Process schedule kernyasathi OS CPU schedular chi madat gheto
/*                   
                      Runnable(No Parent)
		         |
	               Thread(2-Parent : Runnable,Object)
		         |
		       Demo(1-Parent : Thread)
*/


//Creating thread by using Thread class

/*
 * run method he Thread class madhe ahe "public void run();"
 * apan Thread class extend kertoy MyThread class madhe menjech Thread class madhil serv method Child class menjech MyThread class madhe inherit honar tyatil me run() he method Override keli 
 * ya madhe jer apan public void run() method asshi n lihita void run() yevdch lihil ter error yete 
 * **attempting to assign weaker access privilege was public 
 * karen Overriding cha rule nusar Parent class cha scope jevda ahe tevdach or tya peksha jast scope child class cha pahije karen apan jer child class madhil method cha scope kami kela ter error yete yamule ti error yete
 * run() method shivay apan new Thread tayar karu shakat nahi
 */
class MyThread extends Thread{
	public void run(){
		System.out.println("Child thread name = "+Thread.currentThread().getName());
		for(int i=0;i<10;i++){
			System.out.println("In run");
		}
	}
}
class ThreadDemo{
	public static void main(String[] args){

		System.out.println("main thread name = "+Thread.currentThread().getName());

		MyThread obj=new MyThread();
		obj.start();

		for(int i=0;i<10;i++){
			System.out.println("In main");
		}
	}
}
//As per the Priority of Thread Thread schedular can schedule the Thread and output will be Displayed.
//In this code two threads are present 
//1.main thread 
//2.thread-0
