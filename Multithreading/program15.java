//Concurrency method in Thread Class
//3 methods are included:
//1.sleep()
//2.join()....Most imp
//3.yeild()

class MyThread extends Thread{
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread());
	}
}
class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{

		System.out.println(Thread.currentThread());

		MyThread obj=new MyThread();
		obj.start();
		Thread.sleep(1000);
		System.out.println(Thread.currentThread());
	}
}


