//yield() method

class MyThread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread().getName());//Thread-0
		System.out.println(getName());//Thread-0
	}
}
class ThreadDemo{
	public static void main(String[] args){

		MyThread obj=new MyThread();
		obj.start();

		obj.yield();
		System.out.println("Main= "+Thread.currentThread().getName());//main
		System.out.println("Main= "+obj.getName());//Thread-0
	}
}

