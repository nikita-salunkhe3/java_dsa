class MyThread extends Thread{

	String str=null;
	MyThread(String str){
		super(str);
//		this.str=str;
	}

	public void run(){
		System.out.println("In run");
		System.out.println("MyThread: "+Thread.currentThread());
	}
}
class Client{
	public static void main(String[] args)throws InterruptedException{

		MyThread obj=new MyThread("nikita");

		System.out.println(obj.getName());

		System.out.println("Main : "+Thread.currentThread());

		obj.start();

		Thread.sleep(1000);

		System.out.println("end main");
	}
}
