class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println("In run");
	}
}
class ThreadGroupDemo{
	public static void main(String[] args){

		ThreadGroup tg=new ThreadGroup("Core2web");

		MyThread obj=new MyThread(tg,"C");
		
		obj.start();

		System.out.println(obj.getThreadGroup());
		System.out.println(obj.getThreadGroup());
		System.out.println(obj.getThreadGroup());
	}
}
