//2 Ways for changing thread name

/****By Calling setName() method******/
/*
class MyThread extends Thread{
	public void run(){
		System.out.println("New Thread: "+getName());
	}
}
class ThreadDemo{
	public static void main(String[] args){

		System.out.println("In main Thread: "+Thread.currentThread().getName());
		MyThread obj=new MyThread();
		obj.setName("Nikita");
		obj.start();
	}
}
*/
/*****While creating Thread give ThreadName in Constructor*******/

class MyThread extends Thread{
	MyThread(String str){
		super(str);
	}
	public void run(){
		System.out.println("New Thread: "+getName());
	}
}
class ThreadDemo{
	public static void main(String[] args){

		MyThread obj=new MyThread("Core2web");
		obj.start();
		System.out.println("In main: "+Thread.currentThread().getName());
	}
}

