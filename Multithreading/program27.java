
class FrameWork extends Thread{

	FrameWork(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class FrontEnd{
	public static void main(String[] args){

		ThreadGroup tgp = new ThreadGroup("PreRequisite TOOl");

		FrameWork t1=new FrameWork(tgp,"Servlet");
		FrameWork t2=new FrameWork(tgp,"JSP");
		FrameWork t3=new FrameWork(tgp,"Hibernate");

		t1.start();
		t2.start();
		t3.start();

		ThreadGroup tgc = new ThreadGroup(tgp,"SpringBoot");

		FrameWork t4=new FrameWork(tgc,"Spring Core");
		FrameWork t5=new FrameWork(tgc,"Spring Data");
		FrameWork t6=new FrameWork(tgc,"Spring Security");

		t4.start();
		t5.start();
		t6.start();
	}
}

