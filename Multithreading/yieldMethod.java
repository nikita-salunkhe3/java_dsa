class MyThread extends Thread{
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());
	}
}
class Client{
	public static void main(String[] args){

		System.out.println("In main");

		MyThread obj=new MyThread();

		obj.start();

		obj.setPriority(8);

		obj.setName("nikita");

		System.out.println(obj.getName());

		obj.yield();

		MyThread obj1=new MyThread();

		obj1.start();

		obj1.setPriority(3);

		obj1.yield();
	}
}
