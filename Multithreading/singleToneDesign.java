class SingleTonePattern{

	static SingleTonePattern obj=new SingleTonePattern();

	SingleTonePattern(){
		System.out.println("In constructor");
	}
/*	static SingleTonePattern getObject(){
		return obj;
	}*/
	static int getObject(){
		return 10;
	}
}
class Client{
	public static void main(String[] args){
/*
		SingleTonePattern obj1 = SingleTonePattern.getObject();

		System.out.println(obj1);
		
		SingleTonePattern obj2 = SingleTonePattern.getObject();

		System.out.println(obj2);
	
		SingleTonePattern obj3 = SingleTonePattern.getObject();

		System.out.println(obj3);*/

		int z = SingleTonePattern.getObject();
		System.out.println(z);
		int x = SingleTonePattern.getObject();
		int y = SingleTonePattern.getObject();
	}
}
