//join(long,int)===>>>join(miliseconds,nanoseconds)
//join(long)=====>>join(miliseconds)

class MyThread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
		for(int i=0;i<10;i++){
			System.out.println("In run");
		}
	}
}
class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{

		MyThread obj=new MyThread();
		obj.start();

		System.out.println("In Main Thread");
		
		obj.join(100,2233888);//throws InterruptedException
		for(int i=0;i<10;i++){
			System.out.println("In main");
		}
	}
}
