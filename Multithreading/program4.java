
class First extends Thread{
	public void run(){
		System.out.println("runFirst= "+Thread.currentThread().getName());
	}
}	
class Demo extends Thread{
	public void run(){
		System.out.println("runDemo= "+Thread.currentThread().getName());
	}
}
class ThreadDemo{
	public static void main(String[] args){

		System.out.println("Main= "+Thread.currentThread().getName());
	
		Demo obj1=new Demo();
		obj1.start();

		Demo obj2=new Demo();
		obj2.start();

	}
}

