//DeadLock Concept (pahile aap pahile app)
//MyThread ne join() la cl kela ahe ani ThreadDemo sudha join() ya method la cl kela ahe
//te mentyat adhi tu cpu ghe dusra mentoy addhi tu cpu ghe tyamule pahile aap pahile aap chi concept
//picture madhe ali ahe

class MyThread extends Thread{
	static Thread nmMain=null;

	public void run(){
		System.out.println("In run");

		try{
			nmMain.join();
		}catch(InterruptedException ie){
			System.out.println("Exception Occurs");	
		}
		for(int i=0;i<10;i++){
			System.out.println("run");
		}
	}
}
class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{

		System.out.println("In main");

		MyThread.nmMain=Thread.currentThread();
		MyThread obj=new MyThread();
		obj.start();

		obj.join();//it throws Interrupted Exception
		for(int i=0;i<10;i++){
			System.out.println("main");
		}
	}
}


