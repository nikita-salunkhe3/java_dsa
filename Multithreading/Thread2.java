class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
}
class Child extends Parent implements Runnable{

	Child(){
		System.out.println("In Child Constructor");
	}
	public void run(){
		System.out.println("In run");
		System.out.println("In child : "+Thread.currentThread());
	}
	public static void main(String[] args){

		System.out.println("In main");
		Child obj=new Child();

		Thread t=new Thread(obj);

		t.start();

		System.out.println("Runnable : "+obj);
		System.out.println("Thread : "+t);

		System.out.println("Main : "+Thread.currentThread());

		System.out.println("End main");
	}
}

