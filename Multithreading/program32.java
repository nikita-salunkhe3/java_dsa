class MyThread implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(400);
		}catch(InterruptedException ie){

		}
	}
}
class ThreadGroupDemo{
	public static void main(String[] args){

		ThreadGroup tg=new ThreadGroup("India");
		MyThread obj1=new MyThread();
		Thread t1=new Thread(tg,obj1,"Maha");

		t1.start();

		MyThread obj2=new MyThread();
		Thread t2=new Thread(tg,obj2,"dhule");
		t2.start();

		MyThread obj3=new MyThread();
		Thread t3=new Thread(tg,obj3,"Goa");
		t3.start();

		tg.interrupt();
		
		ThreadGroup ctg=new ThreadGroup("Pakistan");
		MyThread obj4=new MyThread();
		Thread t4=new Thread(ctg,obj4,"karachi");
		t4.start();

		MyThread obj5=new MyThread();
		Thread t5=new Thread(ctg,obj5,"Lahore");
		t5.start();

		ctg.interrupt();

		System.out.println(ctg.activeCount());
		System.out.println(ctg.activeGroupCount());
	}
}
