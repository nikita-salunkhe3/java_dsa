//Lang package madhe thread class ahe
class MyThread extends Thread{
	public void run(){
		System.out.println("In Run Method");
		System.out.println(Thread.currentThread());  //Thread[Thread0,5,main]
		System.out.println(Thread.currentThread().getName());  //Thread0
		System.out.println(Thread.currentThread().getPriority());//5
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ex){
			System.out.println("Exception Handle");
		}
	}
}
class Client{
	public static void main(String[] args){
		
		System.out.println("In main");

		System.out.println(Thread.currentThread());  //Thread[main,5,main]
		System.out.println(Thread.currentThread().getName());  //main
		//Thread[Thread_name,thread_Priority,Thread_GroupName]

		System.out.println(Thread.currentThread().getPriority());//5

		MyThread obj=new MyThread();

		obj.run();

		System.out.println("End main");
	}
}
/*
 * sleep()==>InterruptedException  (CheckedException)  must be caught or declare to be thrown
 */


