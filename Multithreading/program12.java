class MyThread extends Thread{
	public void run(){
		Thread t=new Thread();
		System.out.println(t.getPriority());
	}
}
class ThreadDemo{
	public static void main(String[] args){

		Thread t=Thread.currentThread();
		System.out.println(t.getPriority());

		MyThread obj=new MyThread();//Thread-0
		obj.start();

		t.setPriority(7);

		MyThread obj2=new MyThread();//Thread-1
		obj2.start();
	}
}
/*
/*
 * output:
 * 5
 * 5
 * 7
 * OR 
 * 5
 * 7
 * 5
 */
