//sleep() he method static ahe ani hi interruptedException throws kerte
//Thread class madhil run() hi method kontch exception throw nahi karat pn apan hich run() method child class madhe override kerat astana run() madhe jer sleep() method lihili ter sleep() hi method interruptedException throw kerte ani jer apan child class madhun exception throw kel ter error yete karen
//Overriding cha rule nusar Parent madhil method override karat astana ruturn type same assla pahije kiva class retrun hot asel ter co-varient relation pahije ahe 
//tasech function ch name ani parameter same asle pahjet tasech
//Parent chi method jer konte hi Exception throw karat nasel ter child class madhil method la sudha exception throw kerta yet nahi

class MyThread extends Thread{
       public void run()throws InterruptedException{

	       for(int i=0;i<10;i++){
		       System.out.println(i);
		       Thread.sleep(1000);
	       }
       }

}
class ThreadDemo{
	public static void main(String[] args){

		MyThread obj=new MyThread();

		obj.start();
	}
}
/*
 * output:
 *  run() in MyThread cannot implement run() in Runnable
       public void run()throws InterruptedException{
                   ^
  overridden method does not throw InterruptedException
1 error
*/
