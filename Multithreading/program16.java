//join() method
//kamaverchi dependency sangte 
//join() realTime example- schedule(TimeTable)-> Tickets ->MatchDay
//if one thread is switches to another Thread then it is called as Context switching
//obj.join()-->> kontya thread la priority deun tich kam zalyashivay swata execute hot nahiye he obj verun therat


class MyThread extends Thread{
	public void run(){
		for(int i=0;i<10;i++){
			System.out.println("In run");
		}
	}
}
class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{

		MyThread obj=new MyThread();
		obj.start();

		obj.join();//exception throw kerte-->>>interruptedException
		System.out.println("End main");
	}
}

