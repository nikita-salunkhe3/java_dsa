import java.util.concurrent.*;
class MyThread extends Thread{
	int num=0;
	MyThread(int num){
		this.num=num;
	}
	public void run(){
		System.out.println("In run");
	}
}
class Client{
	public static void main(String[] args){

		ExecutorService es=Executors.newFixedThreadPool(5);

		System.out.println(es);

		for(int i=1;i<=6;i++){
			MyThread obj=new MyThread(i);
			es.execute(obj);
		}
		es.shutdown();
	}
}
