class MyThread extends Thread{
	MyThread(){
		
	}
	MyThread(String str){
		super(str);
	}
	public void run(){
		System.out.println("In run: "+Thread.currentThread().getName());
	}
}
class ThreadDemo{
	public static void main(String[] args){

		/*
		MyThread obj1=new MyThread("xyz");//xyz
		obj1.start();

		MyThread obj2=new MyThread("pqr");//pqr
		obj2.start();
		
		MyThread obj3=new MyThread();//Thread-0
		obj3.start();
		*/

		MyThread obj4=new MyThread();
		System.out.println(obj4.getName());
		obj4.setName("xyz");
		obj4.start();

		MyThread obj5=new MyThread();
		System.out.println(obj5.getName());
		obj5.setName("pqr");
		obj5.start();

		MyThread obj6=new MyThread();
		System.out.println(obj6.getName());
		obj6.setName("abc");
		obj6.start();
	}
}
