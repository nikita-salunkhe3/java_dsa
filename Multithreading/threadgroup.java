class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
		System.out.println("In MyThread Constructor");
	}
	public void run(){
		System.out.println("In run: "+Thread.currentThread());
	}
}
class Client{
	
	public static void main(String[] args){

		ThreadGroup tg=new ThreadGroup("C");

		MyThread obj1=new MyThread(tg,"C");
		MyThread obj2=new MyThread(tg,"C");
		MyThread obj3=new MyThread(tg,"C");
//		MyThread obj4=new MyThread(tg,"Python");
		
		System.out.println(Thread.currentThread());

		obj1.start();
		obj2.start();
		obj3.start();
//		obj4.start();
	}
}
