class MyThread extends Thread{

	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread());
	}
}
class Client{
	public static void main(String[] args){

		System.out.println("In main");

		ThreadGroup pthread=new ThreadGroup("Indian");

		System.out.println(pthread);
	
		ThreadGroup cthread=new ThreadGroup(pthread,"Defence");
		
		System.out.println(cthread);

		MyThread obj1=new MyThread(cthread,"Army");
		MyThread obj2=new MyThread(cthread,"Navy");
		MyThread obj3=new MyThread(cthread,"AirForce");

		System.out.println("Thread Started");

		obj1.start();
		obj2.start();
		obj3.start();
	}
}
