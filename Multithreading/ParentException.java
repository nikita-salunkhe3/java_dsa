class Parent{
	void fun() throws InterruptedException{
		System.out.println("In parent run");

			Thread.sleep(3000);
	}
}
class Child extends Parent implements Runnable{

	public void run(){
		System.out.println("In Child run");
		try{
			fun();
		}catch(InterruptedException ex){
			System.out.println("Handled");
		}
	}
}
class Client{
	public static void main(String[] args){

		Child obj=new Child();

		Thread t=new Thread(obj);

		System.out.println("In main");

		t.start();

		System.out.println("In main");
	}
}



