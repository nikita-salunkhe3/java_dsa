class MyThread extends Thread{

	public void run(){

		System.out.println(Thread.currentThread().getName());

		for(int i=1;i<=10;i++){
			System.out.println("In run");
		}
	}
}
class Client{
	public static void main(String[] args){

		System.out.println("In main");

		MyThread obj=new MyThread();
		obj.start();

		try{
			obj.join(-100);//Unreported Exception InterruptedException must be caught or declare to br thrown

		}catch(InterruptedException ex){
			System.out.println("Exception Handled");
		}
		for(int i=1;i<=10;i++){
			System.out.println("In main");	
		}
		System.out.println("End main");
	}
}

