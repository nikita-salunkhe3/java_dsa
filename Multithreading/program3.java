/*
*4 things are complusory for Multithreading
1.Extends Thread class 
2.Have to Ovveride run method which can be present in Thread class
3.Call to start() method which is present in Thread class 
4.Do not override the start() method which is present in Thread class
*/

//I am Trying to Override start() method in MyThread class
//jer apan start() method lihili ter aplya start() method madhe yevdi power nahiye ki to eka new create kelelya thread la start karen (means fataka marel) menun apan start() hi method ovverride keraychi nahi ani jeri kelich ter apan jer thread benvla assla teri to thread kadhich kont kam exceute kerta yenar nahi karen to thread ajun start ch nahi zala menun

class MyThread extends Thread{
	public void run(){

		System.out.println("run method: "+Thread.currentThread().getName());
		System.out.println("In run");
	}
	public void start(){
		System.out.println("In start");
		run();
	}
}
class ThreadDemo{
	public static void main(String[] args){
		
		System.out.println("main method: "+Thread.currentThread().getName());

		MyThread obj=new MyThread();
		obj.start();
	}
}
/*
output:
main method: main
In start
run method: main
In run

ekch thread kam kertoy tyamule o/p exact yet
*/


