class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){

		System.out.println("In run :"+getName());//Thread class chi getName() method
	}
}
class ThreadDemo {
	public static void main(String[] args){

		ThreadGroup ptg=new ThreadGroup("Core2web");

		MyThread obj=new MyThread(ptg,"C CPP DS");
		obj.start();

		ThreadGroup ctg=new ThreadGroup(ptg,"Core2web");
		System.out.println("TG: "+ctg.getName());//ThreadGroup class chi method
		System.out.println(Thread.currentThread().getThreadGroup());//Thread class chi method

	}
}
