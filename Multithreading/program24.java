class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){

		System.out.println("In run");
	}
}
class ThreadGroupDemo{
	public static void main(String[] args){

		ThreadGroup tg=new ThreadGroup("Core2web");
		System.out.println("**tg.getName()**:"+tg.getName());//ThreadGroup class chi method ahe

		MyThread obj=new MyThread(tg,"XYZ");
		obj.start();

		System.out.println("**Obj.getThreadGroup()**: "+obj.getThreadGroup());//java.lang.ThreadGroup[name=Core2web,maxpri=10]
		System.out.println("**obj.getThreadGroup()**: "+obj.getThreadGroup());//null
		System.out.println("**obj.getThreadGroup()**: "+obj.getThreadGroup());//null

		System.out.println("In main: "+Thread.currentThread().getName());//main
	}
}


