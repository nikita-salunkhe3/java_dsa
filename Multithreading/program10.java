//Thread class Constructore sathi rich ahe 
//karen Thread class madhe ekun 10 constructor ahet

//Same kam kernarya threads cha grp kela jato tya thread grp la thread grp ase mentat

//ByDefault thread la 5 yevdi priority aaste

/****************Priority of Threads*************/

/***Unix madhe Process la jevda chota number assto tevdi tyachi priority vadte
 ***pn Linux Operating System nusar jevda Motha number asel tevdi tyala priority jast dili jate**/

class MyThread extends Thread{
	public void run(){
		Thread obj=Thread.currentThread();
		System.out.println(obj);//[main,5,main]
		obj.start();//IllegalThreadStateException

		Thread obj1=Thread.currentThread();
		System.out.println(obj1);//[main,5,main]
		obj1.start();//IllegalThreadStateException

		Thread obj2=new Thread();
		System.out.println(obj2);//[thread-1,5,main]
		obj2.start();
		
		Thread obj3=new Thread();
		System.out.println(obj3);//[thread-2,5,main]
		obj3.start();
	}
	public static void main(String[] args){
		MyThread obj1=new MyThread();
		obj1.run();
	}
}
