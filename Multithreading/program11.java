/****Priority of Thread******/
/*
[main,5,main] =>>  {Thread Name,Thread Priority, Thread Group Name}

[thread-0,5,main](jer me main madhe thread benvla ani object tya Thread cha print kela ter mala ass dist)
{Thread Name.Thread Priority,Thread Group Name}

Thread Priorities are:
Minimum Priority==1
Normal Priority==5
Maximum Priority==10

Parent chi Priority hi Child la dili jate
*/

class Demo extends Thread{
	public void run(){
		Thread t=new Thread();	
		System.out.println("Demo ThreadName: "+Thread.currentThread().getName());//thread-1
		System.out.println("Demo Thread: "+t);//[thread-2,5,main]
		System.out.println("Demo Priority: "+t.getPriority());
	}
}
class MyThread extends Demo{
	public void run(){
		Demo t=new Demo();
		System.out.println("MyThread ThreadName: "+Thread.currentThread().getName());//thread-0
		System.out.println("MyThread Thread: "+t);//[thread-1,5,main]
		System.out.println("MyThread Priority: "+t.getPriority());
		t.start();
	}
}
class ThreadDemo{
	public static void main(String[] args){

		MyThread obj=new MyThread();
		System.out.println("ThreadDemo ThreadName: "+Thread.currentThread().getName());//main
		System.out.println("ThreadDemo thread: "+obj);//[thread-0,5,main]
		System.out.println("ThreadDemo Priority:"+obj.getPriority());
		obj.start();

	}
}

/***Conclusion:
 * jer apan eka class madhun ek thread tayar kela parat dusrya thread madhun ajun ek thread tayar kela
 * ashich jer chain thevli ter serv je thread bentil te main Thread ya grp madhun asstil
 */
