class Node{
	int data;
	Node next=null;

	Node(int data){
		this.data=data;
	}
}
class LinkedList{
	Node head=null;
	
	void addFirst(int data){
		Node newnode = new Node(data);

		System.out.println("newnode: "+newnode);
		if(head==null){
			head=newnode;
		}else{
			newnode.next=head;
			head=newnode;
		}
		System.out.println("addfirst data: "+head.data);
		System.out.println("addfirst next: "+head.next);
	}		
	public static void main(String[] args){

		LinkedList obj=new LinkedList();

		System.out.println(obj.head);

		obj.addFirst(10);

		System.out.println(obj.head);
		
		System.out.println("LL data: "+obj.head.data);
		System.out.println("LL next: "+obj.head.next);
	}
}

