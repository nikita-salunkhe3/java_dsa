/*StackOverflowError -->>Error
                 
                   Object
		      |
		  Throwable
		      |
		    Error
		      |
	      VirtualMachineError
	              |
	      StackOverflowError

**All are in the java.lang package 
*/

class StackDemo{
	static void fun(int x){
		System.out.println(x);
		fun(++x);
	}
	public static void main(String[] args){

		fun(1);
	}
}
/*
 * Output:
 * version= 11
 * 10660
Exception in thread "main" java.lang.StackOverflowError
	at java.base/java.io.FileOutputStream.write(FileOutputStream.java:354)
	at java.base/java.io.BufferedOutputStream.flushBuffer(BufferedOutp.....StackTrace
 */
/*
 * jase apan version check karu tas no. of stack cha count change hoto
 * version - 8   approximate 10775
 * version -11   approximate 10673
 * version -16   approximate 16242
 * version -17   approximate 19234/19026/19187
 *
 * **JAVA madhe no. of stack count ha kami ahe as compare to 'C' language 
 * 'C' language madhe approximate 2,61,731 ya count peryant jato
 * 'C' language madhe stack count 2 lakh cha range madhe assto
 *
 * "C code"
#include<stdio.h>
int fun(int x){
        printf("%d\n",x);
        fun(++x);
}
void main(){
        fun(1);
}

 */
