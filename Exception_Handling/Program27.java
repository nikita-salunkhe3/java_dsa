//RealTime Example of User-defined Exception

import java.io.*;
import java.util.Scanner;

class DataOverFlowException extends IOException{
	DataOverFlowException(String msg){
		super(msg);
	}
}
class DataUnderFlowException extends IOException{
	DataUnderFlowException(String msg){
		super(msg);
	}
}
class ArrayDemo{
	public static void main(String[] args){

		int arr[]=new int[5];

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter integer value");
		System.out.println("Note: 0 < element < 100");

		for(int i=0;i<arr.length;i++){
			int data=sc.nextInt();

			if(data>100)
				throw new DataOverFlowException("Mitra data 100 peksha motha ahe");

			if(data<0)
				throw new DataUnderFlowException("Mitra data 0 peksha lahan ahe");
			arr[i]=data;
		}
		System.out.println("Array elements are:");
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}
/*
 * error: unreported exception DataOverFlowException; must be caught or declared to be thrown
				throw new DataOverFlowException("Mitra data 100 peksha motha ahe");
				^
Program27.java:33: error: unreported exception DataUnderFlowException; must be caught or declared to be thrown
				throw new DataUnderFlowException("Mitra data 0 peksha lahan ahe");
				^
2 errors
*/
