//realTime example

import java.io.*;

class ExtraCharacterException extends RuntimeException{
	ExtraCharacterException(String msg){
		super(msg);
	}
}
class LessCharacterException extends RuntimeException{
	LessCharacterException(String msg){
		super(msg);
	}
}
class CheckPassword{
	public static void main(String[] args){

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Passward");

		String str=null;
		try{
			str=br.readLine();
		}catch(IOException obj){

		}

		char arr[]=str.toCharArray();

		if(arr.length<8){
			throw new LessCharacterException("Enter 8 character for Passward");
		}
		if(arr.length>8){
			throw new ExtraCharacterException("You Enter more than 8 character");
		}
		System.out.print("Password is: ");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]);
		}
		System.out.println();
	}
}

