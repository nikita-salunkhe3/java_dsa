//Handle StackOverflowError
/*Normal termination is possible in Error */

class StackDemo{
	static void fun(int x){
	        System.out.println(x);
		fun(++x);
	}
	public static void main(String[] args){

		System.out.println("Start main");
		
		try{
			fun(1);
		}catch(StackOverflowError obj){

			System.out.println("Exception handle");
		}
		System.out.println("End main");
	}
}
/*
output:
version-11
....
10666
10667
10668
10669
10670Exception handle
End main
*/
