//Throw (Clause/Keyword)
//throw apan Predefine Exception throw kernyasathi sudha used kerto pn company Prototype nusar Userdefine exception throw kele jatat
//throw ha userdefine exception feknyasathi used kela jato(Comapany Prototype)
//throws ha Exception deligate kernyasathi used kela jato (deligate means jababdari sopavne)
//throw -->> Exception pude yenar ahe ki nahi yachi vat nko baghu te Exception Occurs hoil ki nahi mahit nahi pn tu throw ker yasathi throw vapertat
//he ka kel jat karen java hi Robust Language ahe
//throw cha khali konti hi line lihili ter error yeto : unreachable statement 

import java.util.*;

class Demo{
	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter data");

		int x=sc.nextInt();

		try{
			if(x==0){
				throw new ArithmeticException("Divide by 0");
				System.out.println(10/0);//error
			}
		}catch(ArithmeticException obj){
				System.out.println("Handle");
		}
	}
}
/*
 * error: unreachable statement
				System.out.println(10/0);//error
				^
i
 */


