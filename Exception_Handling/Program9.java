//why the need of Try and catch rather than throws
/*
class Demo{
	public static void main(String[] args)throws ArithmeticException{

		System.out.println("start main");

		System.out.println(10/0);

		System.out.println("End Main");
	}
}*/

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException,NumberFormatException{

		System.out.println("Start main");

		System.out.println("Enter data");

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int num=Integer.parseInt(br.readLine());

		System.out.println(num);
	
		System.out.println("End Main");
	}
}


		


