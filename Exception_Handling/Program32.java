import java.io.*;
class DataOverFlowException extends RuntimeException{
	DataOverFlowException(String msg){

		super(msg);
	}
}
class DataUnderFlowException extends RuntimeException{
	DataUnderFlowException(String msg){
		super(msg);
	}
}
class Demo{
	public static void main(String[] args){

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter data");

		int data=0;

		try{
			data=Integer.parseInt(br.readLine());
			if(data>100)
				throw new DataOverFlowException("Mitra data 100 peksha motha ahe");

			if(data<0){
				throw new DataUnderFlowException("Mitr data 0 peksha chota ahe");
			//	System.out.println("In try");error-unreachable statement
			}
		
		}catch(IOException | DataUnderFlowException | DataOverFlowException obj ){
			System.out.println("Exception Handle");
		}
		System.out.println("End main");

	}
}


