//IOException he checked Exception ahe 
//IOException he generally throws kele jatat

import java.io.*;

class Demo{
	public static void main(String[] args) throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		String str=br.readLine();
		System.out.println(str);

		br.close();

		int num=Integer.parseInt(br.readLine());
		System.out.println(num);
	}
}
/*
 * output:
nikita
nikita
Exception in thread "main" java.io.IOException: Stream closed
	at java.base/java.io.BufferedReader.ensureOpen(BufferedReader.java:123)
	at java.base/java.io.BufferedReader.readLine(BufferedReader.java:321)
	at java.base/java.io.BufferedReader.readLine(BufferedReader.java:396)
	at Demo.main(Program8.java:16)
*/

