import java.io.*;

class ExceptionDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		String str=br.readLine();
		System.out.println(str);

		System.out.println("Enter Integer data");
		int data=0;
		try{
			data=Integer.parseInt(br.readLine());

		}catch(NumberFormatException obj){
			System.out.println("Please enter integer data");
			data=Integer.parseInt(br.readLine());
		}
		System.out.println(data);
	}
}

