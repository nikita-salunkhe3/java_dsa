
//ekach catch cha bracket madhe apan multiple Exception lihu shakto
//ha secnario tevach vapraycha jeva aplyala serv exception sathi ekch comment keraych ahe or ekch 
//seravnsathi common SOP() chi line lihaychi assel tevach....

import java.io.*;

class UserInput{
	public static void main(String[] args){

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("In main");

		System.out.println("Enter the integer");

		int num=0;

		try{
			num=Integer.parseInt(br.readLine());
		}catch(IOException | NumberFormatException | NullPointerException obj){
			System.out.println("Exception found");
		}
		System.out.println("End main");
	}
}
