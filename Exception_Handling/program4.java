//Runtime Exception
//1. ArithmaticException

class Demo{
	void m1(){
		System.out.println("start m1");
		m2();
		System.out.println("end m1");
	}
	void m2(){
		System.out.println("start m2");
		System.out.println(10/0);
		System.out.println("end m2");
	}
	public static void main(String[] args){
		Demo obj=new Demo();
		obj.m1();
	}
}
/*
 * output:
 * start m1
start m2
Exception in thread "main" java.lang.ArithmeticException: / by zero
	at Demo.m2(program4.java:12)
	at Demo.m1(program4.java:7)
	at Demo.main(program4.java:17)

*/
