/*
Exception Handling:
        *aapla code run hot astana Uncertain event handle keranyasathi exception handling uesd kel jat
	*aapla code run hot asstana abnormal condition found hotat mg apla code completly run honyasathi
         	exception handling uesd kel jat
	*Exception 2 padhtine handle kel jat
	   1.throws
	   2.used Try catch
	*Exception handling sathi base class ha throwable class ahe
        *Throwable class cha Parent class ha object class ahe
                          
                                     Object
			               |
				   Throwable
				       |
			     ------------------------------
		             |                            |
			   Error                       Exception
	                        	------------------------------------------
	                         	|                                        |
			       CompileTimeException/                    RunTimeException/
			        Checked Exception                       Unchecked Exception

				*IOException                            *NullPointerException
				                                        *ArithmaticException
									*NumberFoarmateException
									*IndexOutOfBoundes
									  -ArrayIndexOutOfBounds
									  -StringIndexOutOfBounds
*/
      
//IOException-CompileTime exception

import java.io.*;
class Demo{
	public static void main(String[] args){

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter data");

		int data=Integer.parseInt(br.readLine());

		System.out.println(data);
	}
}
/*
output:
error: unreported exception IOException; must be caught or declared to be thrown
		int data=Integer.parseInt(br.readLine());
		                                     ^
1 error

*/
