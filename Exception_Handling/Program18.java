//jeva apan OR madhe catch madhe exception lihu teva te exception ekmekanche siblings teri pahijet or
//diferent assel pahijet tyat Parent Exception lihun chalnar nahi
//OR Exception catch madhe lihit asstana Exact Exception code madhe kont yetay he mahit assl pahije
//ani OR Exception madhe Exact exception lihayche

import java.io.*;

class Input{
	public static void main(String[] args){

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("start main");

		System.out.println("Enter data");

		int num=0;
		try{
			 num=Integer.parseInt(br.readLine());
		}catch(IOException | NumberFormatException | RuntimeException obj){
			System.out.println("Exception found");
		}
		System.out.println(num);
		System.out.println("End main");
	}
}
/*
 error: Alternatives in a multi-catch statement cannot be related by subclassing
		}catch(IOException | NumberFormatException | RuntimeException obj){
		                                             ^
  Alternative NumberFormatException is a subclass of alternative RuntimeException
1 error
*/
