import java.io.*;

class Demo{
	public static void main(String[] args){

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the integer value");

		int data=0;
		try{
			 data=Integer.parseInt(br.readLine());
		}catch(NumberFormatException obj){

			System.out.println("Exception found and i can handle it");

		}catch(IOException obj1){
		          
			System.out.println("Exception found and i can handle it");
		}

		System.out.println(data);
		System.out.println("End Main");
	}
}
