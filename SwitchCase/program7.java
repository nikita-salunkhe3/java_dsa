//realtime example


class NestedSwitch{
	public static void main(String[] args){
		System.out.println("TV Channels");
		String str="Zee";
		switch(str){
			case "Zee":
				{
					String str1="Zee Cinema";
					switch(str1){
						case "Zee Cinema":
							{
								String str2="Movies";
								switch(str2){
									case "Movies":
										System.out.println("Hindi Movies");
										break;

									case "Ads":
										System.out.println("Boring Ads");
										break;

									default:
										System.out.println("Invalid case");
										break;
								}
							}
							break;

						case "Zee Marathi":
							{
								String str2="Movies";
								switch(str2){
									case "Movies":
										System.out.println("Marathi movies");
										break;

									case "Ads":
										System.out.println("Boring ads");
										break;

									default:
										System.out.println("Invalid case");
										break;
								}
							}
							break;
					}
				}
				break;
		}
		System.out.println("TV band");
	}
}

