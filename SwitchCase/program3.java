class switchDemo{
	public static void main(String[] args){
		int ch=65;

		switch(ch){
			case 'A':
				System.out.println("char-A");
				break;
			case 65:
				System.out.println("num-A");//error-Duplicate case label
				break;
			case 'B':
				System.out.println("char-B");
				break;
			case 66:
				System.out.println("num-B");//error-Duplicate case label
				break;
			default:
				System.out.println("Invalid");
				break;
		}
	}
}
