class SwitchDemo{
	public static void main(String[] args){
		int x=5;

		switch(x){
			case 1:
				System.out.println("1");
				break;
			case 2:
				System.out.println("2");
				break;
			case 5:
				System.out.println("first-5");
				break;
			case 5:
				System.out.println("second-5");//error...Duplicate case label
				break;
			case 2:
				System.out.println("second-2");//error...Duplicate case label
				break;
			default:
				System.out.println("No match");
				break;
		}
		System.out.println("After switch");
	}
}

