class switchDemo{
	public static void main(String[] args){

		int x=3;
		int a=1;
		int b=2;

		switch(x){
			case a://error....constant expression required
				System.out.println("1");
				break;
			case b://error
				System.out.println("2");
				break;
			case a+b://error
				System.out.println("3");
				break;
			case a+a+b://error
				System.out.println("4");
				break;
			case a+b+b://error
				System.out.println("5");
				break;
			default://error
				System.out.println("Invalid");
				break;
		}
	}
}
//output-5 error
