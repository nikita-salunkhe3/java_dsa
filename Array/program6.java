class Demo{
	void fun(int arr[]){
		arr[0]=500;
		arr[1]=400;
	}
	public static void main(String[] args){

		int arr[]=new int[]{200,300,400,500};

		for(int x:arr){
			System.out.println(x);
		}

		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(arr[3]));

		Demo obj=new Demo();

		obj.fun(arr);

		int x=500;
		int y=400;
		
		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));

		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
	}
}
		
