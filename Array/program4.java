class Demo{
	public static void main(String[] args){
//		int arr[]={1,A,2};//A la error Cannot find symbol

//		int arr1[]={1,12.11,4,5,6};//12.11 error incompatible type lossy conversion from double
		//to int

		int arr2[]={1,'A','B',3};//no error output --->> 1 65 66 3
		
		float arr3[]={1,2,3,11.5f};//no error output--->> 1.0  2.0  3.0  11.5

		char arr4[]={'A',65,66,50};//no error output--->> A  A  B  2

//		char arr5[]={A,B,C};//error A B C tighana pn (cannot find symbol)

//		char arr6[]={'A',32.11f};//32.11 la error incompatible type:possible lossy conversion
		//of Float to char

		System.out.println(&arr);

		for(int i=0;i<arr4.length;i++){
			System.out.print(arr4[i]+"  ");
		}
		System.out.println();

		
	}
}
