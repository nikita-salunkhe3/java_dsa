
import java.io.*;
class ArrayDemo{
	public static void main(String[] args){

		int arr[][][]={{{1,2,3},{4,5,6},{7,8,9}},{{10,11,12},{13,14,15},{16,17,18}}};

		System.out.println(arr.length);//2 ->> plane
		System.out.println(arr[0].length);//3->>> rows
		System.out.println(arr[0][0].length);//3-->> column
	}
}

