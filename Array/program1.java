import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter array size");

		int size=Integer.parseInt(br.readLine());

		int []arr=new int[size];
		
		System.err.println("enter elements of an array");

		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.err.println("Array elements are");
		for(int i=0;i<size;i++){
			System.out.print(arr[i]+"  ");
		}
		System.out.println();
	}
}


