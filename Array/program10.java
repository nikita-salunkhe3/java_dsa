//jagged array for 3D array

import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number of plane");

		int plane=Integer.parseInt(br.readLine());

		int arr[][][]=new int[plane][][];

		for(int i=0;i<arr.length;i++){
			System.out.println("Enter number of rows for "+i+" plane");

			int row=Integer.parseInt(br.readLine());

			arr[i]=new arr[row];

			for(int j=0;j<arr[i].length;i++){
				System.out.println("enter no of columns:");
				int col=Integer.parseInt(br.readLine());
				 arr[i][j]=new int[][col];

				 for(int k=0;k<arr[i][j].length;k++){
					 System.out.println("Enter array elements");
					 arr[i][j][k]=Integer.parseInt(br.readLine());
				 }
			}

		}

		System.out.println();
		System.out.println("Array becomes");
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;i++){
				for(int k=0;k<arr[i][j].length;k++){
					System.out.print(arr[i][j][k]);
				}
				System.out.println();
			}
			System.out.println();
			System.out.println();
		}
	}
}
