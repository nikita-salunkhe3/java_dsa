//realTime example

abstract class PTC{
	void workTime(){
		System.out.println("9:00 - 6:00");
	}
	abstract void role();
}
class Student extends PTC{
	void role(){
		System.out.println("Freshers");
	}
}
class Main{
	public static void main(String[] args){

		PTC obj=new Student();
		obj.workTime();
		obj.role();
	}
}

