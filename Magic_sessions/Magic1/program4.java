//by using simple while loop
//Write a program to print count of odd digits of given number

class Demo{
	public static void main(String[] args){
		int num=1234567;

		int count=0;
		while(num!=0){
			int rem=num%10;
			if(rem%2==1){
				count++;
			}
			num=num/10;
		}
		System.out.println(count);
	}
}
