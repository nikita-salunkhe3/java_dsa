//used here while loop
//if number is even then add them
//and if number is odd the take product of the number

class Demo{
	public static void main(String[] args){
		int num=12345;
		int sum=0;
		int product=1;

		while(num != 0){
			int rem=num%10;

			if(rem%2==0){
				sum=sum+rem;
			}else{
				product=product*rem;
			}
			num=num/10;
		}
		System.out.println("sum of even number is = "+sum);
		System.out.println("product of odd number is = "+product);
	}
}
