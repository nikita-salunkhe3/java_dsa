//by using simple while loop
//Write a program to print table of 2

class Demo{
	public static void main(String[] args){
		int num=2;

		int i=1;
		while(i<=10){
			System.out.println(num*i);
			i++;
		}
	}
}
