//by using while loop
//write a program to count the digits of given number

class Count{
	public static void main(String[] args){

		int num=94533;

		int count=0;
		while(num != 0){
			count++;
			num=num/10;
		}
		System.out.println(count);
	}
}
