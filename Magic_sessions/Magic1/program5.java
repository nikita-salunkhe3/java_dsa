//by using while loop only
//Write a program to print square of even digit of given number

class Square{
	public static void main(String[] args){
		int num=123456789;

		while(num != 0){
			int rem=num%10;
			if(rem % 2==0){
				System.out.println(rem*rem);
			}num=num/10;
		}
	}
}

