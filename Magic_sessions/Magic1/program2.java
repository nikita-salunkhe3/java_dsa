//by using simple While loop
//write a program to calculate the factorial of given number
//input:6
//output:720


class Fact{
	public static void main(String[] args){
		int num=6;

		int fact=1;
		
		while(num>1){
			fact=fact*num;
			num--;
		}
		System.out.println(fact);
	}
}

			

