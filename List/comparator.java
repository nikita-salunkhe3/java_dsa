//To sort arraylist data 

import java.util.*;

class Employee{
	String name=null;
	int empId=0;

	Employee(String name,int empId){

		this.name=name;
		this.empId=empId;
	}
	public String toString(){
		return name+":"+empId;
	}
}
class SortByName implements Comparator<Employee> {

	public int compare(Employee obj1,Employee obj2){
		return (obj1.name).compareTo(obj2.name);
	}
}
class SortByEmpID implements Comparator<Employee>{

	public int compare(Employee obj1,Employee obj2){
		return (int)(obj1.empId-obj2.empId);
	}
}
class Client{
	public static void main(String[] args){
		
		ArrayList<Employee> al=new ArrayList<Employee>();

		al.add(new Employee("nikita",1));
		al.add(new Employee("sarthak",2));
		al.add(new Employee("martand",3));
		al.add(new Employee("venkya",4));

		System.out.println(al);

		Collections.sort(al,new SortByName());

		System.out.println(al);

		Collections.sort(al,new SortByEmpID());
		
		System.out.println(al);
	}
}

