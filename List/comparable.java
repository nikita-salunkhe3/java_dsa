import java.util.*;
class Platform implements Comparable{
	String str=null;
	int foundYear=0;

	Platform(String str,int foundYear){
		this.str=str;
		this.foundYear=foundYear;
	}

	public int compareTo(Object obj){
		return (int)((this.foundYear)-((Platform)obj).foundYear);
	}
	public String toString(){
		return "{"+str+":"+foundYear+"}";
	}
}

class Client{
	public static void main(String[] args){
			
		TreeMap<Platform,String> tm=new TreeMap<Platform,String>();

		tm.put(new Platform("YouTube",2005),"Google");
		tm.put(new Platform("Instagram",2010),"Meta");
		tm.put(new Platform("ChatGPT",2022),"OpenAI");

		System.out.println(tm);
	}
}
