/***************TreeSet******************/

/*
 * TreeSet madhe sorted data store kela jato
 * TreeSet madhe Duplicate data allow karat nahi
 * TreeSet madhe jer user-defined object tayar karat asu ter to User-defined class Comparable type cha class Asava lagto
 */

import java.util.*;

class Movies implements Comparable{
	String movieName = null;
	float imdb = 0.0f;

	Movies(String movieName,float imdb){
		this.movieName = movieName;
		this.imdb = imdb;
	}
	public String toString(){
		return movieName;
	}
	public int compareTo(Object obj){
		return movieName.compareTo(((Movies)obj).movieName);
	}
}
class TreeSetDemo{
	public static void main(String[] args){

		TreeSet ts = new TreeSet();

		ts.add(new Movies("OMG2",8.0f));
		ts.add(new Movies("Gadar",8.5f));
		ts.add(new Movies("jalier",9.0f));

		System.out.println(ts);

		System.out.println(ts.getClass().getName());//java.util.TreeSet
	}
}

