/*
 * Predefined class la jer sop() madhe lihil ter internally tya object ver toString() ya method la call
 * jato menun alyala actual data disto
 * pn jer apan User-defined class cha object tayar kela ani jer mala tya object cha address print hoil
 * aplyala tya object madhil actual data print karun pahije asel ter Object class madhil toString()
 * hi method aplyala aplya user-defined class madhe override keravi lagte 
 */

import java.util.*;

class Country{
	String name = null;

	Country(String name){
		this.name = name;
	}

	public String toString(){
		return name;
	}
}
class GCHashMapDemo{
	public static void main(String[] args){

		HashMap hm = new HashMap();

		hm.put(new Country("India"),"IND");
		hm.put(new Country("Pakistan"),"PAK");
		hm.put(new Country("Shrilanka"),"SL");
		hm.put(new Country("NewYork"),"NY");

		System.out.println(hm);
	}
}
