import java.util.*;

class Defence implements Comparable{
	String defenceWay = null;

	Defence(String defenceWay){
		this.defenceWay = defenceWay;
	}
	public String toString(){
		return defenceWay;
	}
	public int compareTo(Object obj){
		return defenceWay.compareTo(((Defence)obj).defenceWay);
	}
}
class TreeSetDemo{
	public static void main(String[] args){

		TreeSet ts = new TreeSet();

		ts.add(new Defence("Army"));
		ts.add(new Defence("Navy"));
		ts.add(new Defence("Airforce"));

		System.out.println(ts);
	}
}
/*OUTPUT:
 * [Airforce, Army, Navy]
*/
