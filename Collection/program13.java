/*
 * Enumeration ha fkt Stack ani Vector ver chalto
 * Methods of Enumeration*
 *
  public abstract boolean hasMoreElements();
  public abstract E nextElement();
  public default java.util.Iterator<E> asIterator();
 */

import java.util.*;
class EnumerationDemo{
	public static void main(String[] args){

		Vector v=new Vector();
		v.add(10);
		v.add(20);
		v.add(10);

		Enumeration num = v.elements();

		while(num.hasMoreElements()){
			System.out.println(num.nextElement());
		}
	}
}
