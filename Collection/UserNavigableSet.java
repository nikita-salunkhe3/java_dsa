//User-defined class 
//Implements Navigable set 

import java.util.*;

class School implements Comparable{

	String name= null;
	char grade = ' ';

	School(String name,char grade){
		this.name = name;
		this.grade = grade;
	}
	public String toString(){
		return "{"+name+":"+grade+"} ";
	}
	public int compareTo(Object obj){

		return name.compareTo(((School)obj).name);
	}
}
class NavigableSetDemo{
	public static void main(String[] args){

		NavigableSet ns = new TreeSet();

		ns.add(new School("DMVS",'A'));
		ns.add(new School("Shivaji",'B'));
		ns.add(new School("Adersh",'C'));
		ns.add(new School("MES",'D'));

		System.out.println(ns);

		/************Methods Of NavigableSet Interface**************/
		/****public abstract E lower(E);
  public abstract E floor(E);
  public abstract E ceiling(E);
  public abstract E higher(E);
  public abstract E pollFirst();
  public abstract E pollLast();
  public abstract java.util.Iterator<E> iterator();
  public abstract java.util.NavigableSet<E> descendingSet();
  public abstract java.util.Iterator<E> descendingIterator();
  public abstract java.util.NavigableSet<E> subSet(E, boolean, E, boolean);
  public abstract java.util.NavigableSet<E> headSet(E, boolean);
  public abstract java.util.NavigableSet<E> tailSet(E, boolean);
  public abstract java.util.SortedSet<E> subSet(E, E);
  public abstract java.util.SortedSet<E> headSet(E);
  public abstract java.util.SortedSet<E> tailSet(E);
*/
		System.out.println(ns);//[{Adersh:C}, {DMVS:A}, {MES,D}, {Shivaji:B}]

		///E lower(E);// Paramter cha ek gher adhicha data print karun deto

		System.out.println(ns.lower(new School("MES",'D')));//[{DMVS:A}]
		
		System.out.println(ns.lower(new School("Adersh",'C')));//null

		///E floor(E);
		System.out.println(ns.floor(new School("DMVS",'A')));//[{DMVS:A}]
		System.out.println(ns.floor(new School("Shivaji",'B')));//[{Shivaji:B}]

		//E ceiling(E);
		System.out.println(ns.ceiling(new School("DMVS",'A')));//[{DMVS:A}]
		System.out.println(ns.ceiling(new School("Shivaji",'B')));//[{Shivaji,B}]

		//E higher(E);
		System.out.println(ns.higher(new School("DMVS",'A')));//{MES:D}
		System.out.println(ns.higher(new School("Shivaji",'B')));//null

		//E pollFirst();
		System.out.println(ns.higher(new School("DMVS",'A')));//{MES:D}
		System.out.println(ns.higher(new School("Shivaji",'B')));//null

		//E pollLast();
		System.out.println(ns.higher(new School("DMVS",'A')));//{MES:D}
		System.out.println(ns.higher(new School("Shivaji",'B')));//null

		//NavigableSet descendingSet();
		System.out.println(ns.descendingSet());//[{Shivaji:B}, {MES:D}, {DMVS:A}, {Adersh:C}]

		//Iterator iterator()

		Iterator itr = ns.iterator();
		System.out.println(itr);//java.util.TreeMap$KeyIterator@63961c42

		System.out.println(itr.getClass().getName());//java.util.TreeMap$KeyIterator

		while(itr.hasNext()){
			System.out.println(itr.next());
		}
		//NavigableSet descendingIterator()

		System.out.println(ns.descendingIterator());//java.util.TreeMap$NavigableSubMap$DescendingSubMapKeyIterator@65b54208

		//NavigableSet subSet(E, boolean, E, boolean);
	//	System.out.println(ns.subSet((new School("Adersh",'C')),true,(new School("MES",'D')),true));

		//NavigableSet headSet(E, boolean);
		System.out.println(ns.headSet((new School("Adersh",'C')),true));	

		//NavigableSet tailSet(E, boolean);
		System.out.println(ns.tailSet(new School("DMVS",'A')));

		//sortedSet subSet(E, E);
		System.out.println(ns.subSet((new School("DMVS",'A')),(new School("Shivaji",'B'))));
	}
}




