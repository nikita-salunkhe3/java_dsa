import java.util.*;

class IteratorDemo{
	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.add(10);
		ll.add(20);
		ll.add(30);

		System.out.println(ll);
		System.out.println(ll.remove());
		System.out.println(ll);

		Iterator itr=ll.iterator();
		itr.remove();

		System.out.println(itr.getClass());
		
		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20);
		al.add(30);

		Iterator itr1=al.iterator();

		System.out.println(itr1.getClass().getComponentType());
	}
}

		
