import java.util.*;
class ArrayListDemo{
	public static void main(String[] args){

		ArrayList al=new ArrayList();

		al.add(10);
		al.add(20);
		al.add(30);
		al.add(new Integer(40));//Warning for Version-17 and 19
		//instead of new Integer(); java gives valueOf() method for creation of Object of Integer
		al.add(30);

		for(var obj : al){ 
			System.out.println(obj);
		}
	}
}
/*
 * Compiletime
 *  warning: [removal] Integer(int) in Integer has been deprecated and marked for removal
		al.add(new Integer(40));
		       ^ 
Note: program4.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 warning
*/
