import java.util.*;

class SortedSetDemo{
	public static void main(String[] args){

		SortedSet ss = new TreeSet();

		ss.add("Nikita");
		ss.add("Sarthak");
		ss.add("Shweta");
		ss.add("Aakash");

		System.out.println(ss);//[Aakash, Nikita, Sarthak, Shweta]
		/**public abstract java.util.Comparator<? super E> comparator();
  public abstract java.util.SortedSet<E> subSet(E, E);
  public abstract java.util.SortedSet<E> headSet(E);
  public abstract java.util.SortedSet<E> tailSet(E);
  public abstract E first();
  public abstract E last();
  public default java.util.Spliterator<E> spliterator();
		 */

		System.out.println(ss.subSet("Nikita","Shweta"));//[Nikita, Sarthak]
		System.out.println(ss.headSet("Nikita"));//[Aakash]
		System.out.println(ss.tailSet("Nikita"));//[Nikita, Sarthak, Shweta]
		System.out.println(ss.first());//[Aakash]
		System.out.println(ss.last());//[Shweta]
		System.out.println(ss.comparator());//null
	}
}


