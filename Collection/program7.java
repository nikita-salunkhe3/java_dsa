import java.util.*;
class IteratorDemo{
	public static void main(String[] args){

		ArrayList al=new ArrayList();

		al.add("Kanha");
		al.add("Rahul");
		al.add("Ashish");

		Iterator itr=al.iterator();

		while(itr.hasNext()){
			var obj=itr.next();
			if("Rahul".equals(obj))
				itr.remove();
			else 
				System.out.println(obj);
		}
		System.out.println(al);
	}
}

