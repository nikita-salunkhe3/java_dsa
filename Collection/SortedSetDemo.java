import java.util.*;
        

class States implements Comparable{
	String SName = null;
	float illierate = 0.0f;

	States(String SName,float illierate){
		this.SName = SName;
		this.illierate = illierate;
	}
	public String toString(){
		return "{"+SName+":"+illierate+"}";
	}
	/*
	 * public interface java.lang.Comparable<T> {
	   public abstract int compareTo(T);
           }
	 */

	public int compareTo(Object obj){
		return SName.compareTo(((States)obj).SName);
	}
}
class SortedSetDemo{
	public static void main(String[] args){

		SortedSet ts = new TreeSet();

		ts.add(new States("MH",2.1f));
		ts.add(new States("Goa",3.1f));
		ts.add(new States("Kanyakumari",2.8f));
		ts.add(new States("Gujrat",4.9f));

		System.out.println(ts);//[{Goa,3.1f}, {Gujrat,4.9}, {Kanyakumari,2.8f}, {MH,2.1f}]

		/***Methods Of SortedSet Interface******/
		/****public interface java.util.SortedSet<E> extends java.util.Set<E> {
  public abstract java.util.Comparator<? super E> comparator();
  public abstract java.util.SortedSet<E> subSet(E, E);
  public abstract java.util.SortedSet<E> headSet(E);
  public abstract java.util.SortedSet<E> tailSet(E);
  public abstract E first();
  public abstract E last();
  public default java.util.Spliterator<E> spliterator();
}
*/
		System.out.println(ts.subSet((new States("Kanyakumari",2.8f)),(new States("MH",2.1f))));

		System.out.println(ts.subSet((new States("Kanyakumari",2.8f)),(new States("Kanyakumari",2.8f))));//[]
		
		System.out.println(ts.headSet(new States("MH",2.1f)));//
		
		System.out.println(ts.tailSet(new States("Gujrat",4.9f)));//[{Gujart,4.9}, {Kanyakumari,2.8},{MH, 2.1}]

		System.out.println(ts.first());//[{Goa,3.1}]
		System.out.println(ts.last());//[{MH,2.1}]

	}
}



