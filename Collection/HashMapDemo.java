import java.util.*;

class HashMapDemo{
	public static void main(String[] args){

		HashSet hs = new HashSet();

		hs.add("Kanha");
		hs.add("Ashish");
		hs.add("Badhe");
		hs.add("Rahul");

		System.out.println(hs);

		HashMap hm = new HashMap();

		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Badhe","carPro");
		hm.put("Rahul","BMC");

		System.out.println(hm);

		HashMap hm1 = new HashMap();

		hm1.put("Kanha","Infosys");
		hm1.put("Ashish","Barclays");
		hm1.put("Kanha","carPro");
		hm1.put("Kanha","BMC");

		System.out.println(hm1);

	}
}
