import java.util.*;

class Player{
	String name = null;
	int jerNo = 0;

	Player(String name, int jerNo){
		this.name = name;
		this.jerNo = jerNo;
	}
	public String toString(){
		return "{" +name+ ":" +jerNo+ "}";
	}
}
class SortByName implements Comparator{
	public int compare(Object obj1,Object obj2){
		return (((Player)obj1).name).compareTo(((Player)obj2).name);
	}
}
class SortByJerNo implements Comparator{
	public int compare(Object obj1,Object obj2){
		return (int)((((Player)obj1).jerNo) - (((Player)obj2).jerNo));
	}
}

class SortHashSet{
	public static void main(String[] args){

		HashSet hs = new HashSet();

		hs.add(new Player("Virat",18));
		hs.add(new Player("MSD",7));
		hs.add(new Player("Rohit",45));
		hs.add(new Player("Sachin",10));

		System.out.println(hs);

		LinkedList ll = new LinkedList(hs);

		System.out.println("Sort by Name");
	
		Collections.sort(ll,new SortByName());
		System.out.println(ll);
	
		System.out.println("Sort by jerNo");
		Collections.sort(ll,new SortByJerNo());
		System.out.println(ll);
	}
}
