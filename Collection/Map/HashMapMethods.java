/**************HashMap Constructor************/
/***  public java.util.HashMap(int, float);
      public java.util.HashMap(int);
      public java.util.HashMap();
      public java.util.HashMap(java.util.Map<? extends K, ? extends V>);
*/

/***********HashMap Methods***************/
/*** v get(java.util.object)
 *   v put(k,v);
 *   v remove(Object);
 *   set<k> keySet();
 *   Collection<v> values();
 *   set <java.util.Map$Entry(k,v)> entrySet();
 */

import java.util.*;

class HashMapMethods{
	public static void main(String[] args){

		HashMap hm = new HashMap();

		hm.put("Nikita",1);
		hm.put("Sarthak",2);
		hm.put("Shweta",3);
		hm.put("Aakash",4);
		hm.put("Chirag",5);

		System.out.println(hm);

		/**** v get(java.util.Object)***/
		System.out.println(hm.get("Nikita"));//1

		/****v remove(Object)*****/
		System.out.println(hm.remove("Chirag"));//5

		System.out.println(hm.remove("Nisha"));//null

		/***** set<k> keySet();*******/

		System.out.println(hm.keySet());//{Nikita, Sarthak, Shweta, Aakash}....Sequence as
		//per Hashing Function is called

		/*******set entrySet()*********/

		System.out.println(hm.entrySet());//{Nikita=1, Sarthak=2, Shweta=3, Aakash=4}
	}
}


