import java.util.*;

class PriorityQueueDemo{
	public static void main(String[] args){

		PriorityQueue pq = new PriorityQueue();

		pq.add(30 );
		pq.add(20);
		pq.add(40);
		pq.add(10);

		System.out.println(pq);
	}
}
