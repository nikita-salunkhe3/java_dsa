import java.util.*;

class Demo{
	String str=null;

	Demo(String str){
		this.str = str;
	}
	public String toString(){
		return str;
	}
	public void finalize(){
		System.out.println("Notification");
	}
}
class GCDemo{
	public static void main(String[] args){

		Demo obj1 = new Demo("Core2web");
		Demo obj2 = new Demo("Incubator");
		Demo obj3 = new Demo("Biencaps");

		LinkedHashMap lhm = new LinkedHashMap();

		lhm.put(obj1,1);
		lhm.put(obj2,2);
		lhm.put(obj3,3);

		obj1=null;
		obj2=null;

		System.gc();
		System.out.println(lhm);
	}
}
		
