/*
 * Garbage collector actual madhe adhi finalize() method la call kerto ani nanter Object Delete kerto
 */
/** * System.runFinalization(): This method forces the garbage collector to run and call the finalize() method for all objects that have not yet been finalized.
 */
import java.util.*;

class Demo{
	String name = null;

	Demo(String name){
		this.name = name;
	}
	public String toString(){
		return name;
	}
	public void finalize(){//finalized cha call swata Garbage Collector deto
		System.out.println("Your Object is deleted");
	}
}
class GCWorking{
	public static void main(String[] args){

		Demo obj1 = new Demo("Nikita");
		Demo obj2 = new Demo("Shweta");
		Demo obj3 = new Demo("kirti");
		Demo obj4 = new Demo("Sanyogita");

		obj1=null;
		System.gc();//Explicity call kertoy Garbage collector la ki tu null aslele object 
		//free karun tak

//		System.runFinalization();//This Method is debricated
//
		System.out.println("In main");


	}
}
