import java.util.*;

class Demo{
	String str=null;

	Demo(String str){
		this.str = str;
	}
	public String toString(){
		return str;
	}
	public void finalize(){
		System.out.println("Notification");
	}
}
class GCDemo{
	public static void main(String[] args){

		Demo obj1 = new Demo("Core2web");
		Demo obj2 = new Demo("Incubator");
		Demo obj3 = new Demo("Biencaps");

		WeakHashMap whm = new WeakHashMap();

		whm.put(obj1,1);
		whm.put(obj2,2);
		whm.put(obj3,3);

		obj1=null;

		System.gc();
		System.out.println(whm);
	}
}
		
