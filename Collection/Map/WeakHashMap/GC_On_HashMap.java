import java.util.*;
class Demo{
	int num=0;

	Demo(int x){
		num=x;
	}
	public String toString(){
		return num + " ";
	}
	public void finalize(){
		System.out.println("Notification");
	}
}
class GCHashMap{
	public static void main(String[] args){

		Demo obj1 = new Demo(10);
		Demo obj2 = new Demo(20);
		Demo obj3 = new Demo(30);
		Demo obj4 = new Demo(40);

		HashMap hm = new HashMap();

		hm.put(obj1,'A');
		hm.put(obj2,'B');
		hm.put(obj3,'C');
		hm.put(obj4,'D');

		System.out.println(hm);

		obj1 = null;
		obj3 = null;

		System.gc();
	
		System.out.println(hm);
	}
}
