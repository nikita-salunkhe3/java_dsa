import java.util.*;

class Player{
	String name = null;
	int jerNo = 0;

	Player(String name, int jerNo){
		this.name = name;
		this.jerNo = jerNo;
	}
	public String toString(){
		return "{"+name+":"+jerNo+"}";
	}
}
class UserLHM{
	public static void main(String[] args){

		LinkedHashMap lhm = new LinkedHashMap();

		lhm.put(new Player("Virat",18),'A');
		lhm.put(new Player("MSD",7),'B');
		lhm.put(new Player("Rohit",45),'C');
		lhm.put(new Player("Sachin",10),'D');
		lhm.put(new Player("MSD",7),'E');

		System.out.println(lhm);
	}
}
				
