import java.util.*;

class HashMapDemo{
	public static void main(String[] args){

		HashMap hm = new HashMap();

		hm.put("Nik",1);
		hm.put("Sam",2);
		hm.put("Ram",3);
		hm.put("Kam",4);

		System.out.println(hm);

		HashMap hm1 = new HashMap();

		hm1.put(10,1);
		hm1.put(40,2);
		hm1.put(20,3);
		hm1.put(30,4);
		System.out.println(hm1);
	}
}
