import java.util.*;

class Player implements Comparable{
	String name=null;
	int jerNo = 0;

	Player(String name,int jerNo){
		this.name= name;
		this.jerNo= jerNo;
	}
	public String toString(){
		return "{"+ name +":"+jerNo+"} ";
	}
	public int compareTo(Object obj){

		System.out.println("In CompareTo");
		return this.name.compareTo(((Player)obj).name);
	}
}

class SortHashSet{
	public static void main(String[] args){

		HashSet hs= new HashSet();

		hs.add(new Player("Virat",18));
		System.out.println("In main");
		hs.add(new Player("MSD",7));
		hs.add(new Player("Rohit",45));
		hs.add(new Player("Surykumar",68));

		System.out.println(hs);

		TreeSet ts = new TreeSet(hs);

       		System.out.println(ts);
	}
}


