/**This code proves that HashSet Internally calls to HashMap***/
/***HashMap internally call to Hashing function******/

import java.util.*;

class Friends{
	public static void main(String[] args){

		HashSet hs = new HashSet();

		hs.add("Kanha");
		hs.add("Rahul");
		hs.add("Badhe");
		hs.add("Ashish");
		hs.add("Shashi");

		System.out.println(hs);

		HashMap hm = new HashMap();

		hm.put("Kanha","Infosys");
		hm.put("Rahul","BMC");
		hm.put("Badhe","CarPro");
		hm.put("Ashish","Barclays");
		hm.put("Shashi","Biencaps");

		System.out.println(hs);
	}
}


