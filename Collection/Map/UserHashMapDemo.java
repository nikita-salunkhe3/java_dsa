//HashMap 

import java.util.*;

class Player{

	String name = null;
	int jerNo = 0;

	Player(String name,int jerNo){
		this.name = name;
		this.jerNo = jerNo;
	}
	public String toString(){
		return "{"+name+":"+jerNo+"}";
	}
}
class HashMapDemo{
	public static void main(String[] args){

		HashMap hs = new HashMap();

		hs.put((new Player("Virat",18)),1);
		hs.put((new Player("MSD",7)),2);
		hs.put((new Player("Rohit",45)),3);
		hs.put((new Player("Sachin",10)),4);
		hs.put((new Player("MSD",7)),5);

		System.out.println(hs);//It does not update the values of Duplicate Key
	}
}

