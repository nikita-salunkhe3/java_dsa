/*
 * TreeMap --->>>> TreeMap sort the data
 */

import java.util.*;

class TreeMapDemo{
	public static void main(String[] args){

		TreeMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("pak","Pakistan");
		tm.put("SL","Shrilanka");
		tm.put("Aus","Australia");
		tm.put("Ban","Bangladesh");

		System.out.println(tm);
	}
}
