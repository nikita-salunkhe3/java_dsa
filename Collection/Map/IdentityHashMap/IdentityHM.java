/*identityHashMap -===>>> Why it is Coming
 * identity HashMap Store Duplicate Data In it So that we can used IdentityHashMap
 *
 * IdentityHashMap calls Hashing function
 * HashMap --Hashing function
 * HashSet--->>Hashing function
 */

import java.util.*;

class IdentityHM{
	public static void main(String[] args){

		IdentityHashMap ihm = new IdentityHashMap();

		ihm.put("India","Ind");
		ihm.put(new String("Pakistan"),"Pak");
		ihm.put("Shrilanka","SL");
		ihm.put("Austria","AT");
		ihm.put(new String("Pakistan"),"AT");

	/*	ihm.put("SAM",1);
		ihm.put("RAM",2);
		ihm.put("KAM",3);*/

		System.out.println(ihm);
	}
}
