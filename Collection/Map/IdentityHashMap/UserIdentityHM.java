//***
//IdentityHashMap Updates Value Of Pre-defined Classes 
//Because IdentityHashMap Compares Address and If the Address are Match then it will update the values
//but User-defined Objects tayar kele ter apan new nech object tayar karu ani yamule pretek object cha 
//address different ahe ani tyamule to Duplicate Entry chalun gheto

import java.util.*;

class Player{
	String name = null;

	Player(String name){
		this.name = name;
	}
	public String toString(){
		return name;
	}
}
class UserIHM{
	public static void main(String[] args){

		IdentityHashMap ihm = new IdentityHashMap();

		ihm.put(new Player("Virat"),18);
		ihm.put(new Player("MSD"),7);
		ihm.put(new Player("Rohit"),45);
		ihm.put(new Player("Sachin"),10);
		ihm.put(new Player("MSD"),23);

		System.out.println(ihm);//It Does Not Update the Values of Duplicate Key because 
		//IdentityHashMap can Compares Addresses
		
		System.out.println(ihm.size());//5
		System.out.println(ihm.hashCode());//1169728707

		Object obj = ihm.clone();
		System.out.println(obj.hashCode());//1169728707

	}
}



		
