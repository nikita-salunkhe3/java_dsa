import java.util.*;

class Demo{
	public static void main(String[] args){

		HashMap obj = new HashMap();

		obj.put(10,"Kanha");
		obj.put(10,"Badhe");
		obj.put(10,"Rahul");

		System.out.println(obj);//{10,Rahul}
		
		HashMap obj1 = new HashMap();

		obj1.put(new Integer(10),"Kanha");
		obj1.put(new Integer(10),"Badhe");
		obj1.put(new Integer(10),"Rahul");
		System.out.println(obj1);//{10,Rahul}

		System.out.println("For IdentityHashMap ");

		IdentityHashMap iobj = new IdentityHashMap();

		iobj.put(10,"Kanha");
		iobj.put(10,"Badhe");
		iobj.put(10,"Rahul");

		System.out.println(iobj);//{10,Rahul}
		
		IdentityHashMap iobj1 = new IdentityHashMap();

		iobj1.put(new Integer(10),"Kanha");
		iobj1.put(new Integer(10),"Badhe");
		iobj1.put(new Integer(10),"Rahul");
		System.out.println(iobj1);//{10,Rahul}


	}
}





