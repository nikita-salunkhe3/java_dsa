import java.util.*;

class CollectionDemo{
	public static void main(String[] args){

		List l = new ArrayList();

		l.add(10);
		l.add(20.5f);
		l.add(30.40);
		l.add("Nikita");

		System.out.println(l);
	}
}

