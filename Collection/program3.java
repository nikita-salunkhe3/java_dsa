import java.util.*;

class ArrayListDemo extends ArrayList{
	public static void main(String[] args){

		ArrayListDemo al = new ArrayListDemo();

		//void add(E)

		al.add(10);
		al.add(20.5f);
		al.add("Nikita");
		al.add(10);
		System.out.println(al);//[10, 20.5, Nikita, 10]

		//public int size();
		System.out.println(al.size());//4

		//boolean contains(java.lang.object)
		System.out.println(al.contains(al));//false
		System.out.println(al.contains(10));//true
		System.out.println(al.contains("Nikita"));//true

		//int indexOf(object)
		System.out.println(al.indexOf("Nikita"));//2
		System.out.println(al.indexOf(50));//-1

		//int lastIndexOf(object)
		System.out.println(al.lastIndexOf(10));//3
		System.out.println(al.lastIndexOf(40));//-1

		//E get(int)
		System.out.println(al.get(1));//20.5
//		System.out.println(al.get(5));//RuntimeException : IndexOutOfBoundsException

		//E set(int, E)
		System.out.println(al.set(2,"niki"));//Nikita
		System.out.println(al);//[10, 20.5, niki, 10]

		//void add(int, Element);
		al.add(4,"kirti");
		System.out.println(al);//[10, 20.5, niki, 10, kirti]
	//	al.add(6,"Nisha");
	//	System.out.println(al);//[10, 20.5, niki, 10, kirti]
		//Exception in thread "main" java.lang.IndexOutOfBoundsException: Index: 6, Size: 5

		//E remove(int)
		System.out.println(al.remove(4));//kirti
		System.out.println(al);//[10, 20.5, niki, 10]

		ArrayList obj = new ArrayList();
		obj.add(1);
		obj.add(2);
		obj.add(3);

		//boolean remove(object)
		System.out.println(al.remove("niki"));//true
		System.out.println(al);//[10, 20.5, 10]

		//boolean addAll(collection)
		System.out.println(al.addAll(obj));//true
		System.out.println(al);//[10, 20.5, 10, 1, 2, 3]

		ArrayList obj1=new ArrayList();
		obj1.add("Nikita");
		obj1.add("Shweta");
		obj1.add("Kirti");

		//boolean addAll(int, collection)
		System.out.println(al.addAll(2,obj1));//true
		System.out.println(al);//[10, 20.5, Nikita, Shweta, Kirti, 10, 1, 2, 3]

		//object[] toArray();
		Object arr[] = al.toArray();
		System.out.println(arr);//[Ljava.lang.Object;@7ad041f3

		System.out.println("*********Collection is converted to array******");
		for(Object data: arr){
			System.out.print(data+ "  ");
		}
		System.out.println();

		//protected void removeRange(int, int)
		al.removeRange(5,7);//==>> yamadhe remove fkt 5th index ani 6th index vala hoto
		System.out.println(al);//[10, 20.5, Nikita, Shweta, Kirti, 2, 3]

		//void clear()
		al.clear();
		System.out.println(al);//[]

	}
}
