//2. ListIterator
//ListIterator is an Interface
/****methods which are included in ListIterator Interface*****/
/*public abstract boolean hasNext();
  public abstract E next();
  public abstract boolean hasPrevious();
  public abstract E previous();
  public abstract int nextIndex();
  public abstract int previousIndex();
  public abstract void remove();
  public abstract void set(E);
  public abstract void add(E);
  */

import java.util.*;
class ListIteratorDemo{
	public static void main(String[] args){

		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20);
		al.add(30);
		al.add(40);
		al.add(50);

		System.out.println(al);

		ListIterator litr = al.listIterator();
		while(litr.hasNext()){
	
			System.out.println(litr.next());// 10 20  30 40 50
		}
		System.out.println(litr.previousIndex());//4
		System.out.println(litr.nextIndex());//5
		System.out.println(litr.nextIndex());//5

		System.out.println(al.indexOf(litr));//-1
		System.out.println("******************");
		/*while(litr.hasPrevious()){
			System.out.println(litr.next());//Exception fekel NoSuchElementException
		}
		*/

		while(litr.hasPrevious()){
			System.out.println(litr.previous());
		}
		System.out.println(al.indexOf(litr));//-1
		System.out.println(litr.nextIndex());//0
		System.out.println(litr.previousIndex());//-1
/*
		litr.remove();
		
//		ListIterator litr = al.listIterator();
		while(litr.hasPrevious()){
			System.out.println(litr.previous());
		}
		while(litr.hasNext()){
			System.out.println(litr.next());
		}	*/

	}
}
