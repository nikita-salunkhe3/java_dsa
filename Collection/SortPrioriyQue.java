/********Sort PriorityQueue by Implementing Comparator************/

import java.util.*;

class Player{
	String name = null;
	int jerNo = 0;

	Player(String name,int jerNo){
		this.name = name ;
		this.jerNo = jerNo;
	}
	public String toString(){
		return "{"+name+":"+jerNo+"} ";
	}
	public int compareTo(Object obj){
		return this.name.compareTo(((Player)obj).name);
	}
}
class SortByJerNo implements Comparator{

	public int compare(Object obj1,Object obj2){
		System.out.println("In Comparator");
		return ((((Player)obj1).jerNo) - (((Player)obj2).jerNo));
	}
}
class SortPriQueue{
	public static void main(String[] nikita){

		PriorityQueue pq = new PriorityQueue(new SortByJerNo());

		pq.offer(new Player("Virat",18));
		pq.offer(new Player("MSD",7));
		pq.offer(new Player("Rohit",45));
		pq.offer(new Player("Sachin",10));

		System.out.println(pq);

		ArrayList al = new ArrayList(pq);

		System.out.println(al);

		Collections.sort(al,new SortByJerNo());

		System.out.println(al);

		
	}
}


