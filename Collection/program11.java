//cursor 4 types 
//1. Iterator()
//2. Spliterator
//3. ListIterator
//4. Enumeration

//1. Iterator 
//Iterator is an Interface which consist 4 methods
//Iterator is Universal Cursor to all the Collection Family
//***Iterator Includes Following methods*****/
/* 1.public abstract boolean hasNext();
   2.public abstract E next();
   3.public default void remove();
   4.public default void forEachRemaining(java.util.function.Consumer<? super E>);
  */

import java.util.*;
class IteratorDemo{
	public static void main(String[] args){

		ArrayList al= new ArrayList();

		al.add(10);
		al.add(10.2f);
		al.add(30.34);
		al.add("Shahsi");
		al.add(10);

		System.out.println(al);
		System.out.println("*********");

		for(var x: al){
			System.out.println(x.getClass().getName());
		}

		Iterator itr = al.iterator();//iterator mento me jya type chya class ver tayar zalo ahe tyach typecha honar menun ha iterator ArrayList type cha ahe 

		while(itr.hasNext()){
			System.out.println(itr.next());
			itr.remove();
		}
		System.out.println(al);
	}
}	
