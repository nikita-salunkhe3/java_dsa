import java.util.*;
class HastSetDemo{
	public static void main(String[] args){

		HashSet hs= new HashSet();

		hs.add(10);
		hs.add(20);
		hs.add(new Integer(10));
		hs.add(new Integer(20));

		System.out.println(hs);//[20, 10]
	}
}
/**OutPut: [20, 10]
 * add(10) ha internally add(new Integer(10)); assa jato yamule HashSet la samjat internally yala sort 
 * keraych ahe Sorting fkt Predefined Gosti ch hot user-defined pn hot pn tyala aplyala code lihava lagto
 * karen User-defined lihitana Comparable Interface Implements kerava lagto tyatil compareTo() ya method 
 * la Body dyavi lagte
 *
 * Je Pre-Defined classes ahet tyat Comparable interface implements kela ahe ani body sudha dileli ahe 
 * tyamule HashSet Sort karen deto
 *
 * Set madhe jya Type madhe Data Insert kela ahe tya type madhe data output la distat nahi karen
 * Set Sequentially data Store Kernyachi capacity nahiye to swta data sort kerto
 */
