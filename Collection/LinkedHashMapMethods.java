/*********Methods of LinkedHashMap**********/

/**LinkedHashMap -- it Preserve Insertion order ******/

/**Constructor of LinkedHashMap********/
/*  public java.util.LinkedHashMap(int, float);
  public java.util.LinkedHashMap(int);
  public java.util.LinkedHashMap();
  public java.util.LinkedHashMap(java.util.Map<? extends K, ? extends V>);
  public java.util.LinkedHashMap(int, float, boolean);
*/

import java.util.*;

class LinkedHashMapDemo{

	public static void main(String[] args){

		LinkedHashMap lhm = new LinkedHashMap();

		lhm.put("India","Ind");
		lhm.put("Pakistan","Pak");
		lhm.put("USA","US");
		lhm.put("Shrilanka","SL");

		System.out.println(lhm);

		LinkedHashMap lhm1 = new LinkedHashMap(lhm);
		lhm1.put("India","Ind");
		lhm1.put("Pakistan","Pak");
		lhm1.put("USA","US");
		lhm1.put("Austria","AT");
		System.out.println(lhm1);

		/*******Methods of LinkedHashMap **************/
/*public boolean containsValue(java.lang.Object);
  public V get(java.lang.Object);
  public V getOrDefault(java.lang.Object, V);
  public void clear();
  protected boolean removeEldestEntry(java.util.Map$Entry<K, V>);//Accessible When the class is child 
  public java.util.Set<K> keySet();
  final <T> T[] keysToArray(T[]);
  final <T> T[] valuesToArray(T[]);
  public java.util.Collection<V> values();
  public java.util.Set<java.util.Map$Entry<K, V>> entrySet();
*/
		//boolean containsValue(Object) -->> Object madhe fkt value pahije
		System.out.println(lhm1.containsValue("Ind"));//true
		System.out.println(lhm1.containsValue("India"));//false
		System.out.println(lhm1.containsValue("FR"));//false

		//V get(Object)
		System.out.println(lhm1.get("Ind"));//null
		System.out.println(lhm1.get("India"));//Ind
		System.out.println(lhm1.get("FR"));//null

		//V getOrDefault(Object,V)-->> He method Entry madhil Value return Karun dete
		//jeri ti entry collection madhe present nasli teri parameter madhun Value Return Kerun 
		//dete
		System.out.println(lhm1.getOrDefault("India","Ind"));//Ind
		System.out.println(lhm1.getOrDefault("France","FR"));//FR

		//public java.util.Set<K> keySet();
		System.out.println(lhm1.keySet());//[India,Pakistan,USA,Shrilanka,Austria]

		//final <T> T[] keysToArray(T[]);
		
		//public java.util.Collection<V> values();

		System.out.println(lhm1.values());//[Ind,Pak,US,SL,AT]

		//public java.util.Set<java.util.Map$Entry<K, V>> entrySet();

		System.out.println(lhm1.entrySet());//{India=Ind, Pakistan=Pak, USA=US, Shrilanka=SL, Austria=AT}
	}
}


