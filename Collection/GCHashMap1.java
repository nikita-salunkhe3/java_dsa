import java.util.*;

class Country{
	String name = null;

	Country(String name){
		this.name = name;
	}
	public String toString(){
		return name;
	}
	public void finalize(){
		System.out.println("Notification");
	}

}
class GCDemo{
	public static void main(String[] args){

		Country obj1 = new Country("India");
		Country obj2 = new Country("Pakistan");
		Country obj3 = new Country("NewYork");
		Country obj4 = new Country("Shrilanka");
	
		HashMap hs= new HashMap();

		hs.put(obj1,"IND");
		hs.put(obj2,"PAK");
		hs.put(obj3,"NY");
		hs.put(obj4,"SL");

		obj1=null;
		obj2=null;

		System.gc();

		System.out.println(hs);
	}
}
/*
 * Conclusion:
 * HashMap madhe jeri apan Explicitly Garbage collector la call jeri kela teri to Object/Entry gheun
 * jat nahi
 * Karen HashMap la Garbage collector shi kahich ghen den nahiye 
 * jeri HashMap madhil object null zala teri to Garbage collector la object free karu det nahi
 */

