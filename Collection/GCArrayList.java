import java.util.*;

class Country{
	String name= null;

	Country(String name){
		this.name = name;
	}
	public String toString(){
		return name;
	}
	public void finalize(){
		System.out.println("Notify");
	}
}
class GCDemo{
	public static void main(String[] args){

		Country obj1 = new Country("India");
		Country obj2 = new Country("NY");
		Country obj3 = new Country("USA");
		Country obj4 = new Country("Rashia");

		ArrayList al = new ArrayList();

		al.add(obj1);
		al.add(obj2);
		al.add(obj3);
		al.add(obj4);

		System.out.println(al);

		obj1 = null;
		obj2 = null;

		System.gc();

		System.out.println(al);
		System.out.println("In main");
	}
}
