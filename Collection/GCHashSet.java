/**
 * java madhe jasa HashMap Garbage collector la ajibat bhav det nahi menjech 
 * jeri apan System.gc() Garbage collector la explicitly call kela teri to null object delete kerat nahi 
 * asach HashSet sudha Garbage collector la bhav det nahi
 */

import java.util.*;

class Country{
	String name = null;

	Country(String name){
		this.name  = name;
	}
	public String toString(){
		return name;
	}
	public void finalize(){
		System.out.println("Notification");
	}
}
class GCDemo{
	public static void main(String[] args){

		Country obj1 = new Country("India");
		Country obj2 = new Country("USA");
		Country obj3 = new Country("Africa");
		Country obj4 = new Country("Newzland");

		HashSet hs= new HashSet();

		hs.add(obj1);
		hs.add(obj2);
		hs.add(obj3);
		hs.add(obj4);

		obj1 = null;
		obj2 = null;

		System.gc();//finalize() method la eth cl jat nahiye karen jeri apan Explicitly cl kela teri to Object delete karat nahi .............jasa HashMap Garbage collector la bhav det nahi tasach HasSet sudha GC la bhav det nahi karen .......*****HashSet ha internally HashMap la cl kerto*******.........*/
		
		System.out.println(hs);
	}
}


