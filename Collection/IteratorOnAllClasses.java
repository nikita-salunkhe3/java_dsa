import java.util.*;
		
class IteratorDemo{
	public static void main(String[] args){

		/*******ArrayList********/
		ArrayList al = new ArrayList();
		
		al.add(10);
		al.add(20);
		al.add(30);

		System.out.println(al);//[10, 20, 30]
		/******Iterator*****/

		Iterator itr = al.iterator();
		System.out.println(itr);//ArrayList$Itr@Address
		System.out.println(itr.getClass());//class java.util.ArrayList$Itr
		System.out.println(itr.getClass().getName());//java.util.ArrayList$Itr

		/***ListIterator******/

		ListIterator litr=al.listIterator();

		System.out.println(litr);//ArrayList$ListItr@Address
		System.out.println(litr.getClass());//class java.util.ArrayList$ListItr
		System.out.println(litr.getClass().getName());//java.util.ArrayList$ListItr

		/********LinkedList**********/
		LinkedList ll = new LinkedList();

		ll.add(11);
		ll.add(22);
		ll.add(33);

		System.out.println("ll = "+ll);//[11, 22, 33]

		/******Iterator********/
		Iterator itr1=ll.iterator();
		System.out.println(itr1);//LinkedList$ListItr@Address
		System.out.println(itr1.getClass());//class java.util.LinkedList$ListItr
		System.out.println(itr1.getClass().getName());//java.util.LinkedList$ListItr

		/******ListIterator*********/
		ListIterator litr1=ll.listIterator();

		while(litr1.hasNext()){
			System.out.println(litr1.next());//[11, 22, 33]

		}
		System.out.println(litr1);//LinkedList$ListItr@address
		System.out.println(litr1.getClass().getName());//java.util.LinkedList$ListItr

		/***********Vector*************/
		Vector v = new Vector();

		v.add(25);
		v.add(50);
		v.add(75);

		System.out.println(v);//[25, 50, 75]

		/********Iterator*************/
		Iterator itr2= v.iterator();

		System.out.println(itr2);//Vector$Itr@Address

		System.out.println(itr2.getClass().getName());//java.util.Vector$Itr

		while(itr2.hasNext()){
			System.out.println(itr2.next());//[25, 50, 75]
		}
		
		/*********ListIterator************/
		ListIterator litr3=v.listIterator();

		System.out.println(litr3);//Vector$ListItr@Address
		System.out.println(litr3.getClass().getName());//java.util.Vector$ListItr

		while(litr3.hasNext()){
			System.out.println(litr3.next());//[25, 50, 75]
		}
		/***********Enumeration**********/
		Enumeration enm = v.elements();

		System.out.println(enm);//Vector$1@Address

		System.out.println(enm.getClass().getName());//java.util.Vector$1............Karen
		//Ha Annonymous Inner class ahe Tyamule yacha Class ch nav Vector$1 ass aal

		while(enm.hasMoreElements()){
			System.out.println(enm.nextElement());//[25, 50, 75]
		}

		/***********Stack**************/
		Stack s = new Stack();

		System.out.println(s);//[]
		s.add(100);
		s.add(200);
		s.add(300);
		System.out.println(s);//[100, 200, 300]

		/***********Iterator**************/

		Iterator itr4= s.iterator();
		System.out.println(itr4);//Vector$Itr@Address
		System.out.println(itr4.getClass().getName());//java.util.Vector$Itr

		/**********ListIterator***********/
		
		ListIterator litr4= s.listIterator();
		System.out.println(litr4);//Vector$ListItr@Address
		System.out.println(litr4.getClass().getName());//java.util.Vector$ListItr


		/**********Enumeration************/

		Enumeration enm1 = s.elements();

		System.out.println(enm1);//Vector$1@Address

		System.out.println(enm1.getClass().getName());//java.util.Vector$1

		while(enm1.hasMoreElements()){
			System.out.println(enm1.nextElement());//[100, 200, 300]
		}
	}

}

