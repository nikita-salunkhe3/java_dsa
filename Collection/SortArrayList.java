/*>>>>Problem Statement*>>>>>>>>>*/
/*
 * Take Duplicate Data in collection which can be arranged in Sorted Mannner But, One think is clear that
 * Don't remove Duplicate data in it.....*/

//Used ArrayList And For Sorting purpose used sort() method which is Present in Collections class 
//sort() method is static so that you can call the method on class Name
//sort() is does Not Automatically call to compareTo method so That we Have implements Camparator interface
//In This Comparator Interface 2 Methods are Imp 
//1. equals()
//2. compare()
//here our class Parent Must object class Indirectly / Directly so we have know Object class contain equals method and Object class gives body to equals mathod 
//so that we only have to give body to the compare() method


import java.util.*;

class Dmart{
	String DmartName = null;
	float Rating = 0.0f;

	Dmart(String DmartName,float Rating){
		this.DmartName = DmartName;
		this.Rating = Rating;
	}
	public String toString(){
		return "{"+DmartName+":"+Rating+"} ";
	}
	
}
class SortByName implements Comparator{

	/********public abstract int compare(T, T);*********/

	public int compare(Object obj1,Object obj2){//compare() he method Comparator interface chi aahe

		System.out.println("Srting call");
		return (((Dmart)obj1).DmartName).compareTo(((Dmart)obj2).DmartName);
	}
}
class SortByRating implements Comparator{

	public int compare(Object obj1,Object obj2){

		System.out.println("Float call");
		return (int)(((Dmart)obj1).Rating - ((Dmart)obj2).Rating);
	}
}

class SortArrayList{
	public static void main(String[] args){

		ArrayList al = new ArrayList();

		System.out.println(al);

		al.add(new Dmart("Pune Dmart",8.0f));
		al.add(new Dmart("Satara Dmart",9.0f));
		al.add(new Dmart("Narhe Dmart",10.0f));
		al.add(new Dmart("Vai Dmart",8.0f));

		System.out.println(al);

		/**public static <T> void sort(java.util.List<T>, java.util.Comparator<? super T>);****/
		/*Sort method is only for List Interface 
		 * It is Used To Sort data which is present in ArrayList, LinkedList, Vector, Stack
		 * Sort method is not for set 
		 * because Set all ready has one feature that is can sort data so that java developer 
		 * Does not gives sort() method for Set Interface
		 */

		/***Here for TreeSet we Implements Comparable Interface but in ArrayList why we not 
		 * implements Comparable 
		 * because In ArrayList List Does not have a Feature that it can Sort the data so that 
		 * it uses sort() methods which is of Collections Class 
		 * In sort() methods There are 2 Parameter 1 st is 
		 * List Collection and 2nd one is Comparator interface
		 * Sort() methods required Comparator Type Object 
		 * So That we implements Comparator interface Not Comparable Interface
		 */


		Collections.sort(al,new SortByName());

		System.out.println(al);

		Collections.sort(al,new SortByRating());

		System.out.println(al);
	}
}
		
