import java.util.*;
class School{
	char ch = ' ';

	School(char ch){
		this.ch = ch;
	}
	public char tostring(){
		return ch;
	}
}
class Defence{
	String DefenceType=null;
	
	Defence(String str){
		DefenceType = str;
	}
	public String toString(){
		return DefenceType;
	}
}
class HashSetDemo{
	public static void main(String[] args){

		HashSet hs = new HashSet();
		
		hs.add(new Defence("Army"));
		hs.add(new Defence("Navy"));
		hs.add(new Defence("AirForce"));
		hs.add("NIk");

		System.out.println(hs);

	//	System.out.println(hs.indexOf("Army"));      //error: Cannot find symbol
		//karen Set interface or HashSet madhe indexOf() hi method ch nahiye
		//index chi concept fkt List madhe ahe so apan set madhil data index ne access karu
		//shakat nahi

		/******Methods of HashSet Class**********/

	        /**hs.clear();*/

		/**1.boolean isempty()**/
		System.out.println(hs.isEmpty());//False jer me data set ha clear kela ter True answer
		//yeil

		/**2.boolean contains(object)**/
		System.out.println(hs.contains("NIk"));//True (fkt Pridefine class cha object pass kela ter true value yeil user define object madhe string or any data asel ter ani collection madhe jeri to data present asel teri contains() method vaperli teri False return hot)
		System.out.println(hs.contains("Army"));

		/***Iterator<E> iterator()*/

		Iterator itr = hs.iterator();

		/***hasNext();*****/
		while(itr.hasNext()){
		//	itr.remove();//Actually remove() method hi iterator chi nasun Set chi ahe
			/*Exception in thread "main" java.lang.IllegalStateException
	at java.util.HashMap$HashIterator.remove(HashMap.java:1481)
	at HashSetDemo.main(HashSetDemo.java:47)*/

			/********next();********/
			System.out.println(itr.next());//NIk Army Navy AirForce
		}

		/***object[] toArray()********/
		Object arr[]=hs.toArray();
		for(var x : arr){
			System.out.println(x);//NIK Army Navy AirForce
		}
		/***** boolean add(E) ********/
		System.out.println(hs.add("Agri"));//true

		/********boolean containsAll(Collection)*****/

		HashSet hs1=new HashSet();

		hs1.add("AirForce");
		hs1.add("Army");
		hs1.add("Agri");
		hs1.add("Navy");
		hs1.add("NIk");

		System.out.println(hs1);
		System.out.println(hs);

		System.out.println(hs.containsAll(hs1));//False//Ya Secnario madhe apan User hs Hashset
		//madhe UserDefined Object Tayar kele ahet ani hs1 madhe apan String class che Object tayar kertoy menun eth match hot nahiye menun output false yetay.........

		HashSet hs2 = new HashSet();
		
		hs.add(new Defence("Army"));
		hs.add(new Defence("Navy"));
		hs.add(new Defence("AirForce"));
		hs.add("NIk");
		hs.add("Agri");
		System.out.println(hs.containsAll(hs2));//True ....Ya Secnario madhe Pahile 3 Object he 
		//user defined ahet ani last 2 object he predefined String class che ahet menun yete match bhetala servansathi menun yeth True ass O/P bhetl............

		System.out.println(hs);

		/****boolean addAll(Collection)********/

		HashSet hs3 = new HashSet();
		hs3.add('A');
		hs3.add('B');
		hs3.add('C');
		hs3.add('D');

		System.out.println(hs);

		System.out.println(hs3);

		System.out.println(hs.addAll(hs3));//true

		System.out.println(hs);

		/****boolean retainAll(Collection)*********/
		System.out.println(hs);

		System.out.println(hs.retainAll(hs3));

		System.out.println(hs3);
		System.out.println(hs);
	}
}
