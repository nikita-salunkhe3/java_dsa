import java.util.*;

class Movies{
	String movieName = null;
	float imdb = 0.0f;

	Movies(String movieName, float imdb){
		this.movieName = movieName;
		this.imdb = imdb;
	}
	public String toString(){
		return "{" +movieName+ ":" + imdb +"} ";
	}
}
class SortByName implements Comparator{

	public int compare(Object obj1, Object obj2){
		return (((Movies)obj1).movieName).compareTo(((Movies)obj2).movieName);
	}
}

class SortByImdb implements Comparator{

	public int compare(Object obj1, Object obj2){
		return (int)(((Movies)obj1).imdb - (((Movies)obj2).imdb));
	}
}

class SortArrayListUsingAnno{
	public static void main(String[] args){
		ArrayList al = new ArrayList();
 
		al.add(new Movies("Kantara",8.0f));
		al.add(new Movies("Bhediya",8.9f));
		al.add(new Movies("sir",9.5f));

		System.out.println(al);

		Collections.sort(al, new SortByName(){
			public int compare(Object obj1, Object obj2){
				System.out.println("Anonymous String");
				return (((Movies)obj1).movieName).compareTo(((Movies)obj2).movieName);
			}
		});
		
		System.out.println(al);

		Collections.sort(al, new SortByImdb(){
			public int compare(Object obj1,Object obj2){
				System.out.println("Anonymous Rating");
				return (int)((((Movies)obj1).imdb) - (((Movies)obj2).imdb));
			}
		});

		System.out.println(al);
	}
}
