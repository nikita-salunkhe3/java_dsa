import java.util.concurrent.*;
import java.util.*;

class LinkedBQ{
	public static void main(String[] args)throws InterruptedException{

		LinkedBlockingQueue pq = new LinkedBlockingQueue();

		pq.put(10);
		pq.put(30);
		pq.put(20);
		pq.put(40);

		System.out.println(pq);
		
		LinkedBlockingQueue pq1 = new LinkedBlockingQueue(pq);

		pq1.put(50);
		pq1.put(60);
		pq1.put(70);
		pq1.put(80);

		System.out.println(pq1);
		
		LinkedBlockingQueue pq2 = new LinkedBlockingQueue(3);

		pq2.put(90);
		pq2.put(100);
		pq2.put(110);
		pq2.put(120);

		System.out.println(pq2);
	}
}

