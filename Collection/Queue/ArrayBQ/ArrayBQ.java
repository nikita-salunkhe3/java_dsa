//BlockingQueue->>>>
//-BlockingQueue is An interface which is present in util.concurrent Package
//-It is a Bounded Queue
//-It has fixed Size
//-Queue is the Parent of BlockingQueue
//-It Follows Thread-safe(Threads Acts as synchronized)

//ArrayBlockingQueue

//put() method throws interrupted Exception 
import java.util.concurrent.*;

class ArrayBQ{
	public static void main(String[] args)throws InterruptedException{

		ArrayBlockingQueue aqueue = new ArrayBlockingQueue(4);

		System.out.println(aqueue);

		aqueue.put(10);
		aqueue.put(20);
		aqueue.put(30);
		aqueue.put(40);

		System.out.println(aqueue);
		
		aqueue.put(50);//main thread Block zala ahe jeri apan pude data delete kernyasathi
		//take() method lihili teri to pude jat nahi karan to adhichach line la Block zala ahe
	}
}
