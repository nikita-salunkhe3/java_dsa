import java.util.*;
import java.util.concurrent.*;

class Demo{
	String name;

	Demo(String name){
		this.name = name;
	}
	public String toString(){
		return name;
	}
}
class UserArrayPQ{
	public static void main(String[] args)throws InterruptedException{

		ArrayBlockingQueue aq = new ArrayBlockingQueue(3);

		aq.put(new Demo("Nikita"));
		aq.put(new Demo("Sarthak"));
		aq.put(new Demo("Martand"));

		System.out.println(aq);

		aq.put(new Demo("Gaurav"));//Main thread is Blocked 

		System.out.println(aq);
	}
}


		
