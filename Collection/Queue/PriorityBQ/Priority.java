import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

class Task {
    private String name;
    private int priority;

    public Task(String name, int priority) {
        this.name = name;
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", priority=" + priority +
                '}';
    }
}

class PriorityBlockingQueueExample {
    public static void main(String[] args) {
        PriorityBlockingQueue<Task> priorityQueue = new PriorityBlockingQueue<>(10,
            Comparator.comparingInt(Task::getPriority));

        priorityQueue.add(new Task("High Priority Task", 1));
        priorityQueue.add(new Task("Low Priority Task", 10));
        priorityQueue.add(new Task("Medium Priority Task", 5));

        while (!priorityQueue.isEmpty()) {
            System.out.println(priorityQueue.poll());
        }
    }
}
