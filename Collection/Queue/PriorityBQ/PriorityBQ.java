import java.util.concurrent.*;
import java.util.*;

class Demo implements Comparable{

	String str;
	 
	Demo(String str){
		this.str = str;
	}
	public String toString(){
		return str;
	}
	public int compareTo(Object obj){
		return str.compareTo(((Demo)obj).str);
	}
}
class Demo1 implements Comparable{
	int num = 0;

	Demo1(int num){
		this.num = num;
	}
	public String toString(){
		return num+"";
	}
	public int compareTo(Object obj){
		return num - (((Demo1)obj).num);
	}
}
class PriorityQB{
	public static void main(String[] args){

		PriorityBlockingQueue pq = new PriorityBlockingQueue();

		pq.put("Nikita");
		pq.put("Martand");
		pq.put("Sarthak");
		pq.put("Gaurav");

		System.out.println(pq);//[Gaurav, Martand, Sarthak, Nikita]

		PriorityBlockingQueue pq1 = new PriorityBlockingQueue();

		pq1.put(new Demo("Nikita"));
		pq1.put(new Demo("Martand"));
		pq1.put(new Demo("Sarthak"));
		pq1.put(new Demo("Gaurav"));

		System.out.println(pq1);//[Gaurav, Martand, Sarthak, Nikita]
		
		PriorityBlockingQueue pq2 = new PriorityBlockingQueue();

		pq2.put(30);
		pq2.put(10);
		pq2.put(40);
		pq2.put(20);

		System.out.println(pq2);//[Gaurav, Martand, Sarthak, Nikita]

		PriorityBlockingQueue pq3 = new PriorityBlockingQueue();

		pq3.put(new Demo1(30));
		pq3.put(new Demo1(10));
		pq3.put(new Demo1(40));
		pq3.put(new Demo1(20));

		System.out.println(pq3);//[Gaurav, Martand, Sarthak, Nikita]
	}
}
