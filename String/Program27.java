//String Buffer

class StringBufferDemo{
	public static void main(String[] args){

		StringBuffer sb=new StringBuffer();

		System.out.println(sb);//___(blank)
		System.out.println(sb.capacity());//16

		sb.append("Shashi");

		System.out.println(sb);//Shashi
		System.out.println(sb.capacity());//16

		sb.append("Bagal");

		System.out.println(sb);//ShashiBagal
		System.out.println(sb.capacity());//16

		sb.append("Core2web");

		System.out.println(sb);//ShashiBagalCore2web
		System.out.println(sb.capacity());//(16+1)*2===>>> 34
	}
}
		
