class Demo{
	public static void main(String[] args){

		String str1="Nikita";
		String str2="Salunkhe";

		System.out.println(str1+str2);//NikitaSalunkhe

		String str3="NikitaSalunkhe";
		String str4=str1+str2;

		System.out.println(System.identityHashCode(str1));//scp-->>1000
		System.out.println(System.identityHashCode(str2));//scp-->>2000
		System.out.println(System.identityHashCode(str3));//scp-->>3000
		System.out.println(System.identityHashCode(str4));//heap-->>4000
	}
}
/*
Note:

str1+str2 ass kel ki internally StringBuilder madhil append method la call dila jato ani append method 
new String return kerto menun str4 cha object scp ver n benta heap section ver benvla jato
*/
