class StringBufferDemo{
	public static void main(String[] args){

		String str1="Shahsi";
		String str2=new String("Bagal");
		StringBuffer str3=new StringBuffer("core2web");

		StringBuffer str4=str1.append(str3);//error-> Cannot find Symbol..
		System.out.println(str1);//Shashi
		System.out.println(str2);//Bagal
		System.out.println(str3);
		System.out.println(str4);
	}
}

/*
Note:
append() hi method StringBuffer chi ahe ani ti method apan String class vapertoy
tyamule cannot find symbol chi error yete
*/
