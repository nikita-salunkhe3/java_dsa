class Demo{
	public static void main(String[] args){

		StringBuffer sb=new StringBuffer(100);

		System.out.println(sb);//___blank
		System.out.println(sb.capacity());//100

		sb.append("Shashi");

		System.out.println(sb);//Shashi
		System.out.println(sb.capacity());//100

		sb.append("bagal");

		System.out.println(sb);//Shahsibagal
		System.out.println(sb.capacity());//100
	}
}


