//implement substring() method

import java.io.*;
class Method11{
	void mysubstring(String str,int start,int end){
		char arr[]=str.toCharArray();

		char arr1[]=new char[end-start+1];
		int j=0;
		for(int i=start;i<=end;i++){
			arr1[j]=arr[i];
			j++;
		}
		System.out.println(arr1);
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter string");

		String str=br.readLine();

		System.out.println("enter starting index");

		int start=Integer.parseInt(br.readLine());

		System.out.println("enter ending index");

		int end=Integer.parseInt(br.readLine());

		Method11 obj=new Method11();

		obj.mysubstring(str,start,end);
	}
}
