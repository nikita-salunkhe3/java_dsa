//implement lastIndexOf

import java.io.*;
class Method9{
	int myLastIndexOf(String str,char ch,int i){
		char arr[]=str.toCharArray();

		for(int j=i;j>=0;j--){
			if(arr[j]==ch){
				return j;
			}
		}
		return -1;
	}
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the string:");

		String str=br.readLine();

		System.out.println("Enter character you want search");

		char ch=(char)br.read();
		br.skip(1);

		System.out.println("Enter from index");

		int i=Integer.parseInt(br.readLine());

		Method9 obj=new Method9();

		System.out.println(obj.myLastIndexOf(str,ch,i));
	}
}
