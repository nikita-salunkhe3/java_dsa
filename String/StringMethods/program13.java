//implement own function length()

import java.io.*;
class Method2{

	int mylength(String str){

		char arr[]=str.toCharArray();

		int count=0;
		for(int x:arr){
			count++;
		}
		return count;
	}
	
	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter the string:");

		String str=br.readLine();

		Method2 obj=new Method2();
		int ret=obj.mylength(str);

		System.out.println("count is : "+ret);
	}
}
		
		


