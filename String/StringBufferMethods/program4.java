//public synchronized StringBuffer delete(int start, int end);

class Method3{
	public static void main(String[] args){

		StringBuffer str=new StringBuffer("core2web");

		System.out.println(str);

		str.delete(2,7);

		System.out.println(str);
	}
}
/*
 * delete method is used to delete character from given start and end index 
 * (2,7) is given it can be delete from index 3 to 6 character {3,6}
 */
