//Implementation of append() method
//take input from user

import java.io.*;
class Method1{
       	static void myAppend(String str1,String str2){

		char arr1[]=str1.toCharArray();
		char arr2[]=str2.toCharArray();

		char arr3[]=new char[arr1.length+arr2.length];
		int j=arr1.length;

		for(int i=0;i<arr1.length;i++){
			arr3[i]=arr1[i];
		}
		for(int i=0;i<arr2.length;i++){
			arr3[j]=arr2[i];
			j++;
		}

		System.out.println(arr3);
	}

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter first string");

		String str1=br.readLine();

		System.out.println("Enter second string");

		String str2=br.readLine();

		myAppend(str1,str2);

	}
}

		
