class Demo{
	public static void main(String[] args){

		String str1="Shahsi";
		String str2=new String("Bagal");

		StringBuffer str3=new StringBuffer("Core2web");

		StringBuffer str4=str3.append(str1);//error

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
	}
}
/*
 * note:
 * here str3 is type of StringBuffer and 
 * append is themethod of StringBuffer
 * but append can retrun StringBuffer string so that 
 * we are put here String str4 
 * so, string and Stringbuffer are siblings which are incompatible with each other
 * so it will gives error
 */
		
