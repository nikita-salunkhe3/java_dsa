class StringDemo{
	public static void main(String[] args){
		 String str1="kanha";//1000
		 String str2="kanha";//1000
		 String str3=new String("kanha");//2000
		 String str4=new String("kanha");//3000
		 String str5=new String("rahul");//4000
		 String str6="rahul";//5000

		 String str7="Shashi";

		 System.out.println(System.identityHashCode(str1));//1000
		 System.out.println(System.identityHashCode(str2));//1000
		 System.out.println(System.identityHashCode(str3));//2000
		 System.out.println(System.identityHashCode(str4));//3000
		 System.out.println(System.identityHashCode(str5));//4000
		 System.out.println(System.identityHashCode(str6));//5000

		 System.out.println();
		 System.out.println(str1.hashCode());//100
		 System.out.println(str2.hashCode());//100
		 System.out.println(str3.hashCode());//100
		 System.out.println(str4.hashCode());//100
		 System.out.println(str5.hashCode());//200
		 System.out.println(str6.hashCode());//200
		 System.out.println(str7.hashCode());//1819698008---->>>>different machine ver run
		 //kela teri same code yetoy
	}
}




