class Demo{
	public static void main(String[] args){

		String str1="Shahsi";
		String str2=new String("Bagal");

		StringBuffer str3=new StringBuffer("core2web");

		String str4=str1.concat(str3);//error->>incompatible type:
		//StringBuffer cannot be converted to String

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);

	}
}
/*
 * note :
 * concat is the method of String class 
 * it is only visible for String class 
 * As concat method always needed only one parameter as String 
 * it does not support StringBuffer parameter 
 * so it will gives error as Incompatible type
 */ 
