class Demo{
	public static void main(String[] args){

		String str1="core2web";
		String str2=new String("core2web");

		String str3=new String("10.21");

		String str4="Core2Web";

		String str5="core2web";

		char str6[]={'c','2','w'};

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
		System.out.println(str5);

		System.out.println(str1+" = "+System.identityHashCode(str1));
		System.out.println(str2+" = "+System.identityHashCode(str2));
		System.out.println(str3+" = "+System.identityHashCode(str3));
		System.out.println(str4+" = "+System.identityHashCode(str4));
		System.out.println(str5+" = "+System.identityHashCode(str5));
		System.out.println(str6+" = "+System.identityHashCode(str6));

	}
}
